import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HttpResponse } from '@angular/common/http';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { SingleHousingTemplateComponent } from 'app/view-selected-establishment/single-housing-template/single-housing-template.component';
import { BookingService } from 'app/entities/booking/booking.service';
import { Booking, IBooking } from 'app/shared/model/booking.model';
import { BookingElementService } from 'app/entities/booking-element/booking-element.service';
import { BookingElement, IBookingElement } from 'app/shared/model/booking-element.model';
import { Moment } from 'moment';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { UserService } from 'app/core/user/user.service';
import { IUser } from 'app/core/user/user.model';
import { Establishment, IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';
import { FiltersService } from 'app/filters/filters.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class SelectedEstablishmentService {
  public housingTemplateList?: IHousingTemplate[];
  public housingTemplateListNb?: Array<number>;
  public housingTemplateListSelected?: Array<boolean>;
  //public housingTemplatesList ?: IHousingTemplate[];
  public housingTemplateComponentList: SingleHousingTemplateComponent[];
  public establishmentId?: number;
  eventSubscriber?: Subscription;
  public userIdentity?: Account | null;
  public user?: IUser;
  public establishment?: Establishment;
  public booking?: Booking;
  public isValidated?: boolean;
  public isRefused?: boolean;
  public totalPrice?: number;

  constructor(
    protected housingTemplateService: HousingTemplateService,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute,
    protected userService: UserService,
    protected establishmentService: EstablishmentService,
    private accountService: AccountService,
    protected bookingService: BookingService,
    protected filterService: FiltersService,
    protected bookingElementService: BookingElementService,
    private router: Router
  ) {
    this.housingTemplateComponentList = [];
    accountService.getAuthenticationState().subscribe(account => {
      this.userIdentity = account;
      if (this.userIdentity !== undefined && this.userIdentity !== null) {
        this.userService.find(this.userIdentity.login).subscribe(user => (this.user = user));
      }
    });
    this.totalPrice = 0;
  }

  init(id: number): void {
    // eslint-disable-next-line no-console
    console.log('CHARGEMENT DU PANIER');
    this.isValidated = false;
    this.isRefused = false;
    this.establishmentId = id;
    this.totalPrice = 0;
    this.housingTemplateService.findEstablishmentHousings(this.establishmentId).subscribe((res: HttpResponse<IHousingTemplate[]>) => {
      this.housingTemplateList = res.body || [];
      this.registerChangeInHousingTemplates();
    });
    this.establishmentService.find(this.establishmentId).subscribe((res: HttpResponse<Establishment>) => {
      this.establishment = res.body || undefined;
    });
  }

  registerChangeInHousingTemplates(): void {
    if (this.housingTemplateList !== undefined) {
      this.housingTemplateListNb = Array<number>(this.housingTemplateList.length);
      this.housingTemplateListSelected = Array<boolean>(this.housingTemplateList.length);
      for (let i = 0; i < this.housingTemplateListNb.length; i++) {
        this.housingTemplateListNb[i] = 1;
      }
      for (let i = 0; i < this.housingTemplateListSelected.length; i++) {
        this.housingTemplateListSelected[i] = false;
      }
    }
  }

  getHousingTList(): Observable<IHousingTemplate[]> | null {
    //if(this.housingTemplateList) return this.housingTemplateList.asObservable();
    return null;
  }

  increaseNbSelected(index: number): void {
    if (
      this.housingTemplateListNb !== undefined &&
      this.housingTemplateList !== undefined &&
      this.housingTemplateListSelected !== undefined
    ) {
      this.housingTemplateListNb[index]++;
    }
    if (this.isRefused) {
      this.isRefused = false;
    }
  }

  decreaseNbSelected(index: number): void {
    if (
      this.housingTemplateListNb !== undefined &&
      this.housingTemplateList !== undefined &&
      this.housingTemplateListSelected !== undefined
    ) {
      this.housingTemplateListNb[index]--;
    }
    if (this.isRefused) {
      this.isRefused = false;
    }
  }

  switchSelection(index: number): void {
    if (
      this.housingTemplateListNb !== undefined &&
      this.housingTemplateList !== undefined &&
      this.housingTemplateListSelected !== undefined
    ) {
      this.housingTemplateListSelected[index] = !this.housingTemplateListSelected[index];
    }
    if (this.isRefused) {
      this.isRefused = false;
    }
  }

  canValidate(): boolean {
    if (this.housingTemplateListNb !== undefined && this.housingTemplateListSelected !== undefined) {
      for (let i = 0; i < this.housingTemplateListNb.length; i++) {
        if (this.housingTemplateListNb[i] > 0 && this.housingTemplateListSelected[i] === true) return true;
      }
    }
    return false;
  }
  validate(): void {
    if (this.booking !== undefined) {
      this.booking.validate = true;
      this.bookingService.update(this.booking).subscribe();
    }
  }

  registerBooking(): void {
    const b: Booking = new Booking(undefined, this.userIdentity?.email, 1000, '', false, this.user, this.establishment);
    this.bookingService.create(b).subscribe(test => {
      this.booking = test.body || undefined;
      let bookingElement: IBookingElement;
      if (this.housingTemplateListNb !== undefined && this.housingTemplateList !== undefined) {
        for (let i = 0; this.housingTemplateList.length > i; i++) {
          for (let j = 0; this.housingTemplateListNb[i] > j; j++) {
            // eslint-disable-next-line no-console
            console.log('boucle');
            if (
              this.housingTemplateListNb[i] !== 0 &&
              this.housingTemplateList !== undefined &&
              this.housingTemplateListSelected &&
              this.housingTemplateListSelected[i] === true
            ) {
              // eslint-disable-next-line no-console
              console.log('CREATION BOOKING ELEMENT');
              bookingElement = new BookingElement(
                undefined,
                moment(this.filterService.start),
                moment(this.filterService.end),
                this.housingTemplateList[i].pricePerNight,
                this.booking,
                this.housingTemplateList[i]
              );
              this.bookingElementService.create(bookingElement).subscribe();
            }
          }
        }
      }
      if (this.booking) {
        this.bookingService.verify(this.booking).subscribe(test2 => {
          this.booking = test2.body || undefined;
          if (this.booking == null) {
            // eslint-disable-next-line no-console
            console.log('La reservation nest pas valide');
            this.isRefused = true;
          } else {
            // eslint-disable-next-line no-console
            console.log('la reservation est valide');
            //routerLink="/payment"
            this.router.navigate(['/payment']);
          }
        });
      }
    });
  }
}
