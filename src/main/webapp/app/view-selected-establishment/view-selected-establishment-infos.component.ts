import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { Subscription } from 'rxjs';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Establishment, IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';
import { IPhoto, Photo } from 'app/shared/model/photo.model';
import { PhotoService } from 'app/entities/photo/photo.service';
import { ManageService } from 'app/entities/manage/manage.service';
import { Manage } from 'app/shared/model/manage.model';
import { RatingService } from 'app/entities/rating/rating.service';

@Component({
  selector: 'jhi-view-selected-establishment-infos',
  templateUrl: './view-selected-establishment-infos.component.html',
  styleUrls: ['./view-selected-establishment.component.scss'],
})
export class ViewSelectedEstablishmentInfosComponent implements OnInit {
  @Input() establishment?: IEstablishment;
  photos?: Photo[];
  manager?: Manage;
  nginxAddress: String = 'http://89.3.69.138:8085';
  eventSubscriber?: Subscription;
  nbRating?: Number;
  accessToken: String;

  constructor(
    protected establishmenteService: EstablishmentService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute,
    private photoService: PhotoService,
    private manageService: ManageService,
    private ratingService: RatingService
  ) {
    this.accessToken = 'pk.eyJ1IjoiZG9sbHkyMDAzIiwiYSI6ImNraTlqenZwaDBnY3Eycm85dTh0aThiN28ifQ.R4DWvg3xOfB2xTt9HQptEA';
  }

  ngOnInit(): void {
    let id: number;
    this.photos = [];
    if (this.establishment !== undefined && this.establishment.id !== undefined && this.photos !== undefined) {
      id = this.establishment.id;
      this.photoService.findByEstablishment(id).subscribe((res: HttpResponse<IPhoto[]>) => (this.photos = res.body || []));
      this.manageService.findEstablishmentManager(id).subscribe((res: HttpResponse<Manage>) => (this.manager = res.body || undefined));
      this.ratingService.count(id).subscribe((res: HttpResponse<Number>) => (this.nbRating = res.body || undefined));
    }
  }
}
