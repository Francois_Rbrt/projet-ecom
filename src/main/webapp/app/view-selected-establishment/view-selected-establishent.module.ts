import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { viewSelectedEstablishmentRoute } from './view-selected-establishment.route';
import { ViewSelectedEstablishmentComponent } from './view-selected-establishment.component';
import { ViewSelectedEstablishmentInfosComponent } from 'app/view-selected-establishment/view-selected-establishment-infos.component';
import { SingleHousingTemplateComponent } from './single-housing-template/single-housing-template.component';
import { SelectedEstablishmentService } from 'app/view-selected-establishment/selected-establishment.service';
import { AfaleFiltersModule } from 'app/filters/filters.module';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(viewSelectedEstablishmentRoute), AfaleFiltersModule],
  declarations: [ViewSelectedEstablishmentComponent, ViewSelectedEstablishmentInfosComponent, SingleHousingTemplateComponent],
  exports: [ViewSelectedEstablishmentComponent, SingleHousingTemplateComponent],
})
export class AfaleViewSelectedEstablishmentModule {}
