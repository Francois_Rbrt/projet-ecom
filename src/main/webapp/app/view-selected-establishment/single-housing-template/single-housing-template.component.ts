import { Component, Input, OnInit } from '@angular/core';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { SelectedEstablishmentService } from 'app/view-selected-establishment/selected-establishment.service';
import { ContainsBedRooms, IContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';
import { ContainsBathRooms, IContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';
import { ContainsBedRoomsService } from 'app/entities/contains-bed-rooms/contains-bed-rooms.service';
import { ContainsBathRoomsService } from 'app/entities/contains-bath-rooms/contains-bath-rooms.service';
import { HttpResponse } from '@angular/common/http';
import { BookingService } from 'app/entities/booking/booking.service';
import { FiltersService } from 'app/filters/filters.service';

@Component({
  selector: 'jhi-single-housing-template',
  templateUrl: './single-housing-template.component.html',
  styleUrls: ['./single-housing-template.component.scss'],
})
export class SingleHousingTemplateComponent implements OnInit {
  @Input() housingTemplate: IHousingTemplate | undefined;
  @Input() index: number;
  isSelected: boolean;
  nbSelected: number;
  validated: boolean;
  //TODO : attribut qui dit combien il y a de cet exemplaire de libre atm (need date en entrée )
  bedroomsContained?: IContainsBedRooms[];
  bathroomsContained?: IContainsBathRooms[];
  dispoBambi?: Number;

  constructor(
    public selectedEstablishmentService: SelectedEstablishmentService,
    private containBedroomsService: ContainsBedRoomsService,
    private containsBathroomsService: ContainsBathRoomsService,
    private bookingService: BookingService,
    private filterService: FiltersService
  ) {
    this.isSelected = false;
    this.nbSelected = 1;
    this.validated = false;
    this.index = 1;
  }

  ngOnInit(): void {
    //liste des bedroom
    if (this.housingTemplate !== undefined && this.housingTemplate.id !== undefined) {
      this.containBedroomsService
        .findHousingBedrooms(this.housingTemplate.id)
        .subscribe((res: HttpResponse<IContainsBedRooms[]>) => (this.bedroomsContained = res.body || []));
    }
    //liste des bathroom
    if (this.housingTemplate !== undefined && this.housingTemplate.id !== undefined) {
      this.containsBathroomsService
        .findHousingBathrooms(this.housingTemplate.id)
        .subscribe((res: HttpResponse<IContainsBathRooms[]>) => (this.bathroomsContained = res.body || []));
    }
    /*if (this.housingTemplate !== undefined && this.housingTemplate.id !== undefined) {
      this.bookingService
        .findDispo(this.housingTemplate.id,this.filterService.start,this.filterService.end)
        .subscribe((res: HttpResponse<Number>) => (this.dispoBambi = res.body || undefined));
    }*/
  }
  getcolor(): string {
    if (this.isSelected) return '#9dc8dd';
    else return '';
  }

  switchSelection(): void {
    if (this.nbSelected === 0) {
      this.nbSelected++;
      this.selectedEstablishmentService.increaseNbSelected(this.index);
    }
    this.isSelected = !this.isSelected;
    this.selectedEstablishmentService.switchSelection(this.index);
    if (
      this.selectedEstablishmentService.housingTemplateListNb !== undefined &&
      this.selectedEstablishmentService.housingTemplateList !== undefined &&
      this.selectedEstablishmentService.housingTemplateListSelected !== undefined
    ) {
      this.selectedEstablishmentService.totalPrice = 0;
      for (let i = 0; i < this.selectedEstablishmentService.housingTemplateList.length; i++) {
        if (
          this.housingTemplate !== undefined &&
          this.housingTemplate.pricePerNight !== undefined &&
          this.selectedEstablishmentService.housingTemplateListSelected[i]
        ) {
          this.selectedEstablishmentService.totalPrice =
            this.selectedEstablishmentService.totalPrice +
            this.selectedEstablishmentService.housingTemplateListNb[i] * this.housingTemplate.pricePerNight;
        }
      }
    }
  }
  //a enlever si utilisation du service
  increaseNbSelected(): void {
    this.switchSelection();
    //this.selectedEstablishmentService.switchSelection(this.index);
    //besoin de faire une verif : est ce que l'utilisateur n'essaye ps de selectionner plus que ce qui est dispo ?
    this.nbSelected++;
    this.selectedEstablishmentService.increaseNbSelected(this.index);
    if (
      this.selectedEstablishmentService.housingTemplateListNb !== undefined &&
      this.selectedEstablishmentService.housingTemplateList !== undefined &&
      this.selectedEstablishmentService.housingTemplateListSelected !== undefined
    ) {
      this.selectedEstablishmentService.totalPrice = 0;
      for (let i = 0; i < this.selectedEstablishmentService.housingTemplateList.length; i++) {
        if (
          this.housingTemplate !== undefined &&
          this.housingTemplate.pricePerNight !== undefined &&
          this.selectedEstablishmentService.housingTemplateListSelected[i]
        ) {
          this.selectedEstablishmentService.totalPrice =
            this.selectedEstablishmentService.totalPrice +
            this.selectedEstablishmentService.housingTemplateListNb[i] * this.housingTemplate.pricePerNight;
        }
      }
    }
  }

  decreaseNbSelected(): void {
    this.nbSelected--;
    this.selectedEstablishmentService.decreaseNbSelected(this.index);
    if (this.nbSelected > 0) {
      this.switchSelection();
      //this.selectedEstablishmentService.switchSelection(this.index);
    }
    if (
      this.selectedEstablishmentService.housingTemplateListNb !== undefined &&
      this.selectedEstablishmentService.housingTemplateList !== undefined &&
      this.selectedEstablishmentService.housingTemplateListSelected !== undefined
    ) {
      this.selectedEstablishmentService.totalPrice = 0;
      for (let i = 0; i < this.selectedEstablishmentService.housingTemplateList.length; i++) {
        if (
          this.housingTemplate !== undefined &&
          this.housingTemplate.pricePerNight !== undefined &&
          this.selectedEstablishmentService.housingTemplateListSelected[i]
        ) {
          this.selectedEstablishmentService.totalPrice =
            this.selectedEstablishmentService.totalPrice +
            this.selectedEstablishmentService.housingTemplateListNb[i] * this.housingTemplate.pricePerNight;
        }
      }
    }
  }
}
