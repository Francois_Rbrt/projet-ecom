import { Component, OnDestroy, OnInit } from '@angular/core';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { BehaviorSubject, Subscription } from 'rxjs';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { SelectedEstablishmentService } from 'app/view-selected-establishment/selected-establishment.service';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { SingleHousingTemplateComponent } from 'app/view-selected-establishment/single-housing-template/single-housing-template.component';
import { FiltersService } from 'app/filters/filters.service';
import { FiltersComponent } from 'app/filters/filters.component';
import { BookingService } from 'app/entities/booking/booking.service';

@Component({
  selector: 'jhi-view-selected-establishment',
  templateUrl: './view-selected-establishment.component.html',
  styleUrls: ['./view-selected-establishment.component.scss'],
})
export class ViewSelectedEstablishmentComponent implements OnInit, OnDestroy {
  public establishmentId: number;
  establishment?: IEstablishment;
  eventSubscriber?: Subscription;
  isInit?: boolean;

  constructor(
    protected housingTemplateService: HousingTemplateService,
    public selectedEstablishmentService: SelectedEstablishmentService,
    private filterService: FiltersService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected bookingService: BookingService,
    protected activatedRoute: ActivatedRoute
  ) {
    this.establishmentId = this.activatedRoute.snapshot.params['establishmentSelected'];
  }
  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ establishment }) => (this.establishment = establishment));
    this.selectedEstablishmentService.init(this.establishmentId);
    this.isInit = false;
  }

  ngOnDestroy(): void {}

  trackId(index: number, item: IHousingTemplate): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
  loadAll(): void {}

  registerChangeInHousingTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('housingTemplatesListListModification', () => this.loadAll());
  }

  registerBooking(): void {
    this.selectedEstablishmentService.registerBooking();
  }

  onStartChange($event: any): void {
    //this.filterService.
  }
}
