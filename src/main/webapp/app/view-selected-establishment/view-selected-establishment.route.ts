import { ActivatedRouteSnapshot, Resolve, Route, Router, Routes } from '@angular/router';

import { ViewSelectedEstablishmentComponent } from './view-selected-establishment.component';
import { Injectable } from '@angular/core';
import { HousingTemplate, IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { EMPTY, Observable, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Establishment, IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentResolve } from 'app/entities/establishment/establishment.route';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Authority } from 'app/shared/constants/authority.constants';

@Injectable({ providedIn: 'root' })
export class HousingTemplateResolve implements Resolve<IHousingTemplate> {
  constructor(private service: HousingTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHousingTemplate> | Observable<never> {
    const id = route.params['establishmentSelected'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((housingTemplate: HttpResponse<HousingTemplate>) => {
          if (housingTemplate.body) {
            return of(housingTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new HousingTemplate());
  }
}

@Injectable({ providedIn: 'root' })
export class EstablishmentDetailResolve implements Resolve<IEstablishment> {
  constructor(private service: EstablishmentService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEstablishment> | Observable<never> {
    const id = route.params['establishmentSelected'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((establishment: HttpResponse<Establishment>) => {
          if (establishment.body) {
            return of(establishment.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Establishment());
  }
}

export const viewSelectedEstablishmentRoute: Routes = [
  {
    path: '',
    component: ViewSelectedEstablishmentComponent,
    resolve: {
      establishment: EstablishmentDetailResolve,
    },
    data: {
      authorities: [Authority.ADMIN, Authority.USER],
      pageTitle: '',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':establishmentSelected',
    component: ViewSelectedEstablishmentComponent,
    resolve: {
      establishment: EstablishmentDetailResolve,
    },
    data: {
      authorities: [Authority.ADMIN, Authority.USER],
      pageTitle: '',
    },
    canActivate: [UserRouteAccessService],
  },
];
