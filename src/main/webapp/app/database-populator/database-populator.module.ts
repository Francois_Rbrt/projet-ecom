import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { DatabasePopulatorComponent } from './database-populator.component';
import { databasePopulatorRoute } from './database-populator.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(databasePopulatorRoute)],
  declarations: [DatabasePopulatorComponent],
  exports: [DatabasePopulatorComponent],
})
export class AfaleDatabasePopulatorModule {}
