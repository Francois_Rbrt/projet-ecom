import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { IManage } from '../shared/model/manage.model';

@Injectable({ providedIn: 'root' })
export class DatabasePopulatorService {
  public resourceUrl = SERVER_API_URL + 'api/init';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/init';

  constructor(protected http: HttpClient) {}

  initDB(): void {
    this.http.get(this.resourceUrl, { observe: 'response' });
  }
  initDBId(): Observable<HttpResponse<void>> {
    const id = 1;
    return this.http.get<void>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
