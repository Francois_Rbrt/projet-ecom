import { Route, Routes } from '@angular/router';
import { Authority } from 'app/shared/constants/authority.constants';

import { DatabasePopulatorComponent } from './database-populator.component';

export const databasePopulatorRoute: Routes = [
  {
    path: '',
    component: DatabasePopulatorComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'DataBase Populator',
    },
  },
];
