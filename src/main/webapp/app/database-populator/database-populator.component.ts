import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IManage } from 'app/shared/model/manage.model';
import { DatabasePopulatorService } from 'app/database-populator/database-populator.service';

@Component({
  selector: 'jhi-database-populator',
  templateUrl: './database-populator.component.html',
  styleUrls: ['./database-populator.component.scss'],
})
export class DatabasePopulatorComponent implements OnInit {
  manages?: IManage[];
  initStatus?: number;

  constructor(protected http: HttpClient, private databasePopulatorService: DatabasePopulatorService) {}

  ngOnInit(): void {
    //this.initDB();
    //pas compris pourquoi l'appel en dessous fait bien l'appel au backend et pas celui du dessus
    this.initDBId().subscribe((res: HttpResponse<void>) => (this.initStatus = res.status || 0));
    return;
  }

  initDB(): void {
    this.databasePopulatorService.initDB();
  }
  initDBId(): Observable<HttpResponse<void>> {
    return this.databasePopulatorService.initDBId();
  }
}
