import { Component, Input, OnInit } from '@angular/core';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { HttpResponse } from '@angular/common/http';
import { IPhoto, Photo } from 'app/shared/model/photo.model';
import { PhotoService } from 'app/entities/photo/photo.service';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

@Component({
  selector: 'jhi-establishment-list-element',
  templateUrl: './establishment-list-element.component.html',
  styleUrls: ['./establishment-list-element.component.scss'],
})
export class EstablishmentListElementComponent implements OnInit {
  @Input() establishment?: IEstablishment;
  photos?: Photo[];
  nginxAddress: String = 'http://89.3.69.138:8085';
  minPrice?: Number;
  constructor(private photoService: PhotoService, private establishmentService: EstablishmentService) {}

  ngOnInit(): void {
    let id: number;
    this.photos = [];
    if (this.establishment !== undefined && this.establishment.id !== undefined && this.photos !== undefined) {
      id = this.establishment.id;
      this.photoService.findByEstablishment(id).subscribe((res: HttpResponse<IPhoto[]>) => (this.photos = res.body || []));
      this.establishmentService.findMinPrice(id).subscribe((res: HttpResponse<Number>) => (this.minPrice = res.body || undefined));
    }
  }
}
