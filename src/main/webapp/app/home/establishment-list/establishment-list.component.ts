import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IEstablishment } from 'app/shared/model/establishment.model';

import { EstablishmentService } from 'app/entities/establishment/establishment.service';
import { Page } from 'app/shared/model/Page.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { PhotoService } from 'app/entities/photo/photo.service';
import { IPhoto, Photo } from 'app/shared/model/photo.model';

@Component({
  selector: 'jhi-establishment-list',
  templateUrl: './establishment-list.component.html',
  styleUrls: ['./establishment-list.component.scss'],
})
export class EstablishmentListComponent implements OnInit {
  establishments?: Page<IEstablishment>;
  numberElement?: number;

  constructor(private establishmentService: EstablishmentService) {
    this.numberElement = ITEMS_PER_PAGE;
  }

  ngOnInit(): void {
    this.loadFirst();
  }

  loadFirst(): void {
    if (this.numberElement !== undefined) {
      this.establishmentService
        .findPage(0, this.numberElement)
        .subscribe((res: HttpResponse<Page<IEstablishment>>) => (this.establishments = res.body || new Page<IEstablishment>()));
    }
  }

  loadPrevious(): void {
    if (this.numberElement !== undefined && this.establishments !== undefined && this.establishments.number !== undefined) {
      this.establishmentService
        .findPage(this.establishments.number - 1, this.numberElement)
        .subscribe((res: HttpResponse<Page<IEstablishment>>) => (this.establishments = res.body || new Page<IEstablishment>()));
    }
  }

  loadNext(): void {
    if (this.numberElement !== undefined && this.establishments !== undefined && this.establishments.number !== undefined) {
      this.establishmentService
        .findPage(this.establishments.number + 1, this.numberElement)
        .subscribe((res: HttpResponse<Page<IEstablishment>>) => (this.establishments = res.body || new Page<IEstablishment>()));
    }
  }

  loadLast(): void {
    if (this.numberElement !== undefined && this.establishments !== undefined && this.establishments.totalPages !== undefined) {
      this.establishmentService
        .findPage(this.establishments.totalPages - 1, this.numberElement)
        .subscribe((res: HttpResponse<Page<IEstablishment>>) => (this.establishments = res.body || new Page<IEstablishment>()));
    }
  }

  switch(number: number): void {
    this.numberElement = number;
    this.loadFirst();
  }
}
