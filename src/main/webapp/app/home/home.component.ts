import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { IManage } from 'app/shared/model/manage.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  establishments?: IEstablishment[];
  isInit?: boolean;
  //for init database

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private establishmentService: EstablishmentService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.isInit = false;
    this.loadAll();
  }

  loadAll(): void {
    this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
