import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { AfaleFiltersModule } from 'app/filters/filters.module';
import { EstablishmentListComponent } from 'app/home/establishment-list/establishment-list.component';
import { EstablishmentListElementComponent } from './establishment-list-element/establishment-list-element.component';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(HOME_ROUTE), AfaleFiltersModule],
  declarations: [HomeComponent, EstablishmentListComponent, EstablishmentListElementComponent],
  exports: [HomeComponent],
})
export class AfaleHomeModule {}
