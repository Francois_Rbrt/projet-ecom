import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { FiltersComponent } from './filters.component';
import { filtersRoute } from './filters.route';
import { AfaleViewSelectedEstablishmentModule } from 'app/view-selected-establishment/view-selected-establishent.module';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild([filtersRoute])],
  declarations: [FiltersComponent],
  exports: [FiltersComponent],
})
export class AfaleFiltersModule {}
