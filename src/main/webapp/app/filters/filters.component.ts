import { Component, Input, OnInit, Output } from '@angular/core';
import { FiltersService } from 'app/filters/filters.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'jhi-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  establishmentSelected?: number;
  today: Date;
  start: Date;
  end: Date;
  nbTraveler: number;
  nbRoom: number;
  @Output() isInit: EventEmitter<boolean> = new EventEmitter<boolean>();
  isI: boolean;

  constructor(private filterService: FiltersService) {
    this.today = new Date();
    this.start = new Date();
    this.end = new Date();
    this.nbTraveler = 0;
    this.nbRoom = 0;
    this.isI = this.filterService.isInit;
  }

  ngOnInit(): void {
    this.establishmentSelected = this.filterService.establishmentSelected;
    this.start = this.filterService.start;
    this.end = this.filterService.end;
    this.nbTraveler = this.filterService.nbTraveler;
    this.nbRoom = this.filterService.nbRoom;
    if (this.isI === true) {
      this.isInit.emit(true);
    }
  }

  getStart(): Date {
    return this.start;
  }

  getEnd(): Date {
    return this.end;
  }

  onSearch(): void {
    this.filterService.setStart(this.start);
    this.filterService.setEnd(this.end);
    this.filterService.setNbTraveler(this.nbTraveler);
    this.filterService.setNbRoom(this.nbRoom);
    if (this.establishmentSelected !== undefined) {
      this.filterService.setEstablishment(this.establishmentSelected);
    }
    this.isI = true;
    this.filterService.isInit = true;
    this.isInit.emit(true);
  }

  onStartChange(date: Date): void {
    this.start = date;
    this.end = new Date(this.start);
    this.end.setDate(this.end.getDate() + 1);
  }

  onEndChange(date: Date): void {
    this.end = date;
  }
}
