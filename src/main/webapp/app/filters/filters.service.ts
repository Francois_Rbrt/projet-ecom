import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FiltersService {
  establishmentSelected?: number;
  today: Date;
  start: Date;
  end: Date;
  nbTraveler: number;
  nbRoom: number;
  isInit: boolean;

  constructor() {
    this.today = new Date();
    this.start = new Date();
    this.end = new Date();
    this.end.setDate(this.end.getDate() + 1);
    this.nbTraveler = 1;
    this.nbRoom = 1;
    this.isInit = false;
  }

  setEstablishment(id: number): void {
    this.establishmentSelected = id;
  }

  setStart(start: Date): void {
    this.start = start;
  }

  setEnd(end: Date): void {
    this.end = end;
  }

  setNbTraveler(travelers: number): void {
    this.nbTraveler = travelers;
  }

  setNbRoom(room: number): void {
    this.nbRoom = room;
  }
}
