import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillingPageComponent } from './billing-page.component';
import { EditCardComponent } from './edit-card/edit-card.component';
import { CardnumberValidatorDirective } from './cardnumber-validator.directive';
import { FormsModule } from '@angular/forms';
import { AfaleSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { BILLINGPAGE_ROUTE } from 'app/billing-page/billing-page.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild([BILLINGPAGE_ROUTE]), FormsModule],
  declarations: [BillingPageComponent, EditCardComponent, CardnumberValidatorDirective],
  exports: [BillingPageComponent],
})
export class BillingPageModule {}
