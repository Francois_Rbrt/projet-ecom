import { Directive } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { ValidateFn } from 'codelyzer/walkerFactory/walkerFn';

@Directive({
  selector: '[jhiCardnumberValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      useExisting: CardnumberValidatorDirective,
      multi: true,
    },
  ],
})
export class CardnumberValidatorDirective implements Validator {
  validator: (c: AbstractControl) => { cardnumbervalidator: { valid: boolean } } | null;

  constructor() {
    this.validator = this.cardNumberValidator();
  }

  validate(c: FormControl): any {
    return this.validator(c);
  }

  cardNumberValidator(): (c: AbstractControl) => null | { cardnumbervalidator: { valid: boolean } } {
    return (c: AbstractControl) => {
      const isValid = /^(\d{4}-){3}\d{4}$|^(\d{4} ){3}\d{4}$|^\d{16}$/.test(c.value);
      if (isValid) {
        return null;
      } else {
        return {
          cardnumbervalidator: { valid: false },
        };
      }
    };
  }
}
