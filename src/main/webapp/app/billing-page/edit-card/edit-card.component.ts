import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Card, ICard } from '../../shared/model/card.model';
import { SelectedEstablishmentService } from 'app/view-selected-establishment/selected-establishment.service';
import { HttpResponse } from '@angular/common/http';
import { IContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';

@Component({
  selector: 'jhi-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.scss'],
})
export class EditCardComponent implements OnInit {
  model: Card;

  constructor(public selectedEstablishmentService: SelectedEstablishmentService) {
    this.model = new Card(null, null, null, null, null);
  }

  ngOnInit(): void {}

  onSubmit(form: NgForm): void {
    //console.log(form.value);
  }

  validate(): void {
    this.selectedEstablishmentService.validate();
  }
}

// onSubmit(form: NgForm): void { this.submitted = true; }
