import { Route } from '@angular/router';

import { BillingPageComponent } from './billing-page.component';
import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

export const BILLINGPAGE_ROUTE: Route = {
  path: '',
  component: BillingPageComponent,
  data: {
    authorities: [Authority.ADMIN, Authority.USER],
    pageTitle: '',
  },
  canActivate: [UserRouteAccessService],
};
