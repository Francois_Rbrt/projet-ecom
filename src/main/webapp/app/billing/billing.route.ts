import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, Routes } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { Booking, IBooking } from '../shared/model/booking.model';
import { BookingService } from '../entities/booking/booking.service';
import { flatMap } from 'rxjs/operators';
import { BillingComponent } from './billing.component';

@Injectable({ providedIn: 'root' })
export class BookingResolve implements Resolve<IBooking> {
  constructor(private service: BookingService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBooking> | Observable<never> {
    const id = route.params['bookingSelected'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((booking: HttpResponse<Booking>) => {
          if (booking.body) {
            return of(booking.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Booking());
  }
}

export const BillingRoute: Routes = [
  {
    path: '',
    component: BillingComponent,
    resolve: {
      booking: BookingResolve,
    },
  },
];
