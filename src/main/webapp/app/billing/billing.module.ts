import { NgModule } from '@angular/core';
import { BookingComponent } from 'app/entities/booking/booking.component';
import { AfaleSharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { BillingRoute } from './billing.route';
import { BillingComponent } from './billing.component';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(BillingRoute)],
  declarations: [],
})
export class BillingModule {}
