import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookingService } from 'app/entities/booking/booking.service';
import { IBooking } from 'app/shared/model/booking.model';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { BookingElement, IBookingElement } from 'app/shared/model/booking-element.model';
import { BookingElementService } from 'app/entities/booking-element/booking-element.service';

@Component({
  selector: 'jhi-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss'],
})
export class BillingComponent implements OnInit, OnDestroy {
  booking?: IBooking[];
  bookingElements: IBookingElement[];
  bookingId: number;
  eventSubscriber?: Subscription;

  constructor(
    protected bookingService: BookingService,
    protected bookingElementService: BookingElementService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute
  ) {
    this.bookingId = this.activatedRoute.snapshot.params['bookingSelected'];
    this.bookingElements = [];
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ booking }) => (this.booking = booking));
    this.loadAll();
  }
  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }
  loadAll(): void {
    this.bookingElementService
      .findByBookingId(this.bookingId)
      .subscribe((res: HttpResponse<IBookingElement[]>) => (this.bookingElements = res.body || []));
  }
}
