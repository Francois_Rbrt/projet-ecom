import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { IBedRoomTemplate } from 'app/shared/model/bed-room-template.model';

export interface IContainsBedRooms {
  id?: number;
  nbOfUnit?: number;
  housingTemplateId?: IHousingTemplate;
  bedroomTemplateId?: IBedRoomTemplate;
}

export class ContainsBedRooms implements IContainsBedRooms {
  constructor(
    public id?: number,
    public nbOfUnit?: number,
    public housingTemplateId?: IHousingTemplate,
    public bedroomTemplateId?: IBedRoomTemplate
  ) {}
}
