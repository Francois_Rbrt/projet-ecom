export interface IBathRoomTemplate {
  id?: number;
  hasBath?: boolean;
  hasToilet?: boolean;
  hasShower?: boolean;
}

export class BathRoomTemplate implements IBathRoomTemplate {
  constructor(public id?: number, public hasBath?: boolean, public hasToilet?: boolean, public hasShower?: boolean) {
    this.hasBath = this.hasBath || false;
    this.hasToilet = this.hasToilet || false;
    this.hasShower = this.hasShower || false;
  }
}
