import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IEstablishment } from 'app/shared/model/establishment.model';

export interface IRating {
  id?: number;
  rate?: number;
  comment?: string;
  ratingDate?: Moment;
  userId?: IUser;
  establishmentId?: IEstablishment;
}

export class Rating implements IRating {
  constructor(
    public id?: number,
    public rate?: number,
    public comment?: string,
    public ratingDate?: Moment,
    public userId?: IUser,
    public establishmentId?: IEstablishment
  ) {}
}
