import { IUser } from 'app/core/user/user.model';
import { IEstablishment } from 'app/shared/model/establishment.model';

export interface IBooking {
  id?: number;
  email?: string;
  totalPrice?: number;
  comment?: string;
  validate?: boolean;
  jhiUserId?: IUser;
  establishmentId?: IEstablishment;
}

export class Booking implements IBooking {
  constructor(
    public id?: number,
    public email?: string,
    public totalPrice?: number,
    public comment?: string,
    public validate?: boolean,
    public jhiUserId?: IUser,
    public establishmentId?: IEstablishment
  ) {
    this.validate = this.validate || false;
  }
}
