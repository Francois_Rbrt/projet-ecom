import { IEstablishment } from 'app/shared/model/establishment.model';

export interface IPage<T> {
  content?: IEstablishment[];
  last?: boolean;
  totalElements?: number;
  totalPages?: number;
  size?: number;
  number?: number;
  first?: boolean;
  numberOfElements?: number;
}

export class Page<T> implements IPage<T> {
  public constructor(
    public content?: IEstablishment[],
    public last?: boolean,
    public totalElements?: number,
    public totalPages?: number,
    public size?: number,
    public number?: number,
    public first?: boolean,
    public numberOfElements?: number
  ) {}
}
