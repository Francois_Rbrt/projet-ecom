import { Moment } from 'moment';
import { IBooking } from 'app/shared/model/booking.model';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';

export interface IBookingElement {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  elementPrice?: number;
  bookingId?: IBooking;
  housingTemplateId?: IHousingTemplate;
}

export class BookingElement implements IBookingElement {
  constructor(
    public id?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public elementPrice?: number,
    public bookingId?: IBooking,
    public housingTemplateId?: IHousingTemplate
  ) {}
}
