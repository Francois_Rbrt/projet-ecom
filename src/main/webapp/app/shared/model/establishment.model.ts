import { EstablishmentType } from 'app/shared/model/enumerations/establishment-type.model';

export interface IEstablishment {
  id?: number;
  name?: string;
  adress?: string;
  latitude?: number;
  longitude?: number;
  globalRate?: number;
  establishmentType?: EstablishmentType;
  hasParking?: boolean;
  hasRestaurant?: boolean;
  hasFreeWifi?: boolean;
  hasSwimmingPool?: boolean;
  description?: string;
}

export class Establishment implements IEstablishment {
  constructor(
    public id?: number,
    public name?: string,
    public adress?: string,
    public latitude?: number,
    public longitude?: number,
    public globalRate?: number,
    public establishmentType?: EstablishmentType,
    public hasParking?: boolean,
    public hasRestaurant?: boolean,
    public hasFreeWifi?: boolean,
    public hasSwimmingPool?: boolean,
    public description?: string
  ) {
    this.hasParking = this.hasParking || false;
    this.hasRestaurant = this.hasRestaurant || false;
    this.hasFreeWifi = this.hasFreeWifi || false;
    this.hasSwimmingPool = this.hasSwimmingPool || false;
  }
}
