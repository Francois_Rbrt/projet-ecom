import { IUser } from 'app/core/user/user.model';
import { IEstablishment } from 'app/shared/model/establishment.model';

export interface IFavorites {
  id?: number;
  userId?: IUser;
  establishmentId?: IEstablishment;
}

export class Favorites implements IFavorites {
  constructor(public id?: number, public userId?: IUser, public establishmentId?: IEstablishment) {}
}
