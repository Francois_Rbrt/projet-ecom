import { IEstablishment } from 'app/shared/model/establishment.model';
import { HousingType } from 'app/shared/model/enumerations/housing-type.model';

export interface IHousingTemplate {
  id?: number;
  housingType?: HousingType;
  nbOfUnit?: number;
  nbMaxOfOccupants?: number;
  pricePerNight?: number;
  isNonSmoking?: boolean;
  hasKitchen?: boolean;
  hasToilets?: boolean;
  establishmentId?: IEstablishment;
}

export class HousingTemplate implements IHousingTemplate {
  constructor(
    public id?: number,
    public housingType?: HousingType,
    public nbOfUnit?: number,
    public nbMaxOfOccupants?: number,
    public pricePerNight?: number,
    public isNonSmoking?: boolean,
    public hasKitchen?: boolean,
    public hasToilets?: boolean,
    public establishmentId?: IEstablishment
  ) {
    this.isNonSmoking = this.isNonSmoking || false;
    this.hasKitchen = this.hasKitchen || false;
    this.hasToilets = this.hasToilets || false;
  }
}
