import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { IBathRoomTemplate } from 'app/shared/model/bath-room-template.model';

export interface IContainsBathRooms {
  id?: number;
  nbOfUnit?: number;
  housingTemplateId?: IHousingTemplate;
  bathroomTemplateId?: IBathRoomTemplate;
}

export class ContainsBathRooms implements IContainsBathRooms {
  constructor(
    public id?: number,
    public nbOfUnit?: number,
    public housingTemplateId?: IHousingTemplate,
    public bathroomTemplateId?: IBathRoomTemplate
  ) {}
}
