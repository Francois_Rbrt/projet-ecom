import { Moment } from 'moment';
import { IEstablishment } from 'app/shared/model/establishment.model';

export interface IEstablishmentClosure {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  cause?: string;
  establishmentId?: IEstablishment;
}

export class EstablishmentClosure implements IEstablishmentClosure {
  constructor(
    public id?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public cause?: string,
    public establishmentId?: IEstablishment
  ) {}
}
