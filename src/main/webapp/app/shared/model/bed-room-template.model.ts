export interface IBedRoomTemplate {
  id?: number;
  nbSingleBeds?: number;
  nbDoubleBeds?: number;
}

export class BedRoomTemplate implements IBedRoomTemplate {
  constructor(public id?: number, public nbSingleBeds?: number, public nbDoubleBeds?: number) {}
}
