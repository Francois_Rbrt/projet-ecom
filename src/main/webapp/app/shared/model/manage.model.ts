import { IUser } from 'app/core/user/user.model';
import { IEstablishment } from 'app/shared/model/establishment.model';

export interface IManage {
  id?: number;
  userId?: IUser;
  establishmentId?: IEstablishment;
}

export class Manage implements IManage {
  constructor(public id?: number, public userId?: IUser, public establishmentId?: IEstablishment) {}
}
