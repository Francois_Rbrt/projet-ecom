import { Moment } from 'moment';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';

export interface IHousingClosure {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  cause?: string;
  establishmentId?: IHousingTemplate;
}

export class HousingClosure implements IHousingClosure {
  constructor(
    public id?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public cause?: string,
    public establishmentId?: IHousingTemplate
  ) {}
}
