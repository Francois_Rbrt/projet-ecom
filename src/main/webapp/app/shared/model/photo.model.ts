import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { IEstablishment } from 'app/shared/model/establishment.model';

export interface IPhoto {
  id?: number;
  name?: string;
  housingTemplateId?: IHousingTemplate;
  establishmentId?: IEstablishment;
}

export class Photo implements IPhoto {
  constructor(
    public id?: number,
    public name?: string,
    public housingTemplateId?: IHousingTemplate,
    public establishmentId?: IEstablishment
  ) {}
}
