export interface ICard {
  cardNumber: number | null;
  month: number | null;
  year: number | null;
  cvc: number | null;
  name: string | null;
}

export class Card implements ICard {
  constructor(
    public cardNumber: number | null,
    public month: number | null,
    public year: number | null,
    public cvc: number | null,
    public name: string | null
  ) {}
}
