import { Component, OnDestroy, OnInit } from '@angular/core';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { SelectedEstablishmentService } from 'app/view-selected-establishment/selected-establishment.service';
import { IBooking } from 'app/shared/model/booking.model';
import { IBookingElement } from 'app/shared/model/booking-element.model';
import { Subscription } from 'rxjs';
import { BookingService } from 'app/entities/booking/booking.service';
import { BookingElementService } from 'app/entities/booking-element/booking-element.service';
import { JhiEventManager } from 'ng-jhipster';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-current-booking',
  templateUrl: './current-booking.component.html',
  styleUrls: ['./current-booking.component.scss'],
})
export class CurrentBookingComponent implements OnInit, OnDestroy {
  public housingTemplateList?: IHousingTemplate[] | null;
  public housingTemplateListNb?: Array<number>;
  public housingTemplateListSelected?: Array<boolean>;
  eventSubscriber?: Subscription;
  private _htSubcription: Subscription | undefined;

  constructor(
    protected selectedEstablishmentService: SelectedEstablishmentService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute
  ) {
    this.housingTemplateList = [];
  }

  ngOnInit(): void {
    //this.loadAll();
  }

  ngOnDestroy(): void {
    if (this._htSubcription) {
      this._htSubcription.unsubscribe();
    }
  }
  /*loadAll(): void {
    this._htSubcription = this.selectedEstablishmentService.getHousingTList().subscribe((value) => {
      this.housingTemplateList = value;
    });


  }*/
}
