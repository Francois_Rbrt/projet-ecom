import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AfaleSharedModule } from 'app/shared/shared.module';
import { AfaleCoreModule } from 'app/core/core.module';
import { AfaleAppRoutingModule } from './app-routing.module';
import { AfaleHomeModule } from './home/home.module';
import { AfaleEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { BillingPageComponent } from './billing-page/billing-page.component';
import { BillingPageModule } from 'app/billing-page/billing-page.module';
import { BillingComponent } from './billing/billing.component';
import { AfaleViewSelectedEstablishmentModule } from 'app/view-selected-establishment/view-selected-establishent.module';
import { FiltersComponent } from './filters/filters.component';
import { AfaleFiltersModule } from 'app/filters/filters.module';
import { DatabasePopulatorComponent } from './database-populator/database-populator.component';
import { AfaleDatabasePopulatorModule } from 'app/database-populator/database-populator.module';
import { CurrentBookingComponent } from './current-booking/current-booking.component';

@NgModule({
  imports: [
    BrowserModule,
    AfaleSharedModule,
    AfaleCoreModule,
    AfaleHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AfaleEntityModule,
    AfaleAppRoutingModule,
    AfaleViewSelectedEstablishmentModule,
    AfaleFiltersModule,
    AfaleDatabasePopulatorModule,
    BillingPageModule,
  ],
  declarations: [
    MainComponent,
    NavbarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    BillingComponent,
    CurrentBookingComponent,
  ],
  bootstrap: [MainComponent],
  exports: [],
})
export class AfaleAppModule {}
