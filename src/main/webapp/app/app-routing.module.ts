import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { filtersRoute } from 'app/filters/filters.route';

const LAYOUT_ROUTES = [navbarRoute, filtersRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        {
          path: 'billing/:bookingSelected',
          loadChildren: () => import('./billing/billing.module').then(m => m.BillingModule),
        },
        {
          path: 'viewSelectedEstablishment/:establishmentSelected',
          loadChildren: () =>
            import('./view-selected-establishment/view-selected-establishent.module').then(m => m.AfaleViewSelectedEstablishmentModule),
        },
        {
          path: 'init',
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./database-populator/database-populator.module').then(m => m.AfaleDatabasePopulatorModule),
        },
        {
          path: 'payment',
          loadChildren: () => import('./billing-page/billing-page.module').then(m => m.BillingPageModule),
        },
        ...LAYOUT_ROUTES,
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    ),
  ],
  exports: [RouterModule],
})
export class AfaleAppRoutingModule {}
