import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { EstablishmentClosureComponent } from './establishment-closure.component';
import { EstablishmentClosureDetailComponent } from './establishment-closure-detail.component';
import { EstablishmentClosureUpdateComponent } from './establishment-closure-update.component';
import { EstablishmentClosureDeleteDialogComponent } from './establishment-closure-delete-dialog.component';
import { establishmentClosureRoute } from './establishment-closure.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(establishmentClosureRoute)],
  declarations: [
    EstablishmentClosureComponent,
    EstablishmentClosureDetailComponent,
    EstablishmentClosureUpdateComponent,
    EstablishmentClosureDeleteDialogComponent,
  ],
  entryComponents: [EstablishmentClosureDeleteDialogComponent],
})
export class AfaleEstablishmentClosureModule {}
