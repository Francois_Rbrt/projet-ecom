import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEstablishmentClosure, EstablishmentClosure } from 'app/shared/model/establishment-closure.model';
import { EstablishmentClosureService } from './establishment-closure.service';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

@Component({
  selector: 'jhi-establishment-closure-update',
  templateUrl: './establishment-closure-update.component.html',
})
export class EstablishmentClosureUpdateComponent implements OnInit {
  isSaving = false;
  establishments: IEstablishment[] = [];
  startDateDp: any;
  endDateDp: any;

  editForm = this.fb.group({
    id: [],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    cause: [null, [Validators.maxLength(250)]],
    establishmentId: [],
  });

  constructor(
    protected establishmentClosureService: EstablishmentClosureService,
    protected establishmentService: EstablishmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ establishmentClosure }) => {
      this.updateForm(establishmentClosure);

      this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
    });
  }

  updateForm(establishmentClosure: IEstablishmentClosure): void {
    this.editForm.patchValue({
      id: establishmentClosure.id,
      startDate: establishmentClosure.startDate,
      endDate: establishmentClosure.endDate,
      cause: establishmentClosure.cause,
      establishmentId: establishmentClosure.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const establishmentClosure = this.createFromForm();
    if (establishmentClosure.id !== undefined) {
      this.subscribeToSaveResponse(this.establishmentClosureService.update(establishmentClosure));
    } else {
      this.subscribeToSaveResponse(this.establishmentClosureService.create(establishmentClosure));
    }
  }

  private createFromForm(): IEstablishmentClosure {
    return {
      ...new EstablishmentClosure(),
      id: this.editForm.get(['id'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      endDate: this.editForm.get(['endDate'])!.value,
      cause: this.editForm.get(['cause'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEstablishmentClosure>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IEstablishment): any {
    return item.id;
  }
}
