import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEstablishmentClosure } from 'app/shared/model/establishment-closure.model';
import { EstablishmentClosureService } from './establishment-closure.service';
import { EstablishmentClosureDeleteDialogComponent } from './establishment-closure-delete-dialog.component';

@Component({
  selector: 'jhi-establishment-closure',
  templateUrl: './establishment-closure.component.html',
})
export class EstablishmentClosureComponent implements OnInit, OnDestroy {
  establishmentClosures?: IEstablishmentClosure[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected establishmentClosureService: EstablishmentClosureService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.establishmentClosureService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IEstablishmentClosure[]>) => (this.establishmentClosures = res.body || []));
      return;
    }

    this.establishmentClosureService
      .query()
      .subscribe((res: HttpResponse<IEstablishmentClosure[]>) => (this.establishmentClosures = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInEstablishmentClosures();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IEstablishmentClosure): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInEstablishmentClosures(): void {
    this.eventSubscriber = this.eventManager.subscribe('establishmentClosureListModification', () => this.loadAll());
  }

  delete(establishmentClosure: IEstablishmentClosure): void {
    const modalRef = this.modalService.open(EstablishmentClosureDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.establishmentClosure = establishmentClosure;
  }
}
