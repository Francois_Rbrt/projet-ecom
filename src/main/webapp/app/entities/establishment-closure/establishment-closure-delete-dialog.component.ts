import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEstablishmentClosure } from 'app/shared/model/establishment-closure.model';
import { EstablishmentClosureService } from './establishment-closure.service';

@Component({
  templateUrl: './establishment-closure-delete-dialog.component.html',
})
export class EstablishmentClosureDeleteDialogComponent {
  establishmentClosure?: IEstablishmentClosure;

  constructor(
    protected establishmentClosureService: EstablishmentClosureService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.establishmentClosureService.delete(id).subscribe(() => {
      this.eventManager.broadcast('establishmentClosureListModification');
      this.activeModal.close();
    });
  }
}
