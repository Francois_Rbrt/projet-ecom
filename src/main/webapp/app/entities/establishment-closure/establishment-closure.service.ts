import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IEstablishmentClosure } from 'app/shared/model/establishment-closure.model';

type EntityResponseType = HttpResponse<IEstablishmentClosure>;
type EntityArrayResponseType = HttpResponse<IEstablishmentClosure[]>;

@Injectable({ providedIn: 'root' })
export class EstablishmentClosureService {
  public resourceUrl = SERVER_API_URL + 'api/establishment-closures';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/establishment-closures';

  constructor(protected http: HttpClient) {}

  create(establishmentClosure: IEstablishmentClosure): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(establishmentClosure);
    return this.http
      .post<IEstablishmentClosure>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(establishmentClosure: IEstablishmentClosure): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(establishmentClosure);
    return this.http
      .put<IEstablishmentClosure>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEstablishmentClosure>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEstablishmentClosure[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEstablishmentClosure[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(establishmentClosure: IEstablishmentClosure): IEstablishmentClosure {
    const copy: IEstablishmentClosure = Object.assign({}, establishmentClosure, {
      startDate:
        establishmentClosure.startDate && establishmentClosure.startDate.isValid()
          ? establishmentClosure.startDate.format(DATE_FORMAT)
          : undefined,
      endDate:
        establishmentClosure.endDate && establishmentClosure.endDate.isValid()
          ? establishmentClosure.endDate.format(DATE_FORMAT)
          : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? moment(res.body.startDate) : undefined;
      res.body.endDate = res.body.endDate ? moment(res.body.endDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((establishmentClosure: IEstablishmentClosure) => {
        establishmentClosure.startDate = establishmentClosure.startDate ? moment(establishmentClosure.startDate) : undefined;
        establishmentClosure.endDate = establishmentClosure.endDate ? moment(establishmentClosure.endDate) : undefined;
      });
    }
    return res;
  }
}
