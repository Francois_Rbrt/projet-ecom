import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEstablishmentClosure } from 'app/shared/model/establishment-closure.model';

@Component({
  selector: 'jhi-establishment-closure-detail',
  templateUrl: './establishment-closure-detail.component.html',
})
export class EstablishmentClosureDetailComponent implements OnInit {
  establishmentClosure: IEstablishmentClosure | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ establishmentClosure }) => (this.establishmentClosure = establishmentClosure));
  }

  previousState(): void {
    window.history.back();
  }
}
