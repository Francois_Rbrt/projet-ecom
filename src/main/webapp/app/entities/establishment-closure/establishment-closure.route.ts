import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEstablishmentClosure, EstablishmentClosure } from 'app/shared/model/establishment-closure.model';
import { EstablishmentClosureService } from './establishment-closure.service';
import { EstablishmentClosureComponent } from './establishment-closure.component';
import { EstablishmentClosureDetailComponent } from './establishment-closure-detail.component';
import { EstablishmentClosureUpdateComponent } from './establishment-closure-update.component';

@Injectable({ providedIn: 'root' })
export class EstablishmentClosureResolve implements Resolve<IEstablishmentClosure> {
  constructor(private service: EstablishmentClosureService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEstablishmentClosure> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((establishmentClosure: HttpResponse<EstablishmentClosure>) => {
          if (establishmentClosure.body) {
            return of(establishmentClosure.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EstablishmentClosure());
  }
}

export const establishmentClosureRoute: Routes = [
  {
    path: '',
    component: EstablishmentClosureComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishmentClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EstablishmentClosureDetailComponent,
    resolve: {
      establishmentClosure: EstablishmentClosureResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishmentClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EstablishmentClosureUpdateComponent,
    resolve: {
      establishmentClosure: EstablishmentClosureResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishmentClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EstablishmentClosureUpdateComponent,
    resolve: {
      establishmentClosure: EstablishmentClosureResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishmentClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
