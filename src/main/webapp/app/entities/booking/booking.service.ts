import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Moment } from 'moment';
import * as moment from 'moment';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IBooking } from 'app/shared/model/booking.model';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

type EntityResponseType = HttpResponse<IBooking>;
type EntityArrayResponseType = HttpResponse<IBooking[]>;

@Injectable({ providedIn: 'root' })
export class BookingService {
  public resourceUrl = SERVER_API_URL + 'api/bookings';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/bookings';

  constructor(protected http: HttpClient) {}

  create(booking: IBooking): Observable<EntityResponseType> {
    return this.http.post<IBooking>(this.resourceUrl, booking, { observe: 'response' });
  }

  verify(booking: IBooking): Observable<EntityResponseType> {
    return this.http.post<IBooking>(`${this.resourceUrl}/verify`, booking, { observe: 'response' });
  }

  findDispo(id: number, start: Date, end: Date): Observable<HttpResponse<Number>> {
    return this.http.get<Number>(
      `${
        this.resourceUrl
      }/dispo/${id}&${start.getDay()}/${start.getMonth()}/${start.getFullYear()}&${end.getDay()}/${end.getMonth()}/${end.getFullYear()}`,
      { observe: 'response' }
    );
  }

  update(booking: IBooking): Observable<EntityResponseType> {
    return this.http.put<IBooking>(this.resourceUrl, booking, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBooking>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findBooking(BookingId: number): Observable<EntityArrayResponseType> {
    return this.http.get<IBooking[]>(`${this.resourceUrl}/booking_id/${BookingId}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBooking[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBooking[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
