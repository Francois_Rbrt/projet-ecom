import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBooking, Booking } from 'app/shared/model/booking.model';
import { BookingService } from './booking.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

type SelectableEntity = IUser | IEstablishment;

@Component({
  selector: 'jhi-booking-update',
  templateUrl: './booking-update.component.html',
})
export class BookingUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  establishments: IEstablishment[] = [];

  editForm = this.fb.group({
    id: [],
    email: [null, [Validators.required]],
    totalPrice: [null, [Validators.required]],
    comment: [null, [Validators.maxLength(500)]],
    validate: [null, [Validators.required]],
    jhiUserId: [],
    establishmentId: [],
  });

  constructor(
    protected bookingService: BookingService,
    protected userService: UserService,
    protected establishmentService: EstablishmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ booking }) => {
      this.updateForm(booking);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
    });
  }

  updateForm(booking: IBooking): void {
    this.editForm.patchValue({
      id: booking.id,
      email: booking.email,
      totalPrice: booking.totalPrice,
      comment: booking.comment,
      validate: booking.validate,
      jhiUserId: booking.jhiUserId,
      establishmentId: booking.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const booking = this.createFromForm();
    if (booking.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingService.update(booking));
    } else {
      this.subscribeToSaveResponse(this.bookingService.create(booking));
    }
  }

  private createFromForm(): IBooking {
    return {
      ...new Booking(),
      id: this.editForm.get(['id'])!.value,
      email: this.editForm.get(['email'])!.value,
      totalPrice: this.editForm.get(['totalPrice'])!.value,
      comment: this.editForm.get(['comment'])!.value,
      validate: this.editForm.get(['validate'])!.value,
      jhiUserId: this.editForm.get(['jhiUserId'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBooking>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
