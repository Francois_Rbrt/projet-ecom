import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPhoto, Photo } from 'app/shared/model/photo.model';
import { PhotoService } from './photo.service';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

type SelectableEntity = IHousingTemplate | IEstablishment;

@Component({
  selector: 'jhi-photo-update',
  templateUrl: './photo-update.component.html',
})
export class PhotoUpdateComponent implements OnInit {
  isSaving = false;
  housingtemplates: IHousingTemplate[] = [];
  establishments: IEstablishment[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.minLength(1)]],
    housingTemplateId: [],
    establishmentId: [],
  });

  constructor(
    protected photoService: PhotoService,
    protected housingTemplateService: HousingTemplateService,
    protected establishmentService: EstablishmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ photo }) => {
      this.updateForm(photo);

      this.housingTemplateService.query().subscribe((res: HttpResponse<IHousingTemplate[]>) => (this.housingtemplates = res.body || []));

      this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
    });
  }

  updateForm(photo: IPhoto): void {
    this.editForm.patchValue({
      id: photo.id,
      name: photo.name,
      housingTemplateId: photo.housingTemplateId,
      establishmentId: photo.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const photo = this.createFromForm();
    if (photo.id !== undefined) {
      this.subscribeToSaveResponse(this.photoService.update(photo));
    } else {
      this.subscribeToSaveResponse(this.photoService.create(photo));
    }
  }

  private createFromForm(): IPhoto {
    return {
      ...new Photo(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      housingTemplateId: this.editForm.get(['housingTemplateId'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhoto>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
