import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRating, Rating } from 'app/shared/model/rating.model';
import { RatingService } from './rating.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

type SelectableEntity = IUser | IEstablishment;

@Component({
  selector: 'jhi-rating-update',
  templateUrl: './rating-update.component.html',
})
export class RatingUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  establishments: IEstablishment[] = [];
  ratingDateDp: any;

  editForm = this.fb.group({
    id: [],
    rate: [null, [Validators.required, Validators.min(1), Validators.max(5)]],
    comment: [null, [Validators.maxLength(250)]],
    ratingDate: [null, [Validators.required]],
    userId: [],
    establishmentId: [],
  });

  constructor(
    protected ratingService: RatingService,
    protected userService: UserService,
    protected establishmentService: EstablishmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ rating }) => {
      this.updateForm(rating);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
    });
  }

  updateForm(rating: IRating): void {
    this.editForm.patchValue({
      id: rating.id,
      rate: rating.rate,
      comment: rating.comment,
      ratingDate: rating.ratingDate,
      userId: rating.userId,
      establishmentId: rating.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const rating = this.createFromForm();
    if (rating.id !== undefined) {
      this.subscribeToSaveResponse(this.ratingService.update(rating));
    } else {
      this.subscribeToSaveResponse(this.ratingService.create(rating));
    }
  }

  private createFromForm(): IRating {
    return {
      ...new Rating(),
      id: this.editForm.get(['id'])!.value,
      rate: this.editForm.get(['rate'])!.value,
      comment: this.editForm.get(['comment'])!.value,
      ratingDate: this.editForm.get(['ratingDate'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRating>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
