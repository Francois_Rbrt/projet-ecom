import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBookingElement } from 'app/shared/model/booking-element.model';

@Component({
  selector: 'jhi-booking-element-detail',
  templateUrl: './booking-element-detail.component.html',
})
export class BookingElementDetailComponent implements OnInit {
  bookingElement: IBookingElement | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingElement }) => (this.bookingElement = bookingElement));
  }

  previousState(): void {
    window.history.back();
  }
}
