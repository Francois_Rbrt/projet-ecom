import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBookingElement, BookingElement } from 'app/shared/model/booking-element.model';
import { BookingElementService } from './booking-element.service';
import { BookingElementComponent } from './booking-element.component';
import { BookingElementDetailComponent } from './booking-element-detail.component';
import { BookingElementUpdateComponent } from './booking-element-update.component';

@Injectable({ providedIn: 'root' })
export class BookingElementResolve implements Resolve<IBookingElement> {
  constructor(private service: BookingElementService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBookingElement> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bookingElement: HttpResponse<BookingElement>) => {
          if (bookingElement.body) {
            return of(bookingElement.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BookingElement());
  }
}

export const bookingElementRoute: Routes = [
  {
    path: '',
    component: BookingElementComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bookingElement.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BookingElementDetailComponent,
    resolve: {
      bookingElement: BookingElementResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bookingElement.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BookingElementUpdateComponent,
    resolve: {
      bookingElement: BookingElementResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bookingElement.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BookingElementUpdateComponent,
    resolve: {
      bookingElement: BookingElementResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bookingElement.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
