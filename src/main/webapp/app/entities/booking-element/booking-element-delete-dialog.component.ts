import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBookingElement } from 'app/shared/model/booking-element.model';
import { BookingElementService } from './booking-element.service';

@Component({
  templateUrl: './booking-element-delete-dialog.component.html',
})
export class BookingElementDeleteDialogComponent {
  bookingElement?: IBookingElement;

  constructor(
    protected bookingElementService: BookingElementService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bookingElementService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bookingElementListModification');
      this.activeModal.close();
    });
  }
}
