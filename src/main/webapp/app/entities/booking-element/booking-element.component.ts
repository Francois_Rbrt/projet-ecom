import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBookingElement } from 'app/shared/model/booking-element.model';
import { BookingElementService } from './booking-element.service';
import { BookingElementDeleteDialogComponent } from './booking-element-delete-dialog.component';

@Component({
  selector: 'jhi-booking-element',
  templateUrl: './booking-element.component.html',
})
export class BookingElementComponent implements OnInit, OnDestroy {
  bookingElements?: IBookingElement[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected bookingElementService: BookingElementService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.bookingElementService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IBookingElement[]>) => (this.bookingElements = res.body || []));
      return;
    }

    this.bookingElementService.query().subscribe((res: HttpResponse<IBookingElement[]>) => (this.bookingElements = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBookingElements();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBookingElement): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBookingElements(): void {
    this.eventSubscriber = this.eventManager.subscribe('bookingElementListModification', () => this.loadAll());
  }

  delete(bookingElement: IBookingElement): void {
    const modalRef = this.modalService.open(BookingElementDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bookingElement = bookingElement;
  }
}
