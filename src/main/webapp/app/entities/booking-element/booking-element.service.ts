import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IBookingElement } from 'app/shared/model/booking-element.model';
import { Booking, IBooking } from 'app/shared/model/booking.model';

type EntityResponseType = HttpResponse<IBookingElement>;
type EntityArrayResponseType = HttpResponse<IBookingElement[]>;

@Injectable({ providedIn: 'root' })
export class BookingElementService {
  public resourceUrl = SERVER_API_URL + 'api/booking-elements';
  public resourceBookingUrl = SERVER_API_URL + 'api/booking-elements-from-bookingId';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/booking-elements';

  constructor(protected http: HttpClient) {}

  create(bookingElement: IBookingElement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingElement);
    return this.http
      .post<IBookingElement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bookingElement: IBookingElement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingElement);
    return this.http
      .put<IBookingElement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBookingElement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findByBookingId(bookingId: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<IBookingElement[]>(`${this.resourceBookingUrl}/${bookingId}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBookingElement[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBookingElement[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(bookingElement: IBookingElement): IBookingElement {
    const copy: IBookingElement = Object.assign({}, bookingElement, {
      startDate: bookingElement.startDate && bookingElement.startDate.isValid() ? bookingElement.startDate.format(DATE_FORMAT) : undefined,
      endDate: bookingElement.endDate && bookingElement.endDate.isValid() ? bookingElement.endDate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? moment(res.body.startDate) : undefined;
      res.body.endDate = res.body.endDate ? moment(res.body.endDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bookingElement: IBookingElement) => {
        bookingElement.startDate = bookingElement.startDate ? moment(bookingElement.startDate) : undefined;
        bookingElement.endDate = bookingElement.endDate ? moment(bookingElement.endDate) : undefined;
      });
    }
    return res;
  }
}
