import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { BookingElementComponent } from './booking-element.component';
import { BookingElementDetailComponent } from './booking-element-detail.component';
import { BookingElementUpdateComponent } from './booking-element-update.component';
import { BookingElementDeleteDialogComponent } from './booking-element-delete-dialog.component';
import { bookingElementRoute } from './booking-element.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(bookingElementRoute)],
  declarations: [
    BookingElementComponent,
    BookingElementDetailComponent,
    BookingElementUpdateComponent,
    BookingElementDeleteDialogComponent,
  ],
  entryComponents: [BookingElementDeleteDialogComponent],
})
export class AfaleBookingElementModule {}
