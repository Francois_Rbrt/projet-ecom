import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBookingElement, BookingElement } from 'app/shared/model/booking-element.model';
import { BookingElementService } from './booking-element.service';
import { IBooking } from 'app/shared/model/booking.model';
import { BookingService } from 'app/entities/booking/booking.service';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';

type SelectableEntity = IBooking | IHousingTemplate;

@Component({
  selector: 'jhi-booking-element-update',
  templateUrl: './booking-element-update.component.html',
})
export class BookingElementUpdateComponent implements OnInit {
  isSaving = false;
  bookings: IBooking[] = [];
  housingtemplates: IHousingTemplate[] = [];
  startDateDp: any;
  endDateDp: any;

  editForm = this.fb.group({
    id: [],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    elementPrice: [null, [Validators.required]],
    bookingId: [],
    housingTemplateId: [],
  });

  constructor(
    protected bookingElementService: BookingElementService,
    protected bookingService: BookingService,
    protected housingTemplateService: HousingTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingElement }) => {
      this.updateForm(bookingElement);

      this.bookingService.query().subscribe((res: HttpResponse<IBooking[]>) => (this.bookings = res.body || []));

      this.housingTemplateService.query().subscribe((res: HttpResponse<IHousingTemplate[]>) => (this.housingtemplates = res.body || []));
    });
  }

  updateForm(bookingElement: IBookingElement): void {
    this.editForm.patchValue({
      id: bookingElement.id,
      startDate: bookingElement.startDate,
      endDate: bookingElement.endDate,
      elementPrice: bookingElement.elementPrice,
      bookingId: bookingElement.bookingId,
      housingTemplateId: bookingElement.housingTemplateId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bookingElement = this.createFromForm();
    if (bookingElement.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingElementService.update(bookingElement));
    } else {
      this.subscribeToSaveResponse(this.bookingElementService.create(bookingElement));
    }
  }

  private createFromForm(): IBookingElement {
    return {
      ...new BookingElement(),
      id: this.editForm.get(['id'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      endDate: this.editForm.get(['endDate'])!.value,
      elementPrice: this.editForm.get(['elementPrice'])!.value,
      bookingId: this.editForm.get(['bookingId'])!.value,
      housingTemplateId: this.editForm.get(['housingTemplateId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBookingElement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
