import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IContainsBedRooms, ContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';
import { ContainsBedRoomsService } from './contains-bed-rooms.service';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { IBedRoomTemplate } from 'app/shared/model/bed-room-template.model';
import { BedRoomTemplateService } from 'app/entities/bed-room-template/bed-room-template.service';

type SelectableEntity = IHousingTemplate | IBedRoomTemplate;

@Component({
  selector: 'jhi-contains-bed-rooms-update',
  templateUrl: './contains-bed-rooms-update.component.html',
})
export class ContainsBedRoomsUpdateComponent implements OnInit {
  isSaving = false;
  housingtemplates: IHousingTemplate[] = [];
  bedroomtemplates: IBedRoomTemplate[] = [];

  editForm = this.fb.group({
    id: [],
    nbOfUnit: [null, [Validators.required, Validators.min(0)]],
    housingTemplateId: [],
    bedroomTemplateId: [],
  });

  constructor(
    protected containsBedRoomsService: ContainsBedRoomsService,
    protected housingTemplateService: HousingTemplateService,
    protected bedRoomTemplateService: BedRoomTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ containsBedRooms }) => {
      this.updateForm(containsBedRooms);

      this.housingTemplateService.query().subscribe((res: HttpResponse<IHousingTemplate[]>) => (this.housingtemplates = res.body || []));

      this.bedRoomTemplateService.query().subscribe((res: HttpResponse<IBedRoomTemplate[]>) => (this.bedroomtemplates = res.body || []));
    });
  }

  updateForm(containsBedRooms: IContainsBedRooms): void {
    this.editForm.patchValue({
      id: containsBedRooms.id,
      nbOfUnit: containsBedRooms.nbOfUnit,
      housingTemplateId: containsBedRooms.housingTemplateId,
      bedroomTemplateId: containsBedRooms.bedroomTemplateId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const containsBedRooms = this.createFromForm();
    if (containsBedRooms.id !== undefined) {
      this.subscribeToSaveResponse(this.containsBedRoomsService.update(containsBedRooms));
    } else {
      this.subscribeToSaveResponse(this.containsBedRoomsService.create(containsBedRooms));
    }
  }

  private createFromForm(): IContainsBedRooms {
    return {
      ...new ContainsBedRooms(),
      id: this.editForm.get(['id'])!.value,
      nbOfUnit: this.editForm.get(['nbOfUnit'])!.value,
      housingTemplateId: this.editForm.get(['housingTemplateId'])!.value,
      bedroomTemplateId: this.editForm.get(['bedroomTemplateId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContainsBedRooms>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
