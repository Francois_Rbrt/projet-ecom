import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';
import { ContainsBedRoomsService } from './contains-bed-rooms.service';
import { ContainsBedRoomsDeleteDialogComponent } from './contains-bed-rooms-delete-dialog.component';

@Component({
  selector: 'jhi-contains-bed-rooms',
  templateUrl: './contains-bed-rooms.component.html',
})
export class ContainsBedRoomsComponent implements OnInit, OnDestroy {
  containsBedRooms?: IContainsBedRooms[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected containsBedRoomsService: ContainsBedRoomsService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.containsBedRoomsService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IContainsBedRooms[]>) => (this.containsBedRooms = res.body || []));
      return;
    }

    this.containsBedRoomsService.query().subscribe((res: HttpResponse<IContainsBedRooms[]>) => (this.containsBedRooms = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInContainsBedRooms();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IContainsBedRooms): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInContainsBedRooms(): void {
    this.eventSubscriber = this.eventManager.subscribe('containsBedRoomsListModification', () => this.loadAll());
  }

  delete(containsBedRooms: IContainsBedRooms): void {
    const modalRef = this.modalService.open(ContainsBedRoomsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.containsBedRooms = containsBedRooms;
  }
}
