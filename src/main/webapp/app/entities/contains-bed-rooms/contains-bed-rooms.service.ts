import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';
import { IContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';

type EntityResponseType = HttpResponse<IContainsBedRooms>;
type EntityArrayResponseType = HttpResponse<IContainsBedRooms[]>;

@Injectable({ providedIn: 'root' })
export class ContainsBedRoomsService {
  public resourceUrl = SERVER_API_URL + 'api/contains-bed-rooms';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/contains-bed-rooms';

  constructor(protected http: HttpClient) {}

  create(containsBedRooms: IContainsBedRooms): Observable<EntityResponseType> {
    return this.http.post<IContainsBedRooms>(this.resourceUrl, containsBedRooms, { observe: 'response' });
  }

  update(containsBedRooms: IContainsBedRooms): Observable<EntityResponseType> {
    return this.http.put<IContainsBedRooms>(this.resourceUrl, containsBedRooms, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IContainsBedRooms>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findHousingBedrooms(housingTemplateId: number): Observable<EntityArrayResponseType> {
    return this.http.get<IContainsBedRooms[]>(`${this.resourceUrl}/housing-template_id/${housingTemplateId}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IContainsBedRooms[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IContainsBedRooms[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
