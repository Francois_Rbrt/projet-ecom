import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';

@Component({
  selector: 'jhi-contains-bed-rooms-detail',
  templateUrl: './contains-bed-rooms-detail.component.html',
})
export class ContainsBedRoomsDetailComponent implements OnInit {
  containsBedRooms: IContainsBedRooms | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ containsBedRooms }) => (this.containsBedRooms = containsBedRooms));
  }

  previousState(): void {
    window.history.back();
  }
}
