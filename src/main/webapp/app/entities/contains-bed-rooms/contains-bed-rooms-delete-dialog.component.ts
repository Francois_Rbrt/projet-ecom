import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';
import { ContainsBedRoomsService } from './contains-bed-rooms.service';

@Component({
  templateUrl: './contains-bed-rooms-delete-dialog.component.html',
})
export class ContainsBedRoomsDeleteDialogComponent {
  containsBedRooms?: IContainsBedRooms;

  constructor(
    protected containsBedRoomsService: ContainsBedRoomsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.containsBedRoomsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('containsBedRoomsListModification');
      this.activeModal.close();
    });
  }
}
