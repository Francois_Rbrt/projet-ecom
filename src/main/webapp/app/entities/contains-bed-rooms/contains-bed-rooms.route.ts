import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IContainsBedRooms, ContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';
import { ContainsBedRoomsService } from './contains-bed-rooms.service';
import { ContainsBedRoomsComponent } from './contains-bed-rooms.component';
import { ContainsBedRoomsDetailComponent } from './contains-bed-rooms-detail.component';
import { ContainsBedRoomsUpdateComponent } from './contains-bed-rooms-update.component';

@Injectable({ providedIn: 'root' })
export class ContainsBedRoomsResolve implements Resolve<IContainsBedRooms> {
  constructor(private service: ContainsBedRoomsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IContainsBedRooms> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((containsBedRooms: HttpResponse<ContainsBedRooms>) => {
          if (containsBedRooms.body) {
            return of(containsBedRooms.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ContainsBedRooms());
  }
}

export const containsBedRoomsRoute: Routes = [
  {
    path: '',
    component: ContainsBedRoomsComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBedRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ContainsBedRoomsDetailComponent,
    resolve: {
      containsBedRooms: ContainsBedRoomsResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBedRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ContainsBedRoomsUpdateComponent,
    resolve: {
      containsBedRooms: ContainsBedRoomsResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBedRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ContainsBedRoomsUpdateComponent,
    resolve: {
      containsBedRooms: ContainsBedRoomsResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBedRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
