import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { ContainsBedRoomsComponent } from './contains-bed-rooms.component';
import { ContainsBedRoomsDetailComponent } from './contains-bed-rooms-detail.component';
import { ContainsBedRoomsUpdateComponent } from './contains-bed-rooms-update.component';
import { ContainsBedRoomsDeleteDialogComponent } from './contains-bed-rooms-delete-dialog.component';
import { containsBedRoomsRoute } from './contains-bed-rooms.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(containsBedRoomsRoute)],
  declarations: [
    ContainsBedRoomsComponent,
    ContainsBedRoomsDetailComponent,
    ContainsBedRoomsUpdateComponent,
    ContainsBedRoomsDeleteDialogComponent,
  ],
  entryComponents: [ContainsBedRoomsDeleteDialogComponent],
})
export class AfaleContainsBedRoomsModule {}
