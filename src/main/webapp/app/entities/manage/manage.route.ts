import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IManage, Manage } from 'app/shared/model/manage.model';
import { ManageService } from './manage.service';
import { ManageComponent } from './manage.component';
import { ManageDetailComponent } from './manage-detail.component';
import { ManageUpdateComponent } from './manage-update.component';

@Injectable({ providedIn: 'root' })
export class ManageResolve implements Resolve<IManage> {
  constructor(private service: ManageService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IManage> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((manage: HttpResponse<Manage>) => {
          if (manage.body) {
            return of(manage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Manage());
  }
}

export const manageRoute: Routes = [
  {
    path: '',
    component: ManageComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.manage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ManageDetailComponent,
    resolve: {
      manage: ManageResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.manage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ManageUpdateComponent,
    resolve: {
      manage: ManageResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.manage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ManageUpdateComponent,
    resolve: {
      manage: ManageResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.manage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
