import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IManage } from 'app/shared/model/manage.model';
import { IHousingTemplate } from '../../shared/model/housing-template.model';

type EntityResponseType = HttpResponse<IManage>;
type EntityArrayResponseType = HttpResponse<IManage[]>;

@Injectable({ providedIn: 'root' })
export class ManageService {
  public resourceUrl = SERVER_API_URL + 'api/manages';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/manages';

  constructor(protected http: HttpClient) {}

  create(manage: IManage): Observable<EntityResponseType> {
    return this.http.post<IManage>(this.resourceUrl, manage, { observe: 'response' });
  }

  update(manage: IManage): Observable<EntityResponseType> {
    return this.http.put<IManage>(this.resourceUrl, manage, { observe: 'response' });
  }

  findEstablishmentManager(establishmentId: number): Observable<EntityResponseType> {
    // TODO : rajouter les dates de début et de fin comme critère dans la query !
    return this.http.get<IManage>(`${this.resourceUrl}/establishment_id/${establishmentId}`, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IManage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IManage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IManage[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
