import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IManage, Manage } from 'app/shared/model/manage.model';
import { ManageService } from './manage.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

type SelectableEntity = IUser | IEstablishment;

@Component({
  selector: 'jhi-manage-update',
  templateUrl: './manage-update.component.html',
})
export class ManageUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  establishments: IEstablishment[] = [];

  editForm = this.fb.group({
    id: [],
    userId: [],
    establishmentId: [],
  });

  constructor(
    protected manageService: ManageService,
    protected userService: UserService,
    protected establishmentService: EstablishmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ manage }) => {
      this.updateForm(manage);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
    });
  }

  updateForm(manage: IManage): void {
    this.editForm.patchValue({
      id: manage.id,
      userId: manage.userId,
      establishmentId: manage.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const manage = this.createFromForm();
    if (manage.id !== undefined) {
      this.subscribeToSaveResponse(this.manageService.update(manage));
    } else {
      this.subscribeToSaveResponse(this.manageService.create(manage));
    }
  }

  private createFromForm(): IManage {
    return {
      ...new Manage(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IManage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
