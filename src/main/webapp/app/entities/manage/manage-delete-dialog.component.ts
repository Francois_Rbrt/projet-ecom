import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IManage } from 'app/shared/model/manage.model';
import { ManageService } from './manage.service';

@Component({
  templateUrl: './manage-delete-dialog.component.html',
})
export class ManageDeleteDialogComponent {
  manage?: IManage;

  constructor(protected manageService: ManageService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.manageService.delete(id).subscribe(() => {
      this.eventManager.broadcast('manageListModification');
      this.activeModal.close();
    });
  }
}
