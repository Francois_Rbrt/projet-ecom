import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { ManageComponent } from './manage.component';
import { ManageDetailComponent } from './manage-detail.component';
import { ManageUpdateComponent } from './manage-update.component';
import { ManageDeleteDialogComponent } from './manage-delete-dialog.component';
import { manageRoute } from './manage.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(manageRoute)],
  declarations: [ManageComponent, ManageDetailComponent, ManageUpdateComponent, ManageDeleteDialogComponent],
  entryComponents: [ManageDeleteDialogComponent],
})
export class AfaleManageModule {}
