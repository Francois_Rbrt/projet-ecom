import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IManage } from 'app/shared/model/manage.model';
import { ManageService } from './manage.service';
import { ManageDeleteDialogComponent } from './manage-delete-dialog.component';

@Component({
  selector: 'jhi-manage',
  templateUrl: './manage.component.html',
})
export class ManageComponent implements OnInit, OnDestroy {
  manages?: IManage[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected manageService: ManageService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.manageService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IManage[]>) => (this.manages = res.body || []));
      return;
    }

    this.manageService.query().subscribe((res: HttpResponse<IManage[]>) => (this.manages = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInManages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IManage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInManages(): void {
    this.eventSubscriber = this.eventManager.subscribe('manageListModification', () => this.loadAll());
  }

  delete(manage: IManage): void {
    const modalRef = this.modalService.open(ManageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.manage = manage;
  }
}
