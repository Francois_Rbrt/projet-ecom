import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IManage } from 'app/shared/model/manage.model';

@Component({
  selector: 'jhi-manage-detail',
  templateUrl: './manage-detail.component.html',
})
export class ManageDetailComponent implements OnInit {
  manage: IManage | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ manage }) => (this.manage = manage));
  }

  previousState(): void {
    window.history.back();
  }
}
