import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'booking',
        loadChildren: () => import('./booking/booking.module').then(m => m.AfaleBookingModule),
      },
      {
        path: 'booking-element',
        loadChildren: () => import('./booking-element/booking-element.module').then(m => m.AfaleBookingElementModule),
      },
      {
        path: 'favorites',
        loadChildren: () => import('./favorites/favorites.module').then(m => m.AfaleFavoritesModule),
      },
      {
        path: 'rating',
        loadChildren: () => import('./rating/rating.module').then(m => m.AfaleRatingModule),
      },
      {
        path: 'manage',
        loadChildren: () => import('./manage/manage.module').then(m => m.AfaleManageModule),
      },
      {
        path: 'establishment-closure',
        loadChildren: () => import('./establishment-closure/establishment-closure.module').then(m => m.AfaleEstablishmentClosureModule),
      },
      {
        path: 'housing-template',
        loadChildren: () => import('./housing-template/housing-template.module').then(m => m.AfaleHousingTemplateModule),
      },
      {
        path: 'housing-closure',
        loadChildren: () => import('./housing-closure/housing-closure.module').then(m => m.AfaleHousingClosureModule),
      },
      {
        path: 'contains-bed-rooms',
        loadChildren: () => import('./contains-bed-rooms/contains-bed-rooms.module').then(m => m.AfaleContainsBedRoomsModule),
      },
      {
        path: 'contains-bath-rooms',
        loadChildren: () => import('./contains-bath-rooms/contains-bath-rooms.module').then(m => m.AfaleContainsBathRoomsModule),
      },
      {
        path: 'photo',
        loadChildren: () => import('./photo/photo.module').then(m => m.AfalePhotoModule),
      },
      {
        path: 'establishment',
        loadChildren: () => import('./establishment/establishment.module').then(m => m.AfaleEstablishmentModule),
      },
      {
        path: 'bed-room-template',
        loadChildren: () => import('./bed-room-template/bed-room-template.module').then(m => m.AfaleBedRoomTemplateModule),
      },
      {
        path: 'bath-room-template',
        loadChildren: () => import('./bath-room-template/bath-room-template.module').then(m => m.AfaleBathRoomTemplateModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class AfaleEntityModule {}
