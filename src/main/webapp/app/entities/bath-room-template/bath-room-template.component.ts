import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBathRoomTemplate } from 'app/shared/model/bath-room-template.model';
import { BathRoomTemplateService } from './bath-room-template.service';
import { BathRoomTemplateDeleteDialogComponent } from './bath-room-template-delete-dialog.component';

@Component({
  selector: 'jhi-bath-room-template',
  templateUrl: './bath-room-template.component.html',
})
export class BathRoomTemplateComponent implements OnInit, OnDestroy {
  bathRoomTemplates?: IBathRoomTemplate[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected bathRoomTemplateService: BathRoomTemplateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.bathRoomTemplateService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IBathRoomTemplate[]>) => (this.bathRoomTemplates = res.body || []));
      return;
    }

    this.bathRoomTemplateService.query().subscribe((res: HttpResponse<IBathRoomTemplate[]>) => (this.bathRoomTemplates = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBathRoomTemplates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBathRoomTemplate): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBathRoomTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('bathRoomTemplateListModification', () => this.loadAll());
  }

  delete(bathRoomTemplate: IBathRoomTemplate): void {
    const modalRef = this.modalService.open(BathRoomTemplateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bathRoomTemplate = bathRoomTemplate;
  }
}
