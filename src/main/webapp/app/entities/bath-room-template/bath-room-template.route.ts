import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBathRoomTemplate, BathRoomTemplate } from 'app/shared/model/bath-room-template.model';
import { BathRoomTemplateService } from './bath-room-template.service';
import { BathRoomTemplateComponent } from './bath-room-template.component';
import { BathRoomTemplateDetailComponent } from './bath-room-template-detail.component';
import { BathRoomTemplateUpdateComponent } from './bath-room-template-update.component';

@Injectable({ providedIn: 'root' })
export class BathRoomTemplateResolve implements Resolve<IBathRoomTemplate> {
  constructor(private service: BathRoomTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBathRoomTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bathRoomTemplate: HttpResponse<BathRoomTemplate>) => {
          if (bathRoomTemplate.body) {
            return of(bathRoomTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BathRoomTemplate());
  }
}

export const bathRoomTemplateRoute: Routes = [
  {
    path: '',
    component: BathRoomTemplateComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bathRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BathRoomTemplateDetailComponent,
    resolve: {
      bathRoomTemplate: BathRoomTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bathRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BathRoomTemplateUpdateComponent,
    resolve: {
      bathRoomTemplate: BathRoomTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bathRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BathRoomTemplateUpdateComponent,
    resolve: {
      bathRoomTemplate: BathRoomTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bathRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
