import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBathRoomTemplate } from 'app/shared/model/bath-room-template.model';

@Component({
  selector: 'jhi-bath-room-template-detail',
  templateUrl: './bath-room-template-detail.component.html',
})
export class BathRoomTemplateDetailComponent implements OnInit {
  bathRoomTemplate: IBathRoomTemplate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bathRoomTemplate }) => (this.bathRoomTemplate = bathRoomTemplate));
  }

  previousState(): void {
    window.history.back();
  }
}
