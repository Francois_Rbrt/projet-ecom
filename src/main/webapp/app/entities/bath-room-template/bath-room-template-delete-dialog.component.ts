import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBathRoomTemplate } from 'app/shared/model/bath-room-template.model';
import { BathRoomTemplateService } from './bath-room-template.service';

@Component({
  templateUrl: './bath-room-template-delete-dialog.component.html',
})
export class BathRoomTemplateDeleteDialogComponent {
  bathRoomTemplate?: IBathRoomTemplate;

  constructor(
    protected bathRoomTemplateService: BathRoomTemplateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bathRoomTemplateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bathRoomTemplateListModification');
      this.activeModal.close();
    });
  }
}
