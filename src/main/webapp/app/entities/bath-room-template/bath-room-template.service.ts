import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IBathRoomTemplate } from 'app/shared/model/bath-room-template.model';

type EntityResponseType = HttpResponse<IBathRoomTemplate>;
type EntityArrayResponseType = HttpResponse<IBathRoomTemplate[]>;

@Injectable({ providedIn: 'root' })
export class BathRoomTemplateService {
  public resourceUrl = SERVER_API_URL + 'api/bath-room-templates';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/bath-room-templates';

  constructor(protected http: HttpClient) {}

  create(bathRoomTemplate: IBathRoomTemplate): Observable<EntityResponseType> {
    return this.http.post<IBathRoomTemplate>(this.resourceUrl, bathRoomTemplate, { observe: 'response' });
  }

  update(bathRoomTemplate: IBathRoomTemplate): Observable<EntityResponseType> {
    return this.http.put<IBathRoomTemplate>(this.resourceUrl, bathRoomTemplate, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBathRoomTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBathRoomTemplate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBathRoomTemplate[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
