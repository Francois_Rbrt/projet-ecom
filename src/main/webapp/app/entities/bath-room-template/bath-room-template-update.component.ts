import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBathRoomTemplate, BathRoomTemplate } from 'app/shared/model/bath-room-template.model';
import { BathRoomTemplateService } from './bath-room-template.service';

@Component({
  selector: 'jhi-bath-room-template-update',
  templateUrl: './bath-room-template-update.component.html',
})
export class BathRoomTemplateUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    hasBath: [],
    hasToilet: [],
    hasShower: [],
  });

  constructor(
    protected bathRoomTemplateService: BathRoomTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bathRoomTemplate }) => {
      this.updateForm(bathRoomTemplate);
    });
  }

  updateForm(bathRoomTemplate: IBathRoomTemplate): void {
    this.editForm.patchValue({
      id: bathRoomTemplate.id,
      hasBath: bathRoomTemplate.hasBath,
      hasToilet: bathRoomTemplate.hasToilet,
      hasShower: bathRoomTemplate.hasShower,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bathRoomTemplate = this.createFromForm();
    if (bathRoomTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.bathRoomTemplateService.update(bathRoomTemplate));
    } else {
      this.subscribeToSaveResponse(this.bathRoomTemplateService.create(bathRoomTemplate));
    }
  }

  private createFromForm(): IBathRoomTemplate {
    return {
      ...new BathRoomTemplate(),
      id: this.editForm.get(['id'])!.value,
      hasBath: this.editForm.get(['hasBath'])!.value,
      hasToilet: this.editForm.get(['hasToilet'])!.value,
      hasShower: this.editForm.get(['hasShower'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBathRoomTemplate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
