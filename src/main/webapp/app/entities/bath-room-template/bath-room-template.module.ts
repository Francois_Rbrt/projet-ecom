import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { BathRoomTemplateComponent } from './bath-room-template.component';
import { BathRoomTemplateDetailComponent } from './bath-room-template-detail.component';
import { BathRoomTemplateUpdateComponent } from './bath-room-template-update.component';
import { BathRoomTemplateDeleteDialogComponent } from './bath-room-template-delete-dialog.component';
import { bathRoomTemplateRoute } from './bath-room-template.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(bathRoomTemplateRoute)],
  declarations: [
    BathRoomTemplateComponent,
    BathRoomTemplateDetailComponent,
    BathRoomTemplateUpdateComponent,
    BathRoomTemplateDeleteDialogComponent,
  ],
  entryComponents: [BathRoomTemplateDeleteDialogComponent],
})
export class AfaleBathRoomTemplateModule {}
