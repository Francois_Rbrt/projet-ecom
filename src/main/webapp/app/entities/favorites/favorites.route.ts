import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFavorites, Favorites } from 'app/shared/model/favorites.model';
import { FavoritesService } from './favorites.service';
import { FavoritesComponent } from './favorites.component';
import { FavoritesDetailComponent } from './favorites-detail.component';
import { FavoritesUpdateComponent } from './favorites-update.component';

@Injectable({ providedIn: 'root' })
export class FavoritesResolve implements Resolve<IFavorites> {
  constructor(private service: FavoritesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFavorites> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((favorites: HttpResponse<Favorites>) => {
          if (favorites.body) {
            return of(favorites.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Favorites());
  }
}

export const favoritesRoute: Routes = [
  {
    path: '',
    component: FavoritesComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.favorites.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FavoritesDetailComponent,
    resolve: {
      favorites: FavoritesResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.favorites.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FavoritesUpdateComponent,
    resolve: {
      favorites: FavoritesResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.favorites.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FavoritesUpdateComponent,
    resolve: {
      favorites: FavoritesResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.favorites.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
