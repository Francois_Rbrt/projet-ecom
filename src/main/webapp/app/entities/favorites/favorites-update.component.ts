import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFavorites, Favorites } from 'app/shared/model/favorites.model';
import { FavoritesService } from './favorites.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

type SelectableEntity = IUser | IEstablishment;

@Component({
  selector: 'jhi-favorites-update',
  templateUrl: './favorites-update.component.html',
})
export class FavoritesUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  establishments: IEstablishment[] = [];

  editForm = this.fb.group({
    id: [],
    userId: [],
    establishmentId: [],
  });

  constructor(
    protected favoritesService: FavoritesService,
    protected userService: UserService,
    protected establishmentService: EstablishmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ favorites }) => {
      this.updateForm(favorites);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
    });
  }

  updateForm(favorites: IFavorites): void {
    this.editForm.patchValue({
      id: favorites.id,
      userId: favorites.userId,
      establishmentId: favorites.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const favorites = this.createFromForm();
    if (favorites.id !== undefined) {
      this.subscribeToSaveResponse(this.favoritesService.update(favorites));
    } else {
      this.subscribeToSaveResponse(this.favoritesService.create(favorites));
    }
  }

  private createFromForm(): IFavorites {
    return {
      ...new Favorites(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFavorites>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
