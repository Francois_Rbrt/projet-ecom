import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFavorites } from 'app/shared/model/favorites.model';
import { FavoritesService } from './favorites.service';
import { FavoritesDeleteDialogComponent } from './favorites-delete-dialog.component';

@Component({
  selector: 'jhi-favorites',
  templateUrl: './favorites.component.html',
})
export class FavoritesComponent implements OnInit, OnDestroy {
  favorites?: IFavorites[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected favoritesService: FavoritesService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.favoritesService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IFavorites[]>) => (this.favorites = res.body || []));
      return;
    }

    this.favoritesService.query().subscribe((res: HttpResponse<IFavorites[]>) => (this.favorites = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFavorites();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFavorites): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFavorites(): void {
    this.eventSubscriber = this.eventManager.subscribe('favoritesListModification', () => this.loadAll());
  }

  delete(favorites: IFavorites): void {
    const modalRef = this.modalService.open(FavoritesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.favorites = favorites;
  }
}
