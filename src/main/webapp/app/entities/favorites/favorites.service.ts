import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IFavorites } from 'app/shared/model/favorites.model';

type EntityResponseType = HttpResponse<IFavorites>;
type EntityArrayResponseType = HttpResponse<IFavorites[]>;

@Injectable({ providedIn: 'root' })
export class FavoritesService {
  public resourceUrl = SERVER_API_URL + 'api/favorites';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/favorites';

  constructor(protected http: HttpClient) {}

  create(favorites: IFavorites): Observable<EntityResponseType> {
    return this.http.post<IFavorites>(this.resourceUrl, favorites, { observe: 'response' });
  }

  update(favorites: IFavorites): Observable<EntityResponseType> {
    return this.http.put<IFavorites>(this.resourceUrl, favorites, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFavorites>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFavorites[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFavorites[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
