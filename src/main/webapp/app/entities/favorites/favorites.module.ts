import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { FavoritesComponent } from './favorites.component';
import { FavoritesDetailComponent } from './favorites-detail.component';
import { FavoritesUpdateComponent } from './favorites-update.component';
import { FavoritesDeleteDialogComponent } from './favorites-delete-dialog.component';
import { favoritesRoute } from './favorites.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(favoritesRoute)],
  declarations: [FavoritesComponent, FavoritesDetailComponent, FavoritesUpdateComponent, FavoritesDeleteDialogComponent],
  entryComponents: [FavoritesDeleteDialogComponent],
})
export class AfaleFavoritesModule {}
