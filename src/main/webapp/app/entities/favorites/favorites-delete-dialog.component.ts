import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFavorites } from 'app/shared/model/favorites.model';
import { FavoritesService } from './favorites.service';

@Component({
  templateUrl: './favorites-delete-dialog.component.html',
})
export class FavoritesDeleteDialogComponent {
  favorites?: IFavorites;

  constructor(protected favoritesService: FavoritesService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.favoritesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('favoritesListModification');
      this.activeModal.close();
    });
  }
}
