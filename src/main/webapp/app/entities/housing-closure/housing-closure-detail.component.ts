import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHousingClosure } from 'app/shared/model/housing-closure.model';

@Component({
  selector: 'jhi-housing-closure-detail',
  templateUrl: './housing-closure-detail.component.html',
})
export class HousingClosureDetailComponent implements OnInit {
  housingClosure: IHousingClosure | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ housingClosure }) => (this.housingClosure = housingClosure));
  }

  previousState(): void {
    window.history.back();
  }
}
