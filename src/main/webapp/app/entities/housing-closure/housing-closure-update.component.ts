import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IHousingClosure, HousingClosure } from 'app/shared/model/housing-closure.model';
import { HousingClosureService } from './housing-closure.service';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';

@Component({
  selector: 'jhi-housing-closure-update',
  templateUrl: './housing-closure-update.component.html',
})
export class HousingClosureUpdateComponent implements OnInit {
  isSaving = false;
  housingtemplates: IHousingTemplate[] = [];
  startDateDp: any;
  endDateDp: any;

  editForm = this.fb.group({
    id: [],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    cause: [null, [Validators.maxLength(250)]],
    establishmentId: [],
  });

  constructor(
    protected housingClosureService: HousingClosureService,
    protected housingTemplateService: HousingTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ housingClosure }) => {
      this.updateForm(housingClosure);

      this.housingTemplateService.query().subscribe((res: HttpResponse<IHousingTemplate[]>) => (this.housingtemplates = res.body || []));
    });
  }

  updateForm(housingClosure: IHousingClosure): void {
    this.editForm.patchValue({
      id: housingClosure.id,
      startDate: housingClosure.startDate,
      endDate: housingClosure.endDate,
      cause: housingClosure.cause,
      establishmentId: housingClosure.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const housingClosure = this.createFromForm();
    if (housingClosure.id !== undefined) {
      this.subscribeToSaveResponse(this.housingClosureService.update(housingClosure));
    } else {
      this.subscribeToSaveResponse(this.housingClosureService.create(housingClosure));
    }
  }

  private createFromForm(): IHousingClosure {
    return {
      ...new HousingClosure(),
      id: this.editForm.get(['id'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      endDate: this.editForm.get(['endDate'])!.value,
      cause: this.editForm.get(['cause'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHousingClosure>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IHousingTemplate): any {
    return item.id;
  }
}
