import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { HousingClosureComponent } from './housing-closure.component';
import { HousingClosureDetailComponent } from './housing-closure-detail.component';
import { HousingClosureUpdateComponent } from './housing-closure-update.component';
import { HousingClosureDeleteDialogComponent } from './housing-closure-delete-dialog.component';
import { housingClosureRoute } from './housing-closure.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(housingClosureRoute)],
  declarations: [
    HousingClosureComponent,
    HousingClosureDetailComponent,
    HousingClosureUpdateComponent,
    HousingClosureDeleteDialogComponent,
  ],
  entryComponents: [HousingClosureDeleteDialogComponent],
})
export class AfaleHousingClosureModule {}
