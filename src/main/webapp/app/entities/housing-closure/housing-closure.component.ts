import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IHousingClosure } from 'app/shared/model/housing-closure.model';
import { HousingClosureService } from './housing-closure.service';
import { HousingClosureDeleteDialogComponent } from './housing-closure-delete-dialog.component';

@Component({
  selector: 'jhi-housing-closure',
  templateUrl: './housing-closure.component.html',
})
export class HousingClosureComponent implements OnInit, OnDestroy {
  housingClosures?: IHousingClosure[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected housingClosureService: HousingClosureService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.housingClosureService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IHousingClosure[]>) => (this.housingClosures = res.body || []));
      return;
    }

    this.housingClosureService.query().subscribe((res: HttpResponse<IHousingClosure[]>) => (this.housingClosures = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInHousingClosures();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IHousingClosure): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInHousingClosures(): void {
    this.eventSubscriber = this.eventManager.subscribe('housingClosureListModification', () => this.loadAll());
  }

  delete(housingClosure: IHousingClosure): void {
    const modalRef = this.modalService.open(HousingClosureDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.housingClosure = housingClosure;
  }
}
