import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IHousingClosure, HousingClosure } from 'app/shared/model/housing-closure.model';
import { HousingClosureService } from './housing-closure.service';
import { HousingClosureComponent } from './housing-closure.component';
import { HousingClosureDetailComponent } from './housing-closure-detail.component';
import { HousingClosureUpdateComponent } from './housing-closure-update.component';

@Injectable({ providedIn: 'root' })
export class HousingClosureResolve implements Resolve<IHousingClosure> {
  constructor(private service: HousingClosureService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHousingClosure> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((housingClosure: HttpResponse<HousingClosure>) => {
          if (housingClosure.body) {
            return of(housingClosure.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new HousingClosure());
  }
}

export const housingClosureRoute: Routes = [
  {
    path: '',
    component: HousingClosureComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: HousingClosureDetailComponent,
    resolve: {
      housingClosure: HousingClosureResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: HousingClosureUpdateComponent,
    resolve: {
      housingClosure: HousingClosureResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: HousingClosureUpdateComponent,
    resolve: {
      housingClosure: HousingClosureResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingClosure.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
