import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IHousingClosure } from 'app/shared/model/housing-closure.model';

type EntityResponseType = HttpResponse<IHousingClosure>;
type EntityArrayResponseType = HttpResponse<IHousingClosure[]>;

@Injectable({ providedIn: 'root' })
export class HousingClosureService {
  public resourceUrl = SERVER_API_URL + 'api/housing-closures';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/housing-closures';

  constructor(protected http: HttpClient) {}

  create(housingClosure: IHousingClosure): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(housingClosure);
    return this.http
      .post<IHousingClosure>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(housingClosure: IHousingClosure): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(housingClosure);
    return this.http
      .put<IHousingClosure>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IHousingClosure>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IHousingClosure[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IHousingClosure[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(housingClosure: IHousingClosure): IHousingClosure {
    const copy: IHousingClosure = Object.assign({}, housingClosure, {
      startDate: housingClosure.startDate && housingClosure.startDate.isValid() ? housingClosure.startDate.format(DATE_FORMAT) : undefined,
      endDate: housingClosure.endDate && housingClosure.endDate.isValid() ? housingClosure.endDate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? moment(res.body.startDate) : undefined;
      res.body.endDate = res.body.endDate ? moment(res.body.endDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((housingClosure: IHousingClosure) => {
        housingClosure.startDate = housingClosure.startDate ? moment(housingClosure.startDate) : undefined;
        housingClosure.endDate = housingClosure.endDate ? moment(housingClosure.endDate) : undefined;
      });
    }
    return res;
  }
}
