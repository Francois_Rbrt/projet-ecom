import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHousingClosure } from 'app/shared/model/housing-closure.model';
import { HousingClosureService } from './housing-closure.service';

@Component({
  templateUrl: './housing-closure-delete-dialog.component.html',
})
export class HousingClosureDeleteDialogComponent {
  housingClosure?: IHousingClosure;

  constructor(
    protected housingClosureService: HousingClosureService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.housingClosureService.delete(id).subscribe(() => {
      this.eventManager.broadcast('housingClosureListModification');
      this.activeModal.close();
    });
  }
}
