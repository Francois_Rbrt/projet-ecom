import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { HousingTemplateComponent } from './housing-template.component';
import { HousingTemplateDetailComponent } from './housing-template-detail.component';
import { HousingTemplateUpdateComponent } from './housing-template-update.component';
import { HousingTemplateDeleteDialogComponent } from './housing-template-delete-dialog.component';
import { housingTemplateRoute } from './housing-template.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(housingTemplateRoute)],
  declarations: [
    HousingTemplateComponent,
    HousingTemplateDetailComponent,
    HousingTemplateUpdateComponent,
    HousingTemplateDeleteDialogComponent,
  ],
  entryComponents: [HousingTemplateDeleteDialogComponent],
})
export class AfaleHousingTemplateModule {}
