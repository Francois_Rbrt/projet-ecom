import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHousingTemplate } from 'app/shared/model/housing-template.model';

@Component({
  selector: 'jhi-housing-template-detail',
  templateUrl: './housing-template-detail.component.html',
})
export class HousingTemplateDetailComponent implements OnInit {
  housingTemplate: IHousingTemplate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ housingTemplate }) => (this.housingTemplate = housingTemplate));
  }

  previousState(): void {
    window.history.back();
  }
}
