import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IHousingTemplate, HousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from './housing-template.service';
import { HousingTemplateComponent } from './housing-template.component';
import { HousingTemplateDetailComponent } from './housing-template-detail.component';
import { HousingTemplateUpdateComponent } from './housing-template-update.component';

@Injectable({ providedIn: 'root' })
export class HousingTemplateResolve implements Resolve<IHousingTemplate> {
  constructor(private service: HousingTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHousingTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((housingTemplate: HttpResponse<HousingTemplate>) => {
          if (housingTemplate.body) {
            return of(housingTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new HousingTemplate());
  }
}

export const housingTemplateRoute: Routes = [
  {
    path: '',
    component: HousingTemplateComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: HousingTemplateDetailComponent,
    resolve: {
      housingTemplate: HousingTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: HousingTemplateUpdateComponent,
    resolve: {
      housingTemplate: HousingTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: HousingTemplateUpdateComponent,
    resolve: {
      housingTemplate: HousingTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.housingTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
