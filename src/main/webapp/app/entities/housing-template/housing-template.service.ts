import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';

type EntityResponseType = HttpResponse<IHousingTemplate>;
type EntityArrayResponseType = HttpResponse<IHousingTemplate[]>;

@Injectable({ providedIn: 'root' })
export class HousingTemplateService {
  public resourceUrl = SERVER_API_URL + 'api/housing-templates';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/housing-templates';

  constructor(protected http: HttpClient) {}

  create(housingTemplate: IHousingTemplate): Observable<EntityResponseType> {
    return this.http.post<IHousingTemplate>(this.resourceUrl, housingTemplate, { observe: 'response' });
  }

  update(housingTemplate: IHousingTemplate): Observable<EntityResponseType> {
    return this.http.put<IHousingTemplate>(this.resourceUrl, housingTemplate, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHousingTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findEstablishmentHousings(establishmentId: number): Observable<EntityArrayResponseType> {
    // TODO : rajouter les dates de début et de fin comme critère dans la query !
    return this.http.get<IHousingTemplate[]>(`${this.resourceUrl}/establishment_id/${establishmentId}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHousingTemplate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHousingTemplate[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
