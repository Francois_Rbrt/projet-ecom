import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IHousingTemplate, HousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from './housing-template.service';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';

@Component({
  selector: 'jhi-housing-template-update',
  templateUrl: './housing-template-update.component.html',
})
export class HousingTemplateUpdateComponent implements OnInit {
  isSaving = false;
  establishments: IEstablishment[] = [];

  editForm = this.fb.group({
    id: [],
    housingType: [null, [Validators.required]],
    nbOfUnit: [null, [Validators.required, Validators.min(1)]],
    nbMaxOfOccupants: [null, [Validators.required, Validators.min(1)]],
    pricePerNight: [null, [Validators.required]],
    isNonSmoking: [null, [Validators.required]],
    hasKitchen: [null, [Validators.required]],
    hasToilets: [null, [Validators.required]],
    establishmentId: [],
  });

  constructor(
    protected housingTemplateService: HousingTemplateService,
    protected establishmentService: EstablishmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ housingTemplate }) => {
      this.updateForm(housingTemplate);

      this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
    });
  }

  updateForm(housingTemplate: IHousingTemplate): void {
    this.editForm.patchValue({
      id: housingTemplate.id,
      housingType: housingTemplate.housingType,
      nbOfUnit: housingTemplate.nbOfUnit,
      nbMaxOfOccupants: housingTemplate.nbMaxOfOccupants,
      pricePerNight: housingTemplate.pricePerNight,
      isNonSmoking: housingTemplate.isNonSmoking,
      hasKitchen: housingTemplate.hasKitchen,
      hasToilets: housingTemplate.hasToilets,
      establishmentId: housingTemplate.establishmentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const housingTemplate = this.createFromForm();
    if (housingTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.housingTemplateService.update(housingTemplate));
    } else {
      this.subscribeToSaveResponse(this.housingTemplateService.create(housingTemplate));
    }
  }

  private createFromForm(): IHousingTemplate {
    return {
      ...new HousingTemplate(),
      id: this.editForm.get(['id'])!.value,
      housingType: this.editForm.get(['housingType'])!.value,
      nbOfUnit: this.editForm.get(['nbOfUnit'])!.value,
      nbMaxOfOccupants: this.editForm.get(['nbMaxOfOccupants'])!.value,
      pricePerNight: this.editForm.get(['pricePerNight'])!.value,
      isNonSmoking: this.editForm.get(['isNonSmoking'])!.value,
      hasKitchen: this.editForm.get(['hasKitchen'])!.value,
      hasToilets: this.editForm.get(['hasToilets'])!.value,
      establishmentId: this.editForm.get(['establishmentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHousingTemplate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IEstablishment): any {
    return item.id;
  }
}
