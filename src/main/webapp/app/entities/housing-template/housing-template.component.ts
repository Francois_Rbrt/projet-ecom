import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from './housing-template.service';
import { HousingTemplateDeleteDialogComponent } from './housing-template-delete-dialog.component';

@Component({
  selector: 'jhi-housing-template',
  templateUrl: './housing-template.component.html',
})
export class HousingTemplateComponent implements OnInit, OnDestroy {
  housingTemplates?: IHousingTemplate[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected housingTemplateService: HousingTemplateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.housingTemplateService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IHousingTemplate[]>) => (this.housingTemplates = res.body || []));
      return;
    }

    this.housingTemplateService.query().subscribe((res: HttpResponse<IHousingTemplate[]>) => (this.housingTemplates = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInHousingTemplates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IHousingTemplate): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInHousingTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('housingTemplateListModification', () => this.loadAll());
  }

  delete(housingTemplate: IHousingTemplate): void {
    const modalRef = this.modalService.open(HousingTemplateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.housingTemplate = housingTemplate;
  }
}
