import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from './housing-template.service';

@Component({
  templateUrl: './housing-template-delete-dialog.component.html',
})
export class HousingTemplateDeleteDialogComponent {
  housingTemplate?: IHousingTemplate;

  constructor(
    protected housingTemplateService: HousingTemplateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.housingTemplateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('housingTemplateListModification');
      this.activeModal.close();
    });
  }
}
