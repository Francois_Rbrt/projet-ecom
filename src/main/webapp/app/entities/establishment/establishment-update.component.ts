import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEstablishment, Establishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from './establishment.service';

@Component({
  selector: 'jhi-establishment-update',
  templateUrl: './establishment-update.component.html',
})
export class EstablishmentUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(150)]],
    adress: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(150)]],
    latitude: [],
    longitude: [],
    globalRate: [],
    establishmentType: [null, [Validators.required]],
    hasParking: [null, [Validators.required]],
    hasRestaurant: [null, [Validators.required]],
    hasFreeWifi: [null, [Validators.required]],
    hasSwimmingPool: [null, [Validators.required]],
    description: [null, [Validators.maxLength(300)]],
  });

  constructor(protected establishmentService: EstablishmentService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ establishment }) => {
      this.updateForm(establishment);
    });
  }

  updateForm(establishment: IEstablishment): void {
    this.editForm.patchValue({
      id: establishment.id,
      name: establishment.name,
      adress: establishment.adress,
      latitude: establishment.latitude,
      longitude: establishment.longitude,
      globalRate: establishment.globalRate,
      establishmentType: establishment.establishmentType,
      hasParking: establishment.hasParking,
      hasRestaurant: establishment.hasRestaurant,
      hasFreeWifi: establishment.hasFreeWifi,
      hasSwimmingPool: establishment.hasSwimmingPool,
      description: establishment.description,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const establishment = this.createFromForm();
    if (establishment.id !== undefined) {
      this.subscribeToSaveResponse(this.establishmentService.update(establishment));
    } else {
      this.subscribeToSaveResponse(this.establishmentService.create(establishment));
    }
  }

  private createFromForm(): IEstablishment {
    return {
      ...new Establishment(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      adress: this.editForm.get(['adress'])!.value,
      latitude: this.editForm.get(['latitude'])!.value,
      longitude: this.editForm.get(['longitude'])!.value,
      globalRate: this.editForm.get(['globalRate'])!.value,
      establishmentType: this.editForm.get(['establishmentType'])!.value,
      hasParking: this.editForm.get(['hasParking'])!.value,
      hasRestaurant: this.editForm.get(['hasRestaurant'])!.value,
      hasFreeWifi: this.editForm.get(['hasFreeWifi'])!.value,
      hasSwimmingPool: this.editForm.get(['hasSwimmingPool'])!.value,
      description: this.editForm.get(['description'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEstablishment>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
