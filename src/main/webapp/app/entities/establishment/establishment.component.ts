import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from './establishment.service';
import { EstablishmentDeleteDialogComponent } from './establishment-delete-dialog.component';

@Component({
  selector: 'jhi-establishment',
  templateUrl: './establishment.component.html',
})
export class EstablishmentComponent implements OnInit, OnDestroy {
  establishments?: IEstablishment[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected establishmentService: EstablishmentService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.establishmentService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
      return;
    }

    this.establishmentService.query().subscribe((res: HttpResponse<IEstablishment[]>) => (this.establishments = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInEstablishments();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IEstablishment): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInEstablishments(): void {
    this.eventSubscriber = this.eventManager.subscribe('establishmentListModification', () => this.loadAll());
  }

  delete(establishment: IEstablishment): void {
    const modalRef = this.modalService.open(EstablishmentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.establishment = establishment;
  }
}
