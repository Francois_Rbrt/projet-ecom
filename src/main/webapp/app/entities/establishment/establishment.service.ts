import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IEstablishment } from 'app/shared/model/establishment.model';
import { Page } from 'app/shared/model/Page.model';

type EntityResponseType = HttpResponse<IEstablishment>;
type EntityArrayResponseType = HttpResponse<IEstablishment[]>;

type EntityPageResponseType = HttpResponse<Page<IEstablishment>>;

@Injectable({ providedIn: 'root' })
export class EstablishmentService {
  public resourceUrl = SERVER_API_URL + 'api/establishments';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/establishments';

  constructor(protected http: HttpClient) {}

  create(establishment: IEstablishment): Observable<EntityResponseType> {
    return this.http.post<IEstablishment>(this.resourceUrl, establishment, { observe: 'response' });
  }

  update(establishment: IEstablishment): Observable<EntityResponseType> {
    return this.http.put<IEstablishment>(this.resourceUrl, establishment, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEstablishment>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findMinPrice(id: number): Observable<HttpResponse<Number>> {
    return this.http.get<Number>(`${this.resourceUrl}/price/${id}`, { observe: 'response' });
  }

  findPage(nbPage: number, nbElement: number): Observable<EntityPageResponseType> {
    return this.http.get<Page<IEstablishment>>(`${this.resourceUrl}Page/${nbPage}&${nbElement}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEstablishment[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEstablishment[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
