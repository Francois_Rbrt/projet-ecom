import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { EstablishmentComponent } from './establishment.component';
import { EstablishmentDetailComponent } from './establishment-detail.component';
import { EstablishmentUpdateComponent } from './establishment-update.component';
import { EstablishmentDeleteDialogComponent } from './establishment-delete-dialog.component';
import { establishmentRoute } from './establishment.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(establishmentRoute)],
  declarations: [EstablishmentComponent, EstablishmentDetailComponent, EstablishmentUpdateComponent, EstablishmentDeleteDialogComponent],
  entryComponents: [EstablishmentDeleteDialogComponent],
  exports: [EstablishmentComponent],
})
export class AfaleEstablishmentModule {}
