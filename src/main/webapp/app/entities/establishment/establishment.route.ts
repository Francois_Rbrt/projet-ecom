import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEstablishment, Establishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from './establishment.service';
import { EstablishmentComponent } from './establishment.component';
import { EstablishmentDetailComponent } from './establishment-detail.component';
import { EstablishmentUpdateComponent } from './establishment-update.component';

@Injectable({ providedIn: 'root' })
export class EstablishmentResolve implements Resolve<IEstablishment> {
  constructor(private service: EstablishmentService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEstablishment> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((establishment: HttpResponse<Establishment>) => {
          if (establishment.body) {
            return of(establishment.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Establishment());
  }
}

export const establishmentRoute: Routes = [
  {
    path: '',
    component: EstablishmentComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishment.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EstablishmentDetailComponent,
    resolve: {
      establishment: EstablishmentResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishment.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EstablishmentUpdateComponent,
    resolve: {
      establishment: EstablishmentResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishment.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EstablishmentUpdateComponent,
    resolve: {
      establishment: EstablishmentResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.establishment.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
