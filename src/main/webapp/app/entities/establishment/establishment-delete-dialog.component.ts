import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEstablishment } from 'app/shared/model/establishment.model';
import { EstablishmentService } from './establishment.service';

@Component({
  templateUrl: './establishment-delete-dialog.component.html',
})
export class EstablishmentDeleteDialogComponent {
  establishment?: IEstablishment;

  constructor(
    protected establishmentService: EstablishmentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.establishmentService.delete(id).subscribe(() => {
      this.eventManager.broadcast('establishmentListModification');
      this.activeModal.close();
    });
  }
}
