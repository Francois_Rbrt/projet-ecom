import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBedRoomTemplate, BedRoomTemplate } from 'app/shared/model/bed-room-template.model';
import { BedRoomTemplateService } from './bed-room-template.service';
import { BedRoomTemplateComponent } from './bed-room-template.component';
import { BedRoomTemplateDetailComponent } from './bed-room-template-detail.component';
import { BedRoomTemplateUpdateComponent } from './bed-room-template-update.component';

@Injectable({ providedIn: 'root' })
export class BedRoomTemplateResolve implements Resolve<IBedRoomTemplate> {
  constructor(private service: BedRoomTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBedRoomTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bedRoomTemplate: HttpResponse<BedRoomTemplate>) => {
          if (bedRoomTemplate.body) {
            return of(bedRoomTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BedRoomTemplate());
  }
}

export const bedRoomTemplateRoute: Routes = [
  {
    path: '',
    component: BedRoomTemplateComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bedRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BedRoomTemplateDetailComponent,
    resolve: {
      bedRoomTemplate: BedRoomTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bedRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BedRoomTemplateUpdateComponent,
    resolve: {
      bedRoomTemplate: BedRoomTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bedRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BedRoomTemplateUpdateComponent,
    resolve: {
      bedRoomTemplate: BedRoomTemplateResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.bedRoomTemplate.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
