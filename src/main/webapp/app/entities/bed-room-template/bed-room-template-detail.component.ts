import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBedRoomTemplate } from 'app/shared/model/bed-room-template.model';

@Component({
  selector: 'jhi-bed-room-template-detail',
  templateUrl: './bed-room-template-detail.component.html',
})
export class BedRoomTemplateDetailComponent implements OnInit {
  bedRoomTemplate: IBedRoomTemplate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bedRoomTemplate }) => (this.bedRoomTemplate = bedRoomTemplate));
  }

  previousState(): void {
    window.history.back();
  }
}
