import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBedRoomTemplate } from 'app/shared/model/bed-room-template.model';
import { BedRoomTemplateService } from './bed-room-template.service';
import { BedRoomTemplateDeleteDialogComponent } from './bed-room-template-delete-dialog.component';

@Component({
  selector: 'jhi-bed-room-template',
  templateUrl: './bed-room-template.component.html',
})
export class BedRoomTemplateComponent implements OnInit, OnDestroy {
  bedRoomTemplates?: IBedRoomTemplate[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected bedRoomTemplateService: BedRoomTemplateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.bedRoomTemplateService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IBedRoomTemplate[]>) => (this.bedRoomTemplates = res.body || []));
      return;
    }

    this.bedRoomTemplateService.query().subscribe((res: HttpResponse<IBedRoomTemplate[]>) => (this.bedRoomTemplates = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBedRoomTemplates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBedRoomTemplate): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBedRoomTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('bedRoomTemplateListModification', () => this.loadAll());
  }

  delete(bedRoomTemplate: IBedRoomTemplate): void {
    const modalRef = this.modalService.open(BedRoomTemplateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bedRoomTemplate = bedRoomTemplate;
  }
}
