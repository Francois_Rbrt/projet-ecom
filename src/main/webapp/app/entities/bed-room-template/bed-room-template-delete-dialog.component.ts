import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBedRoomTemplate } from 'app/shared/model/bed-room-template.model';
import { BedRoomTemplateService } from './bed-room-template.service';

@Component({
  templateUrl: './bed-room-template-delete-dialog.component.html',
})
export class BedRoomTemplateDeleteDialogComponent {
  bedRoomTemplate?: IBedRoomTemplate;

  constructor(
    protected bedRoomTemplateService: BedRoomTemplateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bedRoomTemplateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bedRoomTemplateListModification');
      this.activeModal.close();
    });
  }
}
