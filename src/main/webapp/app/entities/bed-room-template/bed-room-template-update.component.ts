import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBedRoomTemplate, BedRoomTemplate } from 'app/shared/model/bed-room-template.model';
import { BedRoomTemplateService } from './bed-room-template.service';

@Component({
  selector: 'jhi-bed-room-template-update',
  templateUrl: './bed-room-template-update.component.html',
})
export class BedRoomTemplateUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nbSingleBeds: [null, [Validators.required]],
    nbDoubleBeds: [null, [Validators.required]],
  });

  constructor(
    protected bedRoomTemplateService: BedRoomTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bedRoomTemplate }) => {
      this.updateForm(bedRoomTemplate);
    });
  }

  updateForm(bedRoomTemplate: IBedRoomTemplate): void {
    this.editForm.patchValue({
      id: bedRoomTemplate.id,
      nbSingleBeds: bedRoomTemplate.nbSingleBeds,
      nbDoubleBeds: bedRoomTemplate.nbDoubleBeds,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bedRoomTemplate = this.createFromForm();
    if (bedRoomTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.bedRoomTemplateService.update(bedRoomTemplate));
    } else {
      this.subscribeToSaveResponse(this.bedRoomTemplateService.create(bedRoomTemplate));
    }
  }

  private createFromForm(): IBedRoomTemplate {
    return {
      ...new BedRoomTemplate(),
      id: this.editForm.get(['id'])!.value,
      nbSingleBeds: this.editForm.get(['nbSingleBeds'])!.value,
      nbDoubleBeds: this.editForm.get(['nbDoubleBeds'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBedRoomTemplate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
