import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { BedRoomTemplateComponent } from './bed-room-template.component';
import { BedRoomTemplateDetailComponent } from './bed-room-template-detail.component';
import { BedRoomTemplateUpdateComponent } from './bed-room-template-update.component';
import { BedRoomTemplateDeleteDialogComponent } from './bed-room-template-delete-dialog.component';
import { bedRoomTemplateRoute } from './bed-room-template.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(bedRoomTemplateRoute)],
  declarations: [
    BedRoomTemplateComponent,
    BedRoomTemplateDetailComponent,
    BedRoomTemplateUpdateComponent,
    BedRoomTemplateDeleteDialogComponent,
  ],
  entryComponents: [BedRoomTemplateDeleteDialogComponent],
})
export class AfaleBedRoomTemplateModule {}
