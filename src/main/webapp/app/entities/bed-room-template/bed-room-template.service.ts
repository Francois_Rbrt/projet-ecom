import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IBedRoomTemplate } from 'app/shared/model/bed-room-template.model';

type EntityResponseType = HttpResponse<IBedRoomTemplate>;
type EntityArrayResponseType = HttpResponse<IBedRoomTemplate[]>;

@Injectable({ providedIn: 'root' })
export class BedRoomTemplateService {
  public resourceUrl = SERVER_API_URL + 'api/bed-room-templates';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/bed-room-templates';

  constructor(protected http: HttpClient) {}

  create(bedRoomTemplate: IBedRoomTemplate): Observable<EntityResponseType> {
    return this.http.post<IBedRoomTemplate>(this.resourceUrl, bedRoomTemplate, { observe: 'response' });
  }

  update(bedRoomTemplate: IBedRoomTemplate): Observable<EntityResponseType> {
    return this.http.put<IBedRoomTemplate>(this.resourceUrl, bedRoomTemplate, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBedRoomTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBedRoomTemplate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBedRoomTemplate[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
