import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfaleSharedModule } from 'app/shared/shared.module';
import { ContainsBathRoomsComponent } from './contains-bath-rooms.component';
import { ContainsBathRoomsDetailComponent } from './contains-bath-rooms-detail.component';
import { ContainsBathRoomsUpdateComponent } from './contains-bath-rooms-update.component';
import { ContainsBathRoomsDeleteDialogComponent } from './contains-bath-rooms-delete-dialog.component';
import { containsBathRoomsRoute } from './contains-bath-rooms.route';

@NgModule({
  imports: [AfaleSharedModule, RouterModule.forChild(containsBathRoomsRoute)],
  declarations: [
    ContainsBathRoomsComponent,
    ContainsBathRoomsDetailComponent,
    ContainsBathRoomsUpdateComponent,
    ContainsBathRoomsDeleteDialogComponent,
  ],
  entryComponents: [ContainsBathRoomsDeleteDialogComponent],
})
export class AfaleContainsBathRoomsModule {}
