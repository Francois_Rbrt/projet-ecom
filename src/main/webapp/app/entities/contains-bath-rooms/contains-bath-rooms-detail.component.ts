import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';

@Component({
  selector: 'jhi-contains-bath-rooms-detail',
  templateUrl: './contains-bath-rooms-detail.component.html',
})
export class ContainsBathRoomsDetailComponent implements OnInit {
  containsBathRooms: IContainsBathRooms | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ containsBathRooms }) => (this.containsBathRooms = containsBathRooms));
  }

  previousState(): void {
    window.history.back();
  }
}
