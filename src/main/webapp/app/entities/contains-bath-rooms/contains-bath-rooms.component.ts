import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';
import { ContainsBathRoomsService } from './contains-bath-rooms.service';
import { ContainsBathRoomsDeleteDialogComponent } from './contains-bath-rooms-delete-dialog.component';

@Component({
  selector: 'jhi-contains-bath-rooms',
  templateUrl: './contains-bath-rooms.component.html',
})
export class ContainsBathRoomsComponent implements OnInit, OnDestroy {
  containsBathRooms?: IContainsBathRooms[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected containsBathRoomsService: ContainsBathRoomsService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.containsBathRoomsService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IContainsBathRooms[]>) => (this.containsBathRooms = res.body || []));
      return;
    }

    this.containsBathRoomsService.query().subscribe((res: HttpResponse<IContainsBathRooms[]>) => (this.containsBathRooms = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInContainsBathRooms();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IContainsBathRooms): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInContainsBathRooms(): void {
    this.eventSubscriber = this.eventManager.subscribe('containsBathRoomsListModification', () => this.loadAll());
  }

  delete(containsBathRooms: IContainsBathRooms): void {
    const modalRef = this.modalService.open(ContainsBathRoomsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.containsBathRooms = containsBathRooms;
  }
}
