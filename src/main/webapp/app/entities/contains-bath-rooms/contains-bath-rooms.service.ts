import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';

type EntityResponseType = HttpResponse<IContainsBathRooms>;
type EntityArrayResponseType = HttpResponse<IContainsBathRooms[]>;

@Injectable({ providedIn: 'root' })
export class ContainsBathRoomsService {
  public resourceUrl = SERVER_API_URL + 'api/contains-bath-rooms';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/contains-bath-rooms';

  constructor(protected http: HttpClient) {}

  create(containsBathRooms: IContainsBathRooms): Observable<EntityResponseType> {
    return this.http.post<IContainsBathRooms>(this.resourceUrl, containsBathRooms, { observe: 'response' });
  }

  update(containsBathRooms: IContainsBathRooms): Observable<EntityResponseType> {
    return this.http.put<IContainsBathRooms>(this.resourceUrl, containsBathRooms, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IContainsBathRooms>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findHousingBathrooms(housingTemplateId: number): Observable<EntityArrayResponseType> {
    return this.http.get<IContainsBathRooms[]>(`${this.resourceUrl}/housing-template_id/${housingTemplateId}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IContainsBathRooms[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IContainsBathRooms[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
