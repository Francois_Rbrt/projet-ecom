import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IContainsBathRooms, ContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';
import { ContainsBathRoomsService } from './contains-bath-rooms.service';
import { ContainsBathRoomsComponent } from './contains-bath-rooms.component';
import { ContainsBathRoomsDetailComponent } from './contains-bath-rooms-detail.component';
import { ContainsBathRoomsUpdateComponent } from './contains-bath-rooms-update.component';

@Injectable({ providedIn: 'root' })
export class ContainsBathRoomsResolve implements Resolve<IContainsBathRooms> {
  constructor(private service: ContainsBathRoomsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IContainsBathRooms> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((containsBathRooms: HttpResponse<ContainsBathRooms>) => {
          if (containsBathRooms.body) {
            return of(containsBathRooms.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ContainsBathRooms());
  }
}

export const containsBathRoomsRoute: Routes = [
  {
    path: '',
    component: ContainsBathRoomsComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBathRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ContainsBathRoomsDetailComponent,
    resolve: {
      containsBathRooms: ContainsBathRoomsResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBathRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ContainsBathRoomsUpdateComponent,
    resolve: {
      containsBathRooms: ContainsBathRoomsResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBathRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ContainsBathRoomsUpdateComponent,
    resolve: {
      containsBathRooms: ContainsBathRoomsResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'afaleApp.containsBathRooms.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
