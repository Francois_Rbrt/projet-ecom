import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';
import { ContainsBathRoomsService } from './contains-bath-rooms.service';

@Component({
  templateUrl: './contains-bath-rooms-delete-dialog.component.html',
})
export class ContainsBathRoomsDeleteDialogComponent {
  containsBathRooms?: IContainsBathRooms;

  constructor(
    protected containsBathRoomsService: ContainsBathRoomsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.containsBathRoomsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('containsBathRoomsListModification');
      this.activeModal.close();
    });
  }
}
