import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IContainsBathRooms, ContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';
import { ContainsBathRoomsService } from './contains-bath-rooms.service';
import { IHousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { IBathRoomTemplate } from 'app/shared/model/bath-room-template.model';
import { BathRoomTemplateService } from 'app/entities/bath-room-template/bath-room-template.service';

type SelectableEntity = IHousingTemplate | IBathRoomTemplate;

@Component({
  selector: 'jhi-contains-bath-rooms-update',
  templateUrl: './contains-bath-rooms-update.component.html',
})
export class ContainsBathRoomsUpdateComponent implements OnInit {
  isSaving = false;
  housingtemplates: IHousingTemplate[] = [];
  bathroomtemplates: IBathRoomTemplate[] = [];

  editForm = this.fb.group({
    id: [],
    nbOfUnit: [null, [Validators.required, Validators.min(0)]],
    housingTemplateId: [],
    bathroomTemplateId: [],
  });

  constructor(
    protected containsBathRoomsService: ContainsBathRoomsService,
    protected housingTemplateService: HousingTemplateService,
    protected bathRoomTemplateService: BathRoomTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ containsBathRooms }) => {
      this.updateForm(containsBathRooms);

      this.housingTemplateService.query().subscribe((res: HttpResponse<IHousingTemplate[]>) => (this.housingtemplates = res.body || []));

      this.bathRoomTemplateService.query().subscribe((res: HttpResponse<IBathRoomTemplate[]>) => (this.bathroomtemplates = res.body || []));
    });
  }

  updateForm(containsBathRooms: IContainsBathRooms): void {
    this.editForm.patchValue({
      id: containsBathRooms.id,
      nbOfUnit: containsBathRooms.nbOfUnit,
      housingTemplateId: containsBathRooms.housingTemplateId,
      bathroomTemplateId: containsBathRooms.bathroomTemplateId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const containsBathRooms = this.createFromForm();
    if (containsBathRooms.id !== undefined) {
      this.subscribeToSaveResponse(this.containsBathRoomsService.update(containsBathRooms));
    } else {
      this.subscribeToSaveResponse(this.containsBathRoomsService.create(containsBathRooms));
    }
  }

  private createFromForm(): IContainsBathRooms {
    return {
      ...new ContainsBathRooms(),
      id: this.editForm.get(['id'])!.value,
      nbOfUnit: this.editForm.get(['nbOfUnit'])!.value,
      housingTemplateId: this.editForm.get(['housingTemplateId'])!.value,
      bathroomTemplateId: this.editForm.get(['bathroomTemplateId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContainsBathRooms>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
