import { Route } from '@angular/router';

import { RegisterLoginComponent } from './register-login.component';

export const registerLoginRoutes: Route = {
  path: 'register-login',
  component: RegisterLoginComponent,
  data: {
    authorities: [],
    pageTitle: 'register-login.title',
  },
};
