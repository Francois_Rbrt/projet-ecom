package com.afale;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class BookingSuppressor {
    private final Logger log = LoggerFactory.getLogger(BookingSuppressor.class);

    /*@Autowired
    BookingLock bookingLock;*/
    private BookingLock bookingLock;
    @Autowired
    public BookingSuppressor(BookingLock bookingLock) {
         this.bookingLock = bookingLock;
    }

    @Scheduled(fixedRate=15000)
    public void checkBookings() {
        bookingLock.checkBookings();
    }
}
