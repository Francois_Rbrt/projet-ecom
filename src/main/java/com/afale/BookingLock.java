package com.afale;

import com.afale.domain.Booking;
import com.afale.domain.BookingElement;
import com.afale.repository.BookingElementRepository;
import com.afale.repository.BookingRepository;
import com.afale.repository.search.BookingSearchRepository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookingLock {
    private final Logger log = LoggerFactory.getLogger(BookingLock.class);

    public ArrayList<Long> mapId;
    public ArrayList<LocalDateTime> mapTime;

    private final BookingRepository bookingRepository;
    private final BookingElementRepository bookingElementRepository;


    public BookingLock(BookingRepository bookingRepository, BookingSearchRepository bookingSearchRepository, BookingElementRepository bookingElementRepository) {
        this.bookingRepository = bookingRepository;
        this.bookingElementRepository = bookingElementRepository;
        mapId=new ArrayList<>();
        mapTime=new ArrayList<>();
    }

    /*@GetMapping("/verification/{id}")
    public void verification(@PathVariable Long id) {
        log.debug("REST request to verify if a booking is valid or not :"+id);
        //TODO : regarder si le booking existe encore ou pas / si ca fait plus de 15 min dans la hashmap et renvoyer un code d'erreur ou OK
    }*/

    //TODO
    public synchronized void addNewBookingToCheck(Long id,LocalDateTime serverCreateTime){
        log.debug("ajout d'un nouveau booking'"+id+ " / "+serverCreateTime);
        //ajouter dans la hashmap le nouveau booking a surveiller
        mapId.add(id);
        mapTime.add(serverCreateTime);
    }

    //TODO
    public synchronized void removeBookingToCheck(Long id){
        log.debug("suppression d'un nouveau booking'");
        //supprimer de la hashmap le nouveau booking a surveiller
        //appellé par le bean schedule
        //on supprime le temmps qu iest stocké au même index que l'id du booking qu'on va supprimer
        mapTime.remove(mapId.indexOf(id));
        mapId.remove(id);
        Optional<Booking> b=bookingRepository.findById(id);
        if(b.isPresent()) {
            ArrayList<BookingElement> list_be= (ArrayList<BookingElement>) bookingElementRepository.getBookingElementFromBookingId(b.get());
            for(BookingElement be: list_be) {
                log.debug("REST request to delete BookingElement : {}", be.getId());
                bookingElementRepository.deleteById(be.getId());
            }
            bookingRepository.deleteById(id);
            //bookingSearchRepository.deleteById(id);
        }
    }

    public synchronized void noNeedtoCheck(Long id){
        if(mapId.contains(id)){
            //on supprime le temmps qu iest stocké au même index que l'id du booking qu'on va supprimer
            mapTime.remove(mapId.indexOf(id));
            mapId.remove(id);
        }
    }

    public synchronized void checkBookings(){
        LocalDateTime now=LocalDateTime.now();
        log.debug("Check booking to delete ...............");
        for (Long id : mapId) {
            log.debug("id : " + id);
            log.debug("time : " + mapTime.get(mapId.indexOf(id)));
        }
        long i=0;
            if (mapId.size()!=0) {
                for (Long id : mapId) {
                    long minutes_ecoule = -1*(now.until(this.mapTime.get(mapId.indexOf(id)), ChronoUnit.MINUTES));
                    log.debug("Booking créé depuis : "+minutes_ecoule+ " min");
                    if (minutes_ecoule >= 3) {
                        log.debug("Suppresion du booking : " + id + " car il est pas validé depuis trop longtemps");
                        removeBookingToCheck(id);
                    }
                    i++;
                }
        }
    }
}
