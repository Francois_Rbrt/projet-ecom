package com.afale.repository;

import com.afale.domain.BedRoomTemplate;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BedRoomTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BedRoomTemplateRepository extends JpaRepository<BedRoomTemplate, Long> {
}
