package com.afale.repository;

import com.afale.domain.Rating;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import com.afale.domain.Establishment;

/**
 * Spring Data  repository for the Rating entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

    @Query("select rating from Rating rating where rating.userId.login = ?#{principal.username}")
    List<Rating> findByUserIdIsCurrentUser();

    @Query("select count(rating) from Rating rating where rating.establishmentId = (:id)")
    Optional<Number> countById(@Param("id") Establishment id);
}
