package com.afale.repository;

import com.afale.domain.Establishment;
import com.afale.domain.HousingTemplate;
import com.afale.domain.Photo;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data  repository for the Photo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {
    @Query("select p from Photo p where p.establishmentId = (:e)")
    List<Photo> getPhotoFromEstablishmentId(@Param("e") Establishment e);

    @Query("select p from Photo p where p.housingTemplateId = (:ht)")
    List<Photo> getPhotoFromHousingTemplateId( @Param("ht") HousingTemplate ht);
}
