package com.afale.repository;

import com.afale.domain.BathRoomTemplate;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BathRoomTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BathRoomTemplateRepository extends JpaRepository<BathRoomTemplate, Long> {
}
