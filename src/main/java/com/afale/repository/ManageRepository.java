package com.afale.repository;

import com.afale.domain.Establishment;
import com.afale.domain.Manage;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Manage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ManageRepository extends JpaRepository<Manage, Long> {

    @Query("select manage from Manage manage where manage.userId.login = ?#{principal.username}")
    List<Manage> findByUserIdIsCurrentUser();

    @Query("select m from Manage m where m.establishmentId = (:e)")
    Optional<Manage> findByEstablishment(@Param("e") Establishment e);
}
