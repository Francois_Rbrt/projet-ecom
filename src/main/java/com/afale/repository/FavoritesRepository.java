package com.afale.repository;

import com.afale.domain.Favorites;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Favorites entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FavoritesRepository extends JpaRepository<Favorites, Long> {

    @Query("select favorites from Favorites favorites where favorites.userId.login = ?#{principal.username}")
    List<Favorites> findByUserIdIsCurrentUser();
}
