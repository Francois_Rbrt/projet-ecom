package com.afale.repository;

import com.afale.domain.HousingClosure;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the HousingClosure entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HousingClosureRepository extends JpaRepository<HousingClosure, Long> {
}
