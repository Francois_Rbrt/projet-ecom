package com.afale.repository;

import com.afale.domain.HousingTemplate;
import com.afale.domain.Establishment;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the HousingTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HousingTemplateRepository extends JpaRepository<HousingTemplate, Long> {
       @Query("select ht from HousingTemplate ht where ht.establishmentId = (:e)")
       List<HousingTemplate> getHousingTemplateFromEstablishmentId( @Param("e") Establishment e);

       @Query("select min(ht.pricePerNight) from HousingTemplate ht where ht.establishmentId = (:e)")
       Optional<Number> getMin(@Param("e") Establishment e);
}
