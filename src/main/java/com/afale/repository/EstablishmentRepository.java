package com.afale.repository;

import com.afale.domain.Establishment;
import com.afale.domain.HousingTemplate;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Establishment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EstablishmentRepository extends JpaRepository<Establishment, Long>{
}
