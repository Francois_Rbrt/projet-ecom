package com.afale.repository.search;

import com.afale.domain.Manage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Manage} entity.
 */
public interface ManageSearchRepository extends ElasticsearchRepository<Manage, Long> {
}
