package com.afale.repository.search;

import com.afale.domain.Establishment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Establishment} entity.
 */
public interface EstablishmentSearchRepository extends ElasticsearchRepository<Establishment, Long> {
}
