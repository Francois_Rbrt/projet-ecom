package com.afale.repository.search;

import com.afale.domain.EstablishmentClosure;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link EstablishmentClosure} entity.
 */
public interface EstablishmentClosureSearchRepository extends ElasticsearchRepository<EstablishmentClosure, Long> {
}
