package com.afale.repository.search;

import com.afale.domain.Rating;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Rating} entity.
 */
public interface RatingSearchRepository extends ElasticsearchRepository<Rating, Long> {
}
