package com.afale.repository.search;

import com.afale.domain.BathRoomTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link BathRoomTemplate} entity.
 */
public interface BathRoomTemplateSearchRepository extends ElasticsearchRepository<BathRoomTemplate, Long> {
}
