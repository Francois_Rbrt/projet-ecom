package com.afale.repository.search;

import com.afale.domain.ContainsBathRooms;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link ContainsBathRooms} entity.
 */
public interface ContainsBathRoomsSearchRepository extends ElasticsearchRepository<ContainsBathRooms, Long> {
}
