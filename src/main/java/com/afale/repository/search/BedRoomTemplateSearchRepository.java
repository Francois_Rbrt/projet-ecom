package com.afale.repository.search;

import com.afale.domain.BedRoomTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link BedRoomTemplate} entity.
 */
public interface BedRoomTemplateSearchRepository extends ElasticsearchRepository<BedRoomTemplate, Long> {
}
