package com.afale.repository.search;

import com.afale.domain.BookingElement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link BookingElement} entity.
 */
public interface BookingElementSearchRepository extends ElasticsearchRepository<BookingElement, Long> {
}
