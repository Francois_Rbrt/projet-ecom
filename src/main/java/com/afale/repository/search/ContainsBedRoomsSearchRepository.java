package com.afale.repository.search;

import com.afale.domain.ContainsBedRooms;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link ContainsBedRooms} entity.
 */
public interface ContainsBedRoomsSearchRepository extends ElasticsearchRepository<ContainsBedRooms, Long> {
}
