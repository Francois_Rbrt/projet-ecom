package com.afale.repository.search;

import com.afale.domain.HousingClosure;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link HousingClosure} entity.
 */
public interface HousingClosureSearchRepository extends ElasticsearchRepository<HousingClosure, Long> {
}
