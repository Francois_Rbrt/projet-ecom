package com.afale.repository.search;

import com.afale.domain.HousingTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link HousingTemplate} entity.
 */
public interface HousingTemplateSearchRepository extends ElasticsearchRepository<HousingTemplate, Long> {
}
