package com.afale.repository.search;

import com.afale.domain.Favorites;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Favorites} entity.
 */
public interface FavoritesSearchRepository extends ElasticsearchRepository<Favorites, Long> {
}
