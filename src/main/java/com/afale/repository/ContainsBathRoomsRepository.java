package com.afale.repository;

import com.afale.domain.ContainsBathRooms;

import com.afale.domain.ContainsBedRooms;
import com.afale.domain.HousingTemplate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data  repository for the ContainsBathRooms entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContainsBathRoomsRepository extends JpaRepository<ContainsBathRooms, Long> {
    @Query("select cbr from ContainsBathRooms cbr where cbr.housingTemplateId = (:ht)")
    List<ContainsBathRooms> getContainsBathRoomsFromHousingTemplateId(@Param("ht") HousingTemplate ht);
}
