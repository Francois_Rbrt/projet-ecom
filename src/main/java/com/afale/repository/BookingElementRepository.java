package com.afale.repository;

import com.afale.domain.Booking;
import com.afale.domain.BookingElement;

import com.afale.domain.Establishment;
import com.afale.domain.HousingTemplate;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the BookingElement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingElementRepository extends JpaRepository<BookingElement, Long> {

    @Query("select be from BookingElement be where be.bookingId = (:b)")
    List<BookingElement> getBookingElementFromBookingId(@Param("b") Booking b);

    @Query("select be from BookingElement be where be.housingTemplateId = (:ht)")
    List<BookingElement> getBookingElementFromHT(@Param("ht") HousingTemplate ht);


}

