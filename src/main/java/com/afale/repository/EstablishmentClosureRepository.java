package com.afale.repository;

import com.afale.domain.EstablishmentClosure;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the EstablishmentClosure entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EstablishmentClosureRepository extends JpaRepository<EstablishmentClosure, Long> {
}
