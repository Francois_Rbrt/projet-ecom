package com.afale.repository;

import com.afale.domain.ContainsBedRooms;

import com.afale.domain.Establishment;
import com.afale.domain.HousingTemplate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data  repository for the ContainsBedRooms entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContainsBedRoomsRepository extends JpaRepository<ContainsBedRooms, Long> {

    @Query("select cbr from ContainsBedRooms cbr where cbr.housingTemplateId = (:ht)")
    List<ContainsBedRooms> getContainsBedRoomsFromHousingTemplateId(@Param("ht") HousingTemplate ht);
}
