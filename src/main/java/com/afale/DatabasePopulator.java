package com.afale;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.afale.repository.search.EstablishmentSearchRepository;
import com.afale.repository.search.HousingTemplateSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.afale.repository.*;
import com.afale.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing the population of the database during development.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DatabasePopulator {
    private final Logger log = LoggerFactory.getLogger(DatabasePopulator.class);

    private static final String ENTITY_NAME = "DatabasePopulator";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    //all the repositories needed
    private final HousingTemplateRepository housingTemplateRepository;
    private final EstablishmentRepository establishmentRepository;
    private final ManageRepository manageRepository;
    private final PhotoRepository photoRepository;
    //elastic search repositories
    private final HousingTemplateSearchRepository housingTemplateSearchRepository;
    private final EstablishmentSearchRepository establishmentSearchRepository;

    public DatabasePopulator(HousingTemplateRepository housingTemplateRepository, EstablishmentRepository establishmentRepository, ManageRepository manageRepository, PhotoRepository photoRepository, HousingTemplateSearchRepository housingTemplateSearchRepository, EstablishmentSearchRepository establishmentSearchRepository) {
        this.housingTemplateRepository = housingTemplateRepository;
        this.establishmentRepository = establishmentRepository;
        this.manageRepository = manageRepository;
        this.photoRepository = photoRepository;
        this.housingTemplateSearchRepository = housingTemplateSearchRepository;
        this.establishmentSearchRepository = establishmentSearchRepository;
    }
    /*@Autowired
    HousingTemplateRepository housingTemplateRepository;
    @Autowired
    EstablishmentRepository establishmentRepository;*/


    /**
         * {@code GET  /init} : init examples in the database.
         *
         *
         * @return the {@link ResponseEntity} with status {@code 201 (Created)}, or with status {@code 400 (Bad Request)} if the housingTemplate has already an ID.
         * @throws URISyntaxException if the Location URI syntax is incorrect.
         */
        @GetMapping("/init")
        public void init() throws URISyntaxException {
            log.debug("REST request to populate the database BAMBIIIIIIIIIIIIIIIIIII");
            populate();
        }

       @GetMapping("/init/{id}")
           public void getManage(@PathVariable Long id) {
            log.debug("REST request to populate the database BAMBIIIIIIIIIIIIIIIIIII");
            populate();

           }

	public void populate() {
		//final List<Establishment> establishments = createEstablishments();
		//establishmentRepository.saveAll(establishments);
		/*
		HousingTemplate result = housingTemplateRepository.save(housingTemplate);
                    housingTemplateSearchRepository.save(result);
                    */
        /*Optional<HousingTemplate> chambre_simple_hotel_eclate = housingTemplateRepository.findById((long)1);
        Optional<Establishment> hotel_eclate = establishmentRepository.findById((long)4);
        chambre_simple_hotel_eclate.get().setEstablishmentId(hotel_eclate.get());*/

        //INIT OF HOUSING TEMPLATES
        //TODO: elastic search indexing ?
        setHT_establishment_id(1,4);
        setHT_establishment_id(2,5);
        setHT_establishment_id(3,5);
        setHT_establishment_id(4,6);
        setHT_establishment_id(5,6);
        setHT_establishment_id(6,6);

        setPhoto_owner(1,4,4);
        setPhoto_owner(2,4,4);
        setPhoto_owner(3,5,5);
        setPhoto_owner(4,5,5);
        setPhoto_owner(5,6,6);
        setPhoto_owner(6,6,6);
	}

    public void setHT_establishment_id(int ht_id,int e_id){
            Optional<Establishment> e=establishmentRepository.findById((long)e_id);
            if (e.isPresent()){
                Optional<HousingTemplate> ht=housingTemplateRepository.findById((long)ht_id);
                if(ht.isPresent()){
                    ht.get().setEstablishmentId(e.get());
                    housingTemplateRepository.save(ht.get());
                }
                else{
                    System.out.println("HT not found bambi!");
                }
            }
            else{
                System.out.println("Establishment not found bambi!");
            }
    }
    public void setPhoto_owner(int photo_id,int e_id,int ht_id){
        Optional<Establishment> e=establishmentRepository.findById((long)e_id);
        Optional<HousingTemplate> ht=housingTemplateRepository.findById((long)ht_id);
        if (e.isPresent() || ht.isPresent()){
            Optional<Photo> p=photoRepository.findById((long)photo_id);
            if(e.isPresent() && p.isPresent()){
                p.get().setEstablishmentId(e.get());
                photoRepository.save(p.get());
            }
            if(ht.isPresent() && p.isPresent()){
                p.get().setHousingTemplateId(ht.get());
                photoRepository.save(p.get());
            }
        }
    }
	/*private List<Product> createProducts(int nbProducts) {
		List<Product> products = new ArrayList<Product>();
		for (int i = 0; i < nbProducts; i++) {
			final Product product = new Product();
			product.setName("Pant popper " + (i+1));
			product.setStock(5 + i);
			product.setPrice(10 + i);
			products.add(product);
		}
		return products;
	}*/

	/*private List<Establishment> createEstablishments() {
    		List<Establishment> establishments = new ArrayList<Establishment>();
    			final Establishment e = new Establishment();
    			long id=20;
    			e.setId(id);
    			e.setName("lalala");
    			e.setAdress("fgdfgdg");
    			e.setLatitude((float)55555.);
    			e.setLongitude((float)44444.);
    			e.setGlobalRate((float)4.);
    			e.setEstablishmentType(EstablishmentType.HOTEL);
    			e.setHasParking(true);
    			e.setHasRestaurant(true);
    			e.setHasFreeWifi(true);
    			e.setHasSwimmingPool(true);
    			e.setDescription("lalalalallalalalaaaaaaaaa");
    			establishments.add(e);
    		return establishments;
    	}*/

}
