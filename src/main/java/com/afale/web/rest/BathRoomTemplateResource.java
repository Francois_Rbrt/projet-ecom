package com.afale.web.rest;

import com.afale.domain.BathRoomTemplate;
import com.afale.repository.BathRoomTemplateRepository;
import com.afale.repository.search.BathRoomTemplateSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.BathRoomTemplate}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BathRoomTemplateResource {

    private final Logger log = LoggerFactory.getLogger(BathRoomTemplateResource.class);

    private static final String ENTITY_NAME = "bathRoomTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BathRoomTemplateRepository bathRoomTemplateRepository;

    private final BathRoomTemplateSearchRepository bathRoomTemplateSearchRepository;

    public BathRoomTemplateResource(BathRoomTemplateRepository bathRoomTemplateRepository, BathRoomTemplateSearchRepository bathRoomTemplateSearchRepository) {
        this.bathRoomTemplateRepository = bathRoomTemplateRepository;
        this.bathRoomTemplateSearchRepository = bathRoomTemplateSearchRepository;
    }

    /**
     * {@code POST  /bath-room-templates} : Create a new bathRoomTemplate.
     *
     * @param bathRoomTemplate the bathRoomTemplate to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bathRoomTemplate, or with status {@code 400 (Bad Request)} if the bathRoomTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bath-room-templates")
    public ResponseEntity<BathRoomTemplate> createBathRoomTemplate(@RequestBody BathRoomTemplate bathRoomTemplate) throws URISyntaxException {
        log.debug("REST request to save BathRoomTemplate : {}", bathRoomTemplate);
        if (bathRoomTemplate.getId() != null) {
            throw new BadRequestAlertException("A new bathRoomTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BathRoomTemplate result = bathRoomTemplateRepository.save(bathRoomTemplate);
        bathRoomTemplateSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/bath-room-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bath-room-templates} : Updates an existing bathRoomTemplate.
     *
     * @param bathRoomTemplate the bathRoomTemplate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bathRoomTemplate,
     * or with status {@code 400 (Bad Request)} if the bathRoomTemplate is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bathRoomTemplate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bath-room-templates")
    public ResponseEntity<BathRoomTemplate> updateBathRoomTemplate(@RequestBody BathRoomTemplate bathRoomTemplate) throws URISyntaxException {
        log.debug("REST request to update BathRoomTemplate : {}", bathRoomTemplate);
        if (bathRoomTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BathRoomTemplate result = bathRoomTemplateRepository.save(bathRoomTemplate);
        bathRoomTemplateSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bathRoomTemplate.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bath-room-templates} : get all the bathRoomTemplates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bathRoomTemplates in body.
     */
    @GetMapping("/bath-room-templates")
    public List<BathRoomTemplate> getAllBathRoomTemplates() {
        log.debug("REST request to get all BathRoomTemplates");
        return bathRoomTemplateRepository.findAll();
    }

    /**
     * {@code GET  /bath-room-templates/:id} : get the "id" bathRoomTemplate.
     *
     * @param id the id of the bathRoomTemplate to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bathRoomTemplate, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bath-room-templates/{id}")
    public ResponseEntity<BathRoomTemplate> getBathRoomTemplate(@PathVariable Long id) {
        log.debug("REST request to get BathRoomTemplate : {}", id);
        Optional<BathRoomTemplate> bathRoomTemplate = bathRoomTemplateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bathRoomTemplate);
    }

    /**
     * {@code DELETE  /bath-room-templates/:id} : delete the "id" bathRoomTemplate.
     *
     * @param id the id of the bathRoomTemplate to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bath-room-templates/{id}")
    public ResponseEntity<Void> deleteBathRoomTemplate(@PathVariable Long id) {
        log.debug("REST request to delete BathRoomTemplate : {}", id);
        bathRoomTemplateRepository.deleteById(id);
        bathRoomTemplateSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/bath-room-templates?query=:query} : search for the bathRoomTemplate corresponding
     * to the query.
     *
     * @param query the query of the bathRoomTemplate search.
     * @return the result of the search.
     */
    @GetMapping("/_search/bath-room-templates")
    public List<BathRoomTemplate> searchBathRoomTemplates(@RequestParam String query) {
        log.debug("REST request to search BathRoomTemplates for query {}", query);
        return StreamSupport
            .stream(bathRoomTemplateSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
