package com.afale.web.rest;

import com.afale.domain.HousingTemplate;
import com.afale.repository.HousingTemplateRepository;
import com.afale.domain.Establishment;
import com.afale.repository.EstablishmentRepository;
import com.afale.repository.search.HousingTemplateSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.HousingTemplate}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class HousingTemplateResource {

    private final Logger log = LoggerFactory.getLogger(HousingTemplateResource.class);

    private static final String ENTITY_NAME = "housingTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HousingTemplateRepository housingTemplateRepository;
    private final EstablishmentRepository establishmentRepository;

    private final HousingTemplateSearchRepository housingTemplateSearchRepository;

    public HousingTemplateResource(HousingTemplateRepository housingTemplateRepository, HousingTemplateSearchRepository housingTemplateSearchRepository,EstablishmentRepository establishmentRepository) {
        this.housingTemplateRepository = housingTemplateRepository;
        this.housingTemplateSearchRepository = housingTemplateSearchRepository;
        this.establishmentRepository = establishmentRepository;
    }

    /**
     * {@code POST  /housing-templates} : Create a new housingTemplate.
     *
     * @param housingTemplate the housingTemplate to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new housingTemplate, or with status {@code 400 (Bad Request)} if the housingTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/housing-templates")
    public ResponseEntity<HousingTemplate> createHousingTemplate(@Valid @RequestBody HousingTemplate housingTemplate) throws URISyntaxException {
        log.debug("REST request to save HousingTemplate : {}", housingTemplate);
        if (housingTemplate.getId() != null) {
            throw new BadRequestAlertException("A new housingTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HousingTemplate result = housingTemplateRepository.save(housingTemplate);
        housingTemplateSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/housing-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /housing-templates} : Updates an existing housingTemplate.
     *
     * @param housingTemplate the housingTemplate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated housingTemplate,
     * or with status {@code 400 (Bad Request)} if the housingTemplate is not valid,
     * or with status {@code 500 (Internal Server Error)} if the housingTemplate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/housing-templates")
    public ResponseEntity<HousingTemplate> updateHousingTemplate(@Valid @RequestBody HousingTemplate housingTemplate) throws URISyntaxException {
        log.debug("REST request to update HousingTemplate : {}", housingTemplate);
        if (housingTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HousingTemplate result = housingTemplateRepository.save(housingTemplate);
        housingTemplateSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, housingTemplate.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /housing-templates} : get all the housingTemplates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of housingTemplates in body.
     */
    @GetMapping("/housing-templates")
    public List<HousingTemplate> getAllHousingTemplates() {
        log.debug("REST request to get all HousingTemplates");
        return housingTemplateRepository.findAll();
    }

    /**
     * {@code GET  /housing-templates/:id} : get the "id" housingTemplate.
     *
     * @param id the id of the housingTemplate to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the housingTemplate, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/housing-templates/{id}")
    public ResponseEntity<HousingTemplate> getHousingTemplate(@PathVariable Long id) {
        log.debug("REST request to get HousingTemplate : {}", id);
        Optional<HousingTemplate> housingTemplate = housingTemplateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(housingTemplate);
    }

     /**
         * {@code GET  /housing-templates/establishment_id/:id} : get the housingTemplates corresponding to a specific establishment.
         *
         * @param id the id of the establishment
         * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the housingTemplates, or with status {@code 404 (Not Found)}.
         */
        @GetMapping("/housing-templates/establishment_id/{id}")
        public List<HousingTemplate> getHousingTemplateFromEstablishmentId(@PathVariable Long id) {
            //@RequestParam("establishmentId") long id
            log.debug("REST request to get HousingTemplates for establishment : {}", id);
            //TODO : faire la vraie requete
            Optional<Establishment>  e= establishmentRepository.findById(id);
            if(e.isPresent()){
                return housingTemplateRepository.getHousingTemplateFromEstablishmentId(e.get());
            }
            else
                return new ArrayList<HousingTemplate>();
        }

    /**
     * {@code DELETE  /housing-templates/:id} : delete the "id" housingTemplate.
     *
     * @param id the id of the housingTemplate to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/housing-templates/{id}")
    public ResponseEntity<Void> deleteHousingTemplate(@PathVariable Long id) {
        log.debug("REST request to delete HousingTemplate : {}", id);
        housingTemplateRepository.deleteById(id);
        housingTemplateSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/housing-templates?query=:query} : search for the housingTemplate corresponding
     * to the query.
     *
     * @param query the query of the housingTemplate search.
     * @return the result of the search.
     */
    @GetMapping("/_search/housing-templates")
    public List<HousingTemplate> searchHousingTemplates(@RequestParam String query) {
        log.debug("REST request to search HousingTemplates for query {}", query);
        return StreamSupport
            .stream(housingTemplateSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
