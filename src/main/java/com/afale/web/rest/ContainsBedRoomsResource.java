package com.afale.web.rest;

import com.afale.domain.ContainsBedRooms;
import com.afale.domain.HousingTemplate;
import com.afale.repository.ContainsBedRoomsRepository;
import com.afale.repository.HousingTemplateRepository;
import com.afale.repository.search.ContainsBedRoomsSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.ContainsBedRooms}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ContainsBedRoomsResource {

    private final Logger log = LoggerFactory.getLogger(ContainsBedRoomsResource.class);

    private static final String ENTITY_NAME = "containsBedRooms";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContainsBedRoomsRepository containsBedRoomsRepository;
    private final HousingTemplateRepository housingTemplateRepository;

    private final ContainsBedRoomsSearchRepository containsBedRoomsSearchRepository;

    public ContainsBedRoomsResource(ContainsBedRoomsRepository containsBedRoomsRepository, HousingTemplateRepository housingTemplateRepository, ContainsBedRoomsSearchRepository containsBedRoomsSearchRepository) {
        this.containsBedRoomsRepository = containsBedRoomsRepository;
        this.housingTemplateRepository = housingTemplateRepository;
        this.containsBedRoomsSearchRepository = containsBedRoomsSearchRepository;
    }

    /**
     * {@code POST  /contains-bed-rooms} : Create a new containsBedRooms.
     *
     * @param containsBedRooms the containsBedRooms to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new containsBedRooms, or with status {@code 400 (Bad Request)} if the containsBedRooms has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contains-bed-rooms")
    public ResponseEntity<ContainsBedRooms> createContainsBedRooms(@Valid @RequestBody ContainsBedRooms containsBedRooms) throws URISyntaxException {
        log.debug("REST request to save ContainsBedRooms : {}", containsBedRooms);
        if (containsBedRooms.getId() != null) {
            throw new BadRequestAlertException("A new containsBedRooms cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContainsBedRooms result = containsBedRoomsRepository.save(containsBedRooms);
        containsBedRoomsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/contains-bed-rooms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contains-bed-rooms} : Updates an existing containsBedRooms.
     *
     * @param containsBedRooms the containsBedRooms to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated containsBedRooms,
     * or with status {@code 400 (Bad Request)} if the containsBedRooms is not valid,
     * or with status {@code 500 (Internal Server Error)} if the containsBedRooms couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contains-bed-rooms")
    public ResponseEntity<ContainsBedRooms> updateContainsBedRooms(@Valid @RequestBody ContainsBedRooms containsBedRooms) throws URISyntaxException {
        log.debug("REST request to update ContainsBedRooms : {}", containsBedRooms);
        if (containsBedRooms.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContainsBedRooms result = containsBedRoomsRepository.save(containsBedRooms);
        containsBedRoomsSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, containsBedRooms.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contains-bed-rooms} : get all the containsBedRooms.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of containsBedRooms in body.
     */
    @GetMapping("/contains-bed-rooms")
    public List<ContainsBedRooms> getAllContainsBedRooms() {
        log.debug("REST request to get all ContainsBedRooms");
        return containsBedRoomsRepository.findAll();
    }

    /**
     * {@code GET  /contains-bed-rooms/:id} : get the "id" containsBedRooms.
     *
     * @param id the id of the containsBedRooms to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the containsBedRooms, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contains-bed-rooms/{id}")
    public ResponseEntity<ContainsBedRooms> getContainsBedRooms(@PathVariable Long id) {
        log.debug("REST request to get ContainsBedRooms : {}", id);
        Optional<ContainsBedRooms> containsBedRooms = containsBedRoomsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(containsBedRooms);
    }

    /**
     * {@code GET  /contains-bed-rooms/housing-template_id/:id} : get the contains bed room object to a specific housing template.
     *
     * @param id the id of the housing template
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contains bed rooms, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contains-bed-rooms/housing-template_id/{id}")
    public List<ContainsBedRooms> getContainsBedRoomsFromHousingTemplateId(@PathVariable Long id) {
        log.debug("REST request to get the contains bed room object to a specific housing template : {}", id);
        Optional<HousingTemplate>  ht= housingTemplateRepository.findById(id);
        if(ht.isPresent()){
            return containsBedRoomsRepository.getContainsBedRoomsFromHousingTemplateId(ht.get());
        }
        else
            return new ArrayList<ContainsBedRooms>();
    }

    /**
     * {@code DELETE  /contains-bed-rooms/:id} : delete the "id" containsBedRooms.
     *
     * @param id the id of the containsBedRooms to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contains-bed-rooms/{id}")
    public ResponseEntity<Void> deleteContainsBedRooms(@PathVariable Long id) {
        log.debug("REST request to delete ContainsBedRooms : {}", id);
        containsBedRoomsRepository.deleteById(id);
        containsBedRoomsSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/contains-bed-rooms?query=:query} : search for the containsBedRooms corresponding
     * to the query.
     *
     * @param query the query of the containsBedRooms search.
     * @return the result of the search.
     */
    @GetMapping("/_search/contains-bed-rooms")
    public List<ContainsBedRooms> searchContainsBedRooms(@RequestParam String query) {
        log.debug("REST request to search ContainsBedRooms for query {}", query);
        return StreamSupport
            .stream(containsBedRoomsSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
