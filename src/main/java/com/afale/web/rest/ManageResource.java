package com.afale.web.rest;

import com.afale.domain.Establishment;
import com.afale.domain.Manage;
import com.afale.repository.EstablishmentRepository;
import com.afale.repository.ManageRepository;
import com.afale.repository.search.ManageSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.Manage}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ManageResource {

    private final Logger log = LoggerFactory.getLogger(ManageResource.class);

    private static final String ENTITY_NAME = "manage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ManageRepository manageRepository;
    private final EstablishmentRepository establishmentRepository;

    private final ManageSearchRepository manageSearchRepository;

    public ManageResource(ManageRepository manageRepository, EstablishmentRepository establishmentRepository, ManageSearchRepository manageSearchRepository) {
        this.manageRepository = manageRepository;
        this.establishmentRepository = establishmentRepository;
        this.manageSearchRepository = manageSearchRepository;
    }

    /**
     * {@code POST  /manages} : Create a new manage.
     *
     * @param manage the manage to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new manage, or with status {@code 400 (Bad Request)} if the manage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/manages")
    public ResponseEntity<Manage> createManage(@RequestBody Manage manage) throws URISyntaxException {
        log.debug("REST request to save Manage : {}", manage);
        if (manage.getId() != null) {
            throw new BadRequestAlertException("A new manage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Manage result = manageRepository.save(manage);
        manageSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/manages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /manages} : Updates an existing manage.
     *
     * @param manage the manage to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated manage,
     * or with status {@code 400 (Bad Request)} if the manage is not valid,
     * or with status {@code 500 (Internal Server Error)} if the manage couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/manages")
    public ResponseEntity<Manage> updateManage(@RequestBody Manage manage) throws URISyntaxException {
        log.debug("REST request to update Manage : {}", manage);
        if (manage.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Manage result = manageRepository.save(manage);
        manageSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, manage.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /manages} : get all the manages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of manages in body.
     */
    @GetMapping("/manages")
    public List<Manage> getAllManages() {
        log.debug("REST request to get all Manages");
        return manageRepository.findAll();
    }

    /**
     * {@code GET  /manages/:id} : get the "id" manage.
     *
     * @param id the id of the manage to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the manage, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/manages/{id}")
    public ResponseEntity<Manage> getManage(@PathVariable Long id) {
        log.debug("REST request to get Manage : {}", id);
        Optional<Manage> manage = manageRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(manage);
    }

    /**
         * {@code GET  /manages/establishment_id/:establishmentId} : get the establishment manager.
         *
         * @param establishmentId the establishment of the manage to retrieve.
         * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the manage, or with status {@code 404 (Not Found)}.
         */
        @GetMapping("/manages/establishment_id/{establishmentId}")
        public ResponseEntity<Manage> getManageByEstablishment(@PathVariable Long establishmentId) {
            log.debug("REST request to get Manage : {}", establishmentId);
            Optional<Establishment>  e = establishmentRepository.findById(establishmentId);
            if(e.isPresent()){
                return ResponseUtil.wrapOrNotFound(manageRepository.findByEstablishment(e.get()));
            }
            return null;
        }



    /**
     * {@code DELETE  /manages/:id} : delete the "id" manage.
     *
     * @param id the id of the manage to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/manages/{id}")
    public ResponseEntity<Void> deleteManage(@PathVariable Long id) {
        log.debug("REST request to delete Manage : {}", id);
        manageRepository.deleteById(id);
        manageSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/manages?query=:query} : search for the manage corresponding
     * to the query.
     *
     * @param query the query of the manage search.
     * @return the result of the search.
     */
    @GetMapping("/_search/manages")
    public List<Manage> searchManages(@RequestParam String query) {
        log.debug("REST request to search Manages for query {}", query);
        return StreamSupport
            .stream(manageSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
