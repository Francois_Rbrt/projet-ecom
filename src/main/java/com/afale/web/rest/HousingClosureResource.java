package com.afale.web.rest;

import com.afale.domain.HousingClosure;
import com.afale.repository.HousingClosureRepository;
import com.afale.repository.search.HousingClosureSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.HousingClosure}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class HousingClosureResource {

    private final Logger log = LoggerFactory.getLogger(HousingClosureResource.class);

    private static final String ENTITY_NAME = "housingClosure";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HousingClosureRepository housingClosureRepository;

    private final HousingClosureSearchRepository housingClosureSearchRepository;

    public HousingClosureResource(HousingClosureRepository housingClosureRepository, HousingClosureSearchRepository housingClosureSearchRepository) {
        this.housingClosureRepository = housingClosureRepository;
        this.housingClosureSearchRepository = housingClosureSearchRepository;
    }

    /**
     * {@code POST  /housing-closures} : Create a new housingClosure.
     *
     * @param housingClosure the housingClosure to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new housingClosure, or with status {@code 400 (Bad Request)} if the housingClosure has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/housing-closures")
    public ResponseEntity<HousingClosure> createHousingClosure(@Valid @RequestBody HousingClosure housingClosure) throws URISyntaxException {
        log.debug("REST request to save HousingClosure : {}", housingClosure);
        if (housingClosure.getId() != null) {
            throw new BadRequestAlertException("A new housingClosure cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HousingClosure result = housingClosureRepository.save(housingClosure);
        housingClosureSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/housing-closures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /housing-closures} : Updates an existing housingClosure.
     *
     * @param housingClosure the housingClosure to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated housingClosure,
     * or with status {@code 400 (Bad Request)} if the housingClosure is not valid,
     * or with status {@code 500 (Internal Server Error)} if the housingClosure couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/housing-closures")
    public ResponseEntity<HousingClosure> updateHousingClosure(@Valid @RequestBody HousingClosure housingClosure) throws URISyntaxException {
        log.debug("REST request to update HousingClosure : {}", housingClosure);
        if (housingClosure.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HousingClosure result = housingClosureRepository.save(housingClosure);
        housingClosureSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, housingClosure.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /housing-closures} : get all the housingClosures.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of housingClosures in body.
     */
    @GetMapping("/housing-closures")
    public List<HousingClosure> getAllHousingClosures() {
        log.debug("REST request to get all HousingClosures");
        return housingClosureRepository.findAll();
    }

    /**
     * {@code GET  /housing-closures/:id} : get the "id" housingClosure.
     *
     * @param id the id of the housingClosure to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the housingClosure, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/housing-closures/{id}")
    public ResponseEntity<HousingClosure> getHousingClosure(@PathVariable Long id) {
        log.debug("REST request to get HousingClosure : {}", id);
        Optional<HousingClosure> housingClosure = housingClosureRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(housingClosure);
    }

    /**
     * {@code DELETE  /housing-closures/:id} : delete the "id" housingClosure.
     *
     * @param id the id of the housingClosure to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/housing-closures/{id}")
    public ResponseEntity<Void> deleteHousingClosure(@PathVariable Long id) {
        log.debug("REST request to delete HousingClosure : {}", id);
        housingClosureRepository.deleteById(id);
        housingClosureSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/housing-closures?query=:query} : search for the housingClosure corresponding
     * to the query.
     *
     * @param query the query of the housingClosure search.
     * @return the result of the search.
     */
    @GetMapping("/_search/housing-closures")
    public List<HousingClosure> searchHousingClosures(@RequestParam String query) {
        log.debug("REST request to search HousingClosures for query {}", query);
        return StreamSupport
            .stream(housingClosureSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
