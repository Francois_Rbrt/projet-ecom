package com.afale.web.rest;

import com.afale.domain.EstablishmentClosure;
import com.afale.repository.EstablishmentClosureRepository;
import com.afale.repository.search.EstablishmentClosureSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.EstablishmentClosure}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class EstablishmentClosureResource {

    private final Logger log = LoggerFactory.getLogger(EstablishmentClosureResource.class);

    private static final String ENTITY_NAME = "establishmentClosure";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EstablishmentClosureRepository establishmentClosureRepository;

    private final EstablishmentClosureSearchRepository establishmentClosureSearchRepository;

    public EstablishmentClosureResource(EstablishmentClosureRepository establishmentClosureRepository, EstablishmentClosureSearchRepository establishmentClosureSearchRepository) {
        this.establishmentClosureRepository = establishmentClosureRepository;
        this.establishmentClosureSearchRepository = establishmentClosureSearchRepository;
    }

    /**
     * {@code POST  /establishment-closures} : Create a new establishmentClosure.
     *
     * @param establishmentClosure the establishmentClosure to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new establishmentClosure, or with status {@code 400 (Bad Request)} if the establishmentClosure has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/establishment-closures")
    public ResponseEntity<EstablishmentClosure> createEstablishmentClosure(@Valid @RequestBody EstablishmentClosure establishmentClosure) throws URISyntaxException {
        log.debug("REST request to save EstablishmentClosure : {}", establishmentClosure);
        if (establishmentClosure.getId() != null) {
            throw new BadRequestAlertException("A new establishmentClosure cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EstablishmentClosure result = establishmentClosureRepository.save(establishmentClosure);
        establishmentClosureSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/establishment-closures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /establishment-closures} : Updates an existing establishmentClosure.
     *
     * @param establishmentClosure the establishmentClosure to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated establishmentClosure,
     * or with status {@code 400 (Bad Request)} if the establishmentClosure is not valid,
     * or with status {@code 500 (Internal Server Error)} if the establishmentClosure couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/establishment-closures")
    public ResponseEntity<EstablishmentClosure> updateEstablishmentClosure(@Valid @RequestBody EstablishmentClosure establishmentClosure) throws URISyntaxException {
        log.debug("REST request to update EstablishmentClosure : {}", establishmentClosure);
        if (establishmentClosure.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EstablishmentClosure result = establishmentClosureRepository.save(establishmentClosure);
        establishmentClosureSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, establishmentClosure.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /establishment-closures} : get all the establishmentClosures.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of establishmentClosures in body.
     */
    @GetMapping("/establishment-closures")
    public List<EstablishmentClosure> getAllEstablishmentClosures() {
        log.debug("REST request to get all EstablishmentClosures");
        return establishmentClosureRepository.findAll();
    }

    /**
     * {@code GET  /establishment-closures/:id} : get the "id" establishmentClosure.
     *
     * @param id the id of the establishmentClosure to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the establishmentClosure, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/establishment-closures/{id}")
    public ResponseEntity<EstablishmentClosure> getEstablishmentClosure(@PathVariable Long id) {
        log.debug("REST request to get EstablishmentClosure : {}", id);
        Optional<EstablishmentClosure> establishmentClosure = establishmentClosureRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(establishmentClosure);
    }

    /**
     * {@code DELETE  /establishment-closures/:id} : delete the "id" establishmentClosure.
     *
     * @param id the id of the establishmentClosure to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/establishment-closures/{id}")
    public ResponseEntity<Void> deleteEstablishmentClosure(@PathVariable Long id) {
        log.debug("REST request to delete EstablishmentClosure : {}", id);
        establishmentClosureRepository.deleteById(id);
        establishmentClosureSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/establishment-closures?query=:query} : search for the establishmentClosure corresponding
     * to the query.
     *
     * @param query the query of the establishmentClosure search.
     * @return the result of the search.
     */
    @GetMapping("/_search/establishment-closures")
    public List<EstablishmentClosure> searchEstablishmentClosures(@RequestParam String query) {
        log.debug("REST request to search EstablishmentClosures for query {}", query);
        return StreamSupport
            .stream(establishmentClosureSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
