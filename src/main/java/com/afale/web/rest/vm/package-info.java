/**
 * View Models used by Spring MVC REST controllers.
 */
package com.afale.web.rest.vm;
