package com.afale.web.rest;

import com.afale.domain.ContainsBathRooms;
import com.afale.domain.HousingTemplate;
import com.afale.repository.ContainsBathRoomsRepository;
import com.afale.repository.HousingTemplateRepository;
import com.afale.repository.search.ContainsBathRoomsSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.ContainsBathRooms}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ContainsBathRoomsResource {

    private final Logger log = LoggerFactory.getLogger(ContainsBathRoomsResource.class);

    private static final String ENTITY_NAME = "containsBathRooms";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContainsBathRoomsRepository containsBathRoomsRepository;
    private final HousingTemplateRepository housingTemplateRepository;

    private final ContainsBathRoomsSearchRepository containsBathRoomsSearchRepository;

    public ContainsBathRoomsResource(ContainsBathRoomsRepository containsBathRoomsRepository, HousingTemplateRepository housingTemplateRepository, ContainsBathRoomsSearchRepository containsBathRoomsSearchRepository) {
        this.containsBathRoomsRepository = containsBathRoomsRepository;
        this.housingTemplateRepository = housingTemplateRepository;
        this.containsBathRoomsSearchRepository = containsBathRoomsSearchRepository;
    }

    /**
     * {@code POST  /contains-bath-rooms} : Create a new containsBathRooms.
     *
     * @param containsBathRooms the containsBathRooms to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new containsBathRooms, or with status {@code 400 (Bad Request)} if the containsBathRooms has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contains-bath-rooms")
    public ResponseEntity<ContainsBathRooms> createContainsBathRooms(@Valid @RequestBody ContainsBathRooms containsBathRooms) throws URISyntaxException {
        log.debug("REST request to save ContainsBathRooms : {}", containsBathRooms);
        if (containsBathRooms.getId() != null) {
            throw new BadRequestAlertException("A new containsBathRooms cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContainsBathRooms result = containsBathRoomsRepository.save(containsBathRooms);
        containsBathRoomsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/contains-bath-rooms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contains-bath-rooms} : Updates an existing containsBathRooms.
     *
     * @param containsBathRooms the containsBathRooms to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated containsBathRooms,
     * or with status {@code 400 (Bad Request)} if the containsBathRooms is not valid,
     * or with status {@code 500 (Internal Server Error)} if the containsBathRooms couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contains-bath-rooms")
    public ResponseEntity<ContainsBathRooms> updateContainsBathRooms(@Valid @RequestBody ContainsBathRooms containsBathRooms) throws URISyntaxException {
        log.debug("REST request to update ContainsBathRooms : {}", containsBathRooms);
        if (containsBathRooms.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContainsBathRooms result = containsBathRoomsRepository.save(containsBathRooms);
        containsBathRoomsSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, containsBathRooms.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contains-bath-rooms} : get all the containsBathRooms.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of containsBathRooms in body.
     */
    @GetMapping("/contains-bath-rooms")
    public List<ContainsBathRooms> getAllContainsBathRooms() {
        log.debug("REST request to get all ContainsBathRooms");
        return containsBathRoomsRepository.findAll();
    }

    /**
     * {@code GET  /contains-bath-rooms/:id} : get the "id" containsBathRooms.
     *
     * @param id the id of the containsBathRooms to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the containsBathRooms, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contains-bath-rooms/{id}")
    public ResponseEntity<ContainsBathRooms> getContainsBathRooms(@PathVariable Long id) {
        log.debug("REST request to get ContainsBathRooms : {}", id);
        Optional<ContainsBathRooms> containsBathRooms = containsBathRoomsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(containsBathRooms);
    }

    /**
     * {@code GET  /contains-bath-rooms/housing-template_id/:id} : get the contains bath room object to a specific housing template.
     *
     * @param id the id of the housing template
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contains bath room, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contains-bath-rooms/housing-template_id/{id}")
    public List<ContainsBathRooms> getContainsBathRoomsFromHousingTemplateId(@PathVariable Long id) {
        log.debug("REST request to get the contains bed room object to a specific housing template : {}", id);
        Optional<HousingTemplate>  ht= housingTemplateRepository.findById(id);
        if(ht.isPresent()){
            return containsBathRoomsRepository.getContainsBathRoomsFromHousingTemplateId(ht.get());
        }
        else
            return new ArrayList<ContainsBathRooms>();
    }

    /**
     * {@code DELETE  /contains-bath-rooms/:id} : delete the "id" containsBathRooms.
     *
     * @param id the id of the containsBathRooms to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contains-bath-rooms/{id}")
    public ResponseEntity<Void> deleteContainsBathRooms(@PathVariable Long id) {
        log.debug("REST request to delete ContainsBathRooms : {}", id);
        containsBathRoomsRepository.deleteById(id);
        containsBathRoomsSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/contains-bath-rooms?query=:query} : search for the containsBathRooms corresponding
     * to the query.
     *
     * @param query the query of the containsBathRooms search.
     * @return the result of the search.
     */
    @GetMapping("/_search/contains-bath-rooms")
    public List<ContainsBathRooms> searchContainsBathRooms(@RequestParam String query) {
        log.debug("REST request to search ContainsBathRooms for query {}", query);
        return StreamSupport
            .stream(containsBathRoomsSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
