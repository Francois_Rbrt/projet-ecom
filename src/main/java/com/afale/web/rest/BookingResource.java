package com.afale.web.rest;

import com.afale.BookingLock;
import com.afale.domain.Booking;
import com.afale.domain.BookingElement;
import com.afale.domain.HousingTemplate;
import com.afale.repository.BookingElementRepository;
import com.afale.repository.BookingRepository;
import com.afale.repository.HousingTemplateRepository;
import com.afale.repository.search.BookingElementSearchRepository;
import com.afale.repository.search.BookingSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.Booking}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BookingResource {

    private final Logger log = LoggerFactory.getLogger(BookingResource.class);

    private static final String ENTITY_NAME = "booking";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookingRepository bookingRepository;
    private final BookingElementRepository bookingElementRepository;
    private final HousingTemplateRepository housingTemplateRepository;
    @Autowired
    BookingLock bookingLock;

    private final BookingSearchRepository bookingSearchRepository;
    private final BookingElementSearchRepository bookingElementSearchRepository;

    public BookingResource(BookingRepository bookingRepository, BookingElementRepository bookingElementRepository, HousingTemplateRepository housingTemplateRepository, BookingSearchRepository bookingSearchRepository, BookingElementSearchRepository bookingElementSearchRepository) {
        this.bookingRepository = bookingRepository;
        this.bookingElementRepository = bookingElementRepository;
        this.housingTemplateRepository = housingTemplateRepository;
        this.bookingSearchRepository = bookingSearchRepository;
        this.bookingElementSearchRepository = bookingElementSearchRepository;
    }

    /**
     * {@code POST  /bookings} : Create a new booking.
     *
     * @param booking the booking to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new booking, or with status {@code 400 (Bad Request)} if the booking has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bookings")
    public ResponseEntity<Booking> createBooking(@Valid @RequestBody Booking booking) throws URISyntaxException {
        log.debug("REST request to save Booking : {}", booking);
        if (booking.getId() != null) {

            throw new BadRequestAlertException("A new booking cannot already have an ID", ENTITY_NAME, "idexists");
        }

        Booking result = bookingRepository.save(booking);
        bookingLock.addNewBookingToCheck(result.getId(), LocalDateTime.now());
        bookingSearchRepository.save(result);

        return ResponseEntity.created(new URI("/api/bookings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /bookings} : Verify a booking.
     *
     * @param booking the booking to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new booking, or with status {@code 400 (Bad Request)} if the booking has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bookings/verify")
    public ResponseEntity<Booking> verifyBooking(@Valid @RequestBody Booking booking) throws URISyntaxException {
        log.debug("REST request to verify Booking : {}", booking);
        Optional<Booking> b = bookingRepository.findById(booking.getId());
        if (b.isPresent()) {
            ArrayList<BookingElement> list_be = (ArrayList<BookingElement>) bookingElementRepository.getBookingElementFromBookingId(b.get());
            int i = 0;
            //on se permet 10 essais
            while (list_be.isEmpty() && i < 10) {
                log.debug("THERE IS NO BOOKING ELEMENT FOR THIS HT : RETRY...");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                list_be = (ArrayList<BookingElement>) bookingElementRepository.getBookingElementFromBookingId(b.get());
                i++;
            }
            if (list_be.isEmpty()) {
                return ResponseEntity.created(new URI("/api/bookings/verify"))
                    .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, "suppression"))
                    .body(null);
            }
                for (BookingElement be : list_be) {
                    log.debug("ONE BOOKING ELEMENT CHECKED");

                    //récupérer une liste de booking el qui sont sur le même housing template
                    //TODO : elastic search
                    String query = "\"{ \\\"match\\\": { \\\"id\\\": { \\\"query\\\": \\\""+be.getId()+"\\\" } } } \"";
                    ArrayList<BookingElement> list_search = (ArrayList<BookingElement>)StreamSupport
                        .stream(bookingElementSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                        .collect(Collectors.toList());
                    for (BookingElement be_c : list_search) {
                            log.debug("ELASTIC");
                    }
                    ArrayList<BookingElement> list_be_concurrent = (ArrayList<BookingElement>) bookingElementRepository.getBookingElementFromHT(be.getHousingTemplateId());
                    int nbRes = 0;
                    for (BookingElement be_c : list_be_concurrent) {
                        if (!isNotSameTime(be_c.getStartDate(), be.getStartDate(), be_c.getEndDate(), be.getEndDate())) {
                            //log.debug("ONE RES IN CONFLICT WITH ACTUAL BOOKING ELEMENT");
                            nbRes++;
                        }
                    }
                    if (nbRes > be.getHousingTemplateId().getNbOfUnit()) {
                        for (BookingElement b_e : bookingElementRepository.getBookingElementFromBookingId(b.get())) {
                            //log.debug("REST request to delete BookingElement : {}", b_e.getId());
                            bookingElementRepository.deleteById(b_e.getId());
                        }
                        bookingLock.noNeedtoCheck(b.get().getId());
                        bookingRepository.deleteById(b.get().getId());
                        //bookingSearchRepository.deleteById(id);

                        //TODO : renvoyer une erreur
                        return ResponseEntity.created(new URI("/api/bookings/verify"))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, "suppression"))
                            .body(null);
                    }

                }
            }

            return ResponseEntity.created(new URI("/api/bookings/verify" + booking.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, booking.getId().toString()))
                .body(booking);

    }

    /**
     * {@code GET  bookings/dispo/:id&:start&:end} : get the minimum price "id" establishment.
     *
     * @param id the id of the establishment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the establishment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bookings/dispo/{id}&{start}&{end}")
    public ResponseEntity<Number> getBookingsDispo(@PathVariable Long id,@PathVariable String start,@PathVariable String end) {
        log.debug("REST request to get Booking dispo : {}", id);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        //convert String to LocalDate
        LocalDate s=LocalDate.parse(start,formatter);
        //convert String to LocalDate
        LocalDate e=LocalDate.parse(end,formatter);
        log.debug("START DATE : "+start);
        log.debug("END DATE : "+end);
        log.debug("START DATE : "+s);
        log.debug("END DATE : "+e);
        Optional<HousingTemplate> ht = housingTemplateRepository.findById(id);
        if(ht.isPresent()){
            int nbRes = 0;
                for (BookingElement be_c : bookingElementRepository.getBookingElementFromHT(ht.get())) {
                    if(isNotSameTime(be_c.getStartDate(), s, be_c.getEndDate(), e)) {
                        nbRes++;
                    }
                }
            return ResponseUtil.wrapOrNotFound(Optional.of(nbRes));
        }
        return ResponseUtil.wrapOrNotFound(null);
    }

    /* Renvoie vrai si les dates ne sont pas en conflit */
    private boolean isNotSameTime(LocalDate s1, LocalDate s2, LocalDate e1, LocalDate e2) {
        if (s1.isEqual(s2) ) return false;
        if (s1.isBefore(s2) ) return s2.isAfter(e1) || s2.isEqual(e1);
        return s1.isAfter(e2) || s1.isEqual(e2);
    }

    /**
     * {@code PUT  /bookings} : Updates an existing booking.
     *
     * @param booking the booking to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated booking,
     * or with status {@code 400 (Bad Request)} if the booking is not valid,
     * or with status {@code 500 (Internal Server Error)} if the booking couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bookings")
    public ResponseEntity<Booking> updateBooking(@Valid @RequestBody Booking booking) throws URISyntaxException {
        log.debug("REST request to update Booking : {}", booking);
        if (booking.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if(booking.isValidate()){
            bookingLock.noNeedtoCheck(booking.getId());
        }
        Booking result = bookingRepository.save(booking);
        bookingSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, booking.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bookings} : get all the bookings.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookings in body.
     */
    @GetMapping("/bookings")
    public List<Booking> getAllBookings() {
        log.debug("REST request to get all Bookings");
        return bookingRepository.findAll();
    }

    /**
     * {@code GET  /bookings/:id} : get the "id" booking.
     *
     * @param id the id of the booking to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the booking, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bookings/{id}")
    public ResponseEntity<Booking> getBooking(@PathVariable Long id) {
        log.debug("REST request to get Booking : {}", id);
        Optional<Booking> booking = bookingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(booking);
    }

    /**
     * {@code DELETE  /bookings/:id} : delete the "id" booking.
     *
     * @param id the id of the booking to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bookings/{id}")
    public ResponseEntity<Void> deleteBooking(@PathVariable Long id) {
        log.debug("REST request to delete Booking : {}", id);
        //get all the booking elements that were linked to this booking
        Optional<Booking>  b=bookingRepository.findById(id);
        if(b.isPresent()) {
            ArrayList<BookingElement> list_be= (ArrayList<BookingElement>) bookingElementRepository.getBookingElementFromBookingId(b.get());
            for(BookingElement be: list_be) {
                log.debug("REST request to delete BookingElement : {}", be.getId());
                bookingElementRepository.deleteById(be.getId());
            }
        }
        bookingRepository.deleteById(id);
        bookingSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/bookings?query=:query} : search for the booking corresponding
     * to the query.
     *
     * @param query the query of the booking search.
     * @return the result of the search.
     */
    @GetMapping("/_search/bookings")
    public List<Booking> searchBookings(@RequestParam String query) {
        log.debug("REST request to search Bookings for query {}", query);
        return StreamSupport
            .stream(bookingSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
