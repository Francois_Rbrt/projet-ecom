package com.afale.web.rest;

import com.afale.domain.Establishment;
import com.afale.domain.HousingTemplate;
import com.afale.domain.Photo;
import com.afale.repository.EstablishmentRepository;
import com.afale.repository.HousingTemplateRepository;
import com.afale.repository.PhotoRepository;
import com.afale.repository.search.PhotoSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.Photo}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PhotoResource {

    private final Logger log = LoggerFactory.getLogger(PhotoResource.class);

    private static final String ENTITY_NAME = "photo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhotoRepository photoRepository;
    private final HousingTemplateRepository housingTemplateRepository;
    private final EstablishmentRepository establishmentRepository;

    private final PhotoSearchRepository photoSearchRepository;

    public PhotoResource(PhotoRepository photoRepository, HousingTemplateRepository housingTemplateRepository, EstablishmentRepository establishmentRepository, PhotoSearchRepository photoSearchRepository) {
        this.photoRepository = photoRepository;
        this.housingTemplateRepository = housingTemplateRepository;
        this.establishmentRepository = establishmentRepository;
        this.photoSearchRepository = photoSearchRepository;
    }

    /**
     * {@code POST  /photos} : Create a new photo.
     *
     * @param photo the photo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new photo, or with status {@code 400 (Bad Request)} if the photo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/photos")
    public ResponseEntity<Photo> createPhoto(@Valid @RequestBody Photo photo) throws URISyntaxException {
        log.debug("REST request to save Photo : {}", photo);
        if (photo.getId() != null) {
            throw new BadRequestAlertException("A new photo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Photo result = photoRepository.save(photo);
        photoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/photos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /photos} : Updates an existing photo.
     *
     * @param photo the photo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated photo,
     * or with status {@code 400 (Bad Request)} if the photo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the photo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/photos")
    public ResponseEntity<Photo> updatePhoto(@Valid @RequestBody Photo photo) throws URISyntaxException {
        log.debug("REST request to update Photo : {}", photo);
        if (photo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Photo result = photoRepository.save(photo);
        photoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, photo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /photos} : get all the photos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of photos in body.
     */
    @GetMapping("/photos")
    public List<Photo> getAllPhotos() {
        log.debug("REST request to get all Photos");
        return photoRepository.findAll();
    }

    /**
     * {@code GET  /photos/:id} : get the "id" photo.
     *
     * @param id the id of the photo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the photo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/photos/{id}")
    public ResponseEntity<Photo> getPhoto(@PathVariable Long id) {
        log.debug("REST request to get Photo : {}", id);
        Optional<Photo> photo = photoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(photo);
    }

    /**
     * {@code GET  /photos/establishment_id/:id} : get the photos corresponding to a specific establishment.
     *
     * @param id the id of the establishment
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the housingTemplates, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/photos/establishment_id/{id}")
    public List<Photo> getPhotosFromEstablishmentId(@PathVariable Long id) {
        log.debug("REST request to get Photos for establishment : {}", id);
        Optional<Establishment>  e= establishmentRepository.findById(id);
        if(e.isPresent()){
            return photoRepository.getPhotoFromEstablishmentId(e.get());
        }
        else
            return new ArrayList<Photo>();
    }

    /**
     * {@code GET  /photos/housing-templates_id/:id} : get the photos corresponding to a specific establishment.
     *
     * @param id the id of the housing template
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the housingTemplates, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/photos/housing-templates_id/{id}")
    public List<Photo> getPhotosFromHousingTemplateId(@PathVariable Long id) {
        log.debug("REST request to get Photos for Housing template : {}", id);
        Optional<HousingTemplate>  ht= housingTemplateRepository.findById(id);
        if(ht.isPresent()){
            return photoRepository.getPhotoFromHousingTemplateId(ht.get());
        }
        else
            return new ArrayList<Photo>();
    }

    /**
     * {@code DELETE  /photos/:id} : delete the "id" photo.
     *
     * @param id the id of the photo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/photos/{id}")
    public ResponseEntity<Void> deletePhoto(@PathVariable Long id) {
        log.debug("REST request to delete Photo : {}", id);
        photoRepository.deleteById(id);
        photoSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/photos?query=:query} : search for the photo corresponding
     * to the query.
     *
     * @param query the query of the photo search.
     * @return the result of the search.
     */
    @GetMapping("/_search/photos")
    public List<Photo> searchPhotos(@RequestParam String query) {
        log.debug("REST request to search Photos for query {}", query);
        return StreamSupport
            .stream(photoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
