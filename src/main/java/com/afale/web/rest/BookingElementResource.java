package com.afale.web.rest;

import com.afale.domain.Booking;
import com.afale.domain.BookingElement;
import com.afale.repository.BookingElementRepository;
import com.afale.repository.search.BookingElementSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.BookingElement}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BookingElementResource {

    private final Logger log = LoggerFactory.getLogger(BookingElementResource.class);

    private static final String ENTITY_NAME = "bookingElement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookingElementRepository bookingElementRepository;

    private final BookingElementSearchRepository bookingElementSearchRepository;

    public BookingElementResource(BookingElementRepository bookingElementRepository, BookingElementSearchRepository bookingElementSearchRepository) {
        this.bookingElementRepository = bookingElementRepository;
        this.bookingElementSearchRepository = bookingElementSearchRepository;
    }

    /**
     * {@code POST  /booking-elements} : Create a new bookingElement.
     *
     * @param bookingElement the bookingElement to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bookingElement, or with status {@code 400 (Bad Request)} if the bookingElement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/booking-elements")
    public ResponseEntity<BookingElement> createBookingElement(@Valid @RequestBody BookingElement bookingElement) throws URISyntaxException {
        log.debug("REST request to save BookingElement : {}", bookingElement);
        if (bookingElement.getId() != null) {
            throw new BadRequestAlertException("A new bookingElement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookingElement result = bookingElementRepository.save(bookingElement);
        bookingElementSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/booking-elements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /booking-elements} : Updates an existing bookingElement.
     *
     * @param bookingElement the bookingElement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bookingElement,
     * or with status {@code 400 (Bad Request)} if the bookingElement is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bookingElement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/booking-elements")
    public ResponseEntity<BookingElement> updateBookingElement(@Valid @RequestBody BookingElement bookingElement) throws URISyntaxException {
        log.debug("REST request to update BookingElement : {}", bookingElement);
        if (bookingElement.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BookingElement result = bookingElementRepository.save(bookingElement);
        bookingElementSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bookingElement.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /booking-elements} : get all the bookingElements.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookingElements in body.
     */
    @GetMapping("/booking-elements")
    public List<BookingElement> getAllBookingElements() {
        log.debug("REST request to get all BookingElements");
        return bookingElementRepository.findAll();
    }


    /**
     * {@code GET  /booking-elements-from-bookingId/:bookingId} : get all the bookingElements from id.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookingElements in body.
     */
    @GetMapping("/booking-elements-from-bookingId/{bookingId}")
    public List<BookingElement> getAllBookingElementsOfABooking(@PathVariable Long bookingId) {
        log.debug("REST request to get all BookingElements af a booking");
        List<BookingElement> ret= new ArrayList<BookingElement>();
        List<BookingElement> tmp=bookingElementRepository.findAll();
        for(BookingElement e : tmp) {//parcour tous les booking element pour trouver ceux qui sont relier au bookingId
        							//surement pas tres opti mais bon sa marche
        	if(e.getBookingId()!=null && e.getBookingId().getId().equals(bookingId)) {
        		ret.add(e);
        	}
        }
        return ret;
    }


    /**
     * {@code GET  /booking-elements/:id} : get the "id" bookingElement.
     *
     * @param id the id of the bookingElement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bookingElement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/booking-elements/{id}")
    public ResponseEntity<BookingElement> getBookingElement(@PathVariable Long id) {
        log.debug("REST request to get BookingElement : {}", id);
        Optional<BookingElement> bookingElement = bookingElementRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bookingElement);
    }

    /**
     * {@code DELETE  /booking-elements/:id} : delete the "id" bookingElement.
     *
     * @param id the id of the bookingElement to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/booking-elements/{id}")
    public ResponseEntity<Void> deleteBookingElement(@PathVariable Long id) {
        log.debug("REST request to delete BookingElement : {}", id);
        bookingElementRepository.deleteById(id);
        bookingElementSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/booking-elements?query=:query} : search for the bookingElement corresponding
     * to the query.
     *
     * @param query the query of the bookingElement search.
     * @return the result of the search.
     */
    @GetMapping("/_search/booking-elements")
    public List<BookingElement> searchBookingElements(@RequestParam String query) {
        log.debug("REST request to search BookingElements for query {}", query);
        return StreamSupport
            .stream(bookingElementSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
