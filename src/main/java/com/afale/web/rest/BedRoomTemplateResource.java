package com.afale.web.rest;

import com.afale.domain.BedRoomTemplate;
import com.afale.repository.BedRoomTemplateRepository;
import com.afale.repository.search.BedRoomTemplateSearchRepository;
import com.afale.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.afale.domain.BedRoomTemplate}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BedRoomTemplateResource {

    private final Logger log = LoggerFactory.getLogger(BedRoomTemplateResource.class);

    private static final String ENTITY_NAME = "bedRoomTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BedRoomTemplateRepository bedRoomTemplateRepository;

    private final BedRoomTemplateSearchRepository bedRoomTemplateSearchRepository;

    public BedRoomTemplateResource(BedRoomTemplateRepository bedRoomTemplateRepository, BedRoomTemplateSearchRepository bedRoomTemplateSearchRepository) {
        this.bedRoomTemplateRepository = bedRoomTemplateRepository;
        this.bedRoomTemplateSearchRepository = bedRoomTemplateSearchRepository;
    }

    /**
     * {@code POST  /bed-room-templates} : Create a new bedRoomTemplate.
     *
     * @param bedRoomTemplate the bedRoomTemplate to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bedRoomTemplate, or with status {@code 400 (Bad Request)} if the bedRoomTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bed-room-templates")
    public ResponseEntity<BedRoomTemplate> createBedRoomTemplate(@Valid @RequestBody BedRoomTemplate bedRoomTemplate) throws URISyntaxException {
        log.debug("REST request to save BedRoomTemplate : {}", bedRoomTemplate);
        if (bedRoomTemplate.getId() != null) {
            throw new BadRequestAlertException("A new bedRoomTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BedRoomTemplate result = bedRoomTemplateRepository.save(bedRoomTemplate);
        bedRoomTemplateSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/bed-room-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bed-room-templates} : Updates an existing bedRoomTemplate.
     *
     * @param bedRoomTemplate the bedRoomTemplate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bedRoomTemplate,
     * or with status {@code 400 (Bad Request)} if the bedRoomTemplate is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bedRoomTemplate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bed-room-templates")
    public ResponseEntity<BedRoomTemplate> updateBedRoomTemplate(@Valid @RequestBody BedRoomTemplate bedRoomTemplate) throws URISyntaxException {
        log.debug("REST request to update BedRoomTemplate : {}", bedRoomTemplate);
        if (bedRoomTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BedRoomTemplate result = bedRoomTemplateRepository.save(bedRoomTemplate);
        bedRoomTemplateSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bedRoomTemplate.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bed-room-templates} : get all the bedRoomTemplates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bedRoomTemplates in body.
     */
    @GetMapping("/bed-room-templates")
    public List<BedRoomTemplate> getAllBedRoomTemplates() {
        log.debug("REST request to get all BedRoomTemplates");
        return bedRoomTemplateRepository.findAll();
    }

    /**
     * {@code GET  /bed-room-templates/:id} : get the "id" bedRoomTemplate.
     *
     * @param id the id of the bedRoomTemplate to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bedRoomTemplate, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bed-room-templates/{id}")
    public ResponseEntity<BedRoomTemplate> getBedRoomTemplate(@PathVariable Long id) {
        log.debug("REST request to get BedRoomTemplate : {}", id);
        Optional<BedRoomTemplate> bedRoomTemplate = bedRoomTemplateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bedRoomTemplate);
    }

    /**
     * {@code DELETE  /bed-room-templates/:id} : delete the "id" bedRoomTemplate.
     *
     * @param id the id of the bedRoomTemplate to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bed-room-templates/{id}")
    public ResponseEntity<Void> deleteBedRoomTemplate(@PathVariable Long id) {
        log.debug("REST request to delete BedRoomTemplate : {}", id);
        bedRoomTemplateRepository.deleteById(id);
        bedRoomTemplateSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/bed-room-templates?query=:query} : search for the bedRoomTemplate corresponding
     * to the query.
     *
     * @param query the query of the bedRoomTemplate search.
     * @return the result of the search.
     */
    @GetMapping("/_search/bed-room-templates")
    public List<BedRoomTemplate> searchBedRoomTemplates(@RequestParam String query) {
        log.debug("REST request to search BedRoomTemplates for query {}", query);
        return StreamSupport
            .stream(bedRoomTemplateSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
