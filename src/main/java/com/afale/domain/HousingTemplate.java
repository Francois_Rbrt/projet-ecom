package com.afale.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

import com.afale.domain.enumeration.HousingType;

/**
 * A HousingTemplate.
 */
@Entity
@Table(name = "housing_template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "housingtemplate")
public class HousingTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The type of housing
     */
    @NotNull
    @ApiModelProperty(value = "The type of housing", required = true)
    @Enumerated(EnumType.STRING)
    @Column(name = "housing_type", nullable = false)
    private HousingType housingType;

    /**
     * Number of unit of this kind of housing the establishment has
     */
    @NotNull
    @Min(value = 1)
    @ApiModelProperty(value = "Number of unit of this kind of housing the establishment has", required = true)
    @Column(name = "nb_of_unit", nullable = false)
    private Integer nbOfUnit;

    /**
     * The maximum number of persons that can live in this kind of housing : computed with the number of beds in the Housing
     */
    @NotNull
    @Min(value = 1)
    @ApiModelProperty(value = "The maximum number of persons that can live in this kind of housing : computed with the number of beds in the Housing", required = true)
    @Column(name = "nb_max_of_occupants", nullable = false)
    private Integer nbMaxOfOccupants;

    /**
     * Price per night
     */
    @NotNull
    @ApiModelProperty(value = "Price per night", required = true)
    @Column(name = "price_per_night", nullable = false)
    private Float pricePerNight;

    /**
     * Are smokers allowed to smoke inside of the housing ?
     */
    @NotNull
    @ApiModelProperty(value = "Are smokers allowed to smoke inside of the housing ?", required = true)
    @Column(name = "is_non_smoking", nullable = false)
    private Boolean isNonSmoking;

    /**
     * Does the housing have the necessary to cook ?
     */
    @NotNull
    @ApiModelProperty(value = "Does the housing have the necessary to cook ?", required = true)
    @Column(name = "has_kitchen", nullable = false)
    private Boolean hasKitchen;

    /**
     * Does the housing have toilets that are not included in a bathroom ?
     */
    @NotNull
    @ApiModelProperty(value = "Does the housing have toilets that are not included in a bathroom ?", required = true)
    @Column(name = "has_toilets", nullable = false)
    private Boolean hasToilets;

    /**
     * The establishment that owns this housing template
     */
    @ApiModelProperty(value = "The establishment that owns this housing template")
    @ManyToOne
    @JsonIgnoreProperties(value = "housingTemplates", allowSetters = true)
    private Establishment establishmentId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HousingType getHousingType() {
        return housingType;
    }

    public HousingTemplate housingType(HousingType housingType) {
        this.housingType = housingType;
        return this;
    }

    public void setHousingType(HousingType housingType) {
        this.housingType = housingType;
    }

    public Integer getNbOfUnit() {
        return nbOfUnit;
    }

    public HousingTemplate nbOfUnit(Integer nbOfUnit) {
        this.nbOfUnit = nbOfUnit;
        return this;
    }

    public void setNbOfUnit(Integer nbOfUnit) {
        this.nbOfUnit = nbOfUnit;
    }

    public Integer getNbMaxOfOccupants() {
        return nbMaxOfOccupants;
    }

    public HousingTemplate nbMaxOfOccupants(Integer nbMaxOfOccupants) {
        this.nbMaxOfOccupants = nbMaxOfOccupants;
        return this;
    }

    public void setNbMaxOfOccupants(Integer nbMaxOfOccupants) {
        this.nbMaxOfOccupants = nbMaxOfOccupants;
    }

    public Float getPricePerNight() {
        return pricePerNight;
    }

    public HousingTemplate pricePerNight(Float pricePerNight) {
        this.pricePerNight = pricePerNight;
        return this;
    }

    public void setPricePerNight(Float pricePerNight) {
        this.pricePerNight = pricePerNight;
    }

    public Boolean isIsNonSmoking() {
        return isNonSmoking;
    }

    public HousingTemplate isNonSmoking(Boolean isNonSmoking) {
        this.isNonSmoking = isNonSmoking;
        return this;
    }

    public void setIsNonSmoking(Boolean isNonSmoking) {
        this.isNonSmoking = isNonSmoking;
    }

    public Boolean isHasKitchen() {
        return hasKitchen;
    }

    public HousingTemplate hasKitchen(Boolean hasKitchen) {
        this.hasKitchen = hasKitchen;
        return this;
    }

    public void setHasKitchen(Boolean hasKitchen) {
        this.hasKitchen = hasKitchen;
    }

    public Boolean isHasToilets() {
        return hasToilets;
    }

    public HousingTemplate hasToilets(Boolean hasToilets) {
        this.hasToilets = hasToilets;
        return this;
    }

    public void setHasToilets(Boolean hasToilets) {
        this.hasToilets = hasToilets;
    }

    public Establishment getEstablishmentId() {
        return establishmentId;
    }

    public HousingTemplate establishmentId(Establishment establishment) {
        this.establishmentId = establishment;
        return this;
    }

    public void setEstablishmentId(Establishment establishment) {
        this.establishmentId = establishment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HousingTemplate)) {
            return false;
        }
        return id != null && id.equals(((HousingTemplate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HousingTemplate{" +
            "id=" + getId() +
            ", housingType='" + getHousingType() + "'" +
            ", nbOfUnit=" + getNbOfUnit() +
            ", nbMaxOfOccupants=" + getNbMaxOfOccupants() +
            ", pricePerNight=" + getPricePerNight() +
            ", isNonSmoking='" + isIsNonSmoking() + "'" +
            ", hasKitchen='" + isHasKitchen() + "'" +
            ", hasToilets='" + isHasToilets() + "'" +
            "}";
    }
}
