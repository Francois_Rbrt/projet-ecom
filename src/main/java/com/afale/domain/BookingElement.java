package com.afale.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * The Booking entity
 */
@ApiModel(description = "The Booking entity")
@Entity
@Table(name = "booking_element")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "bookingelement")
public class BookingElement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    /**
     * The total price of the booking, computed with the price per night of the HousingCategory and the duration of the booking
     */
    @NotNull
    @ApiModelProperty(value = "The total price of the booking, computed with the price per night of the HousingCategory and the duration of the booking", required = true)
    @Column(name = "element_price", nullable = false)
    private Integer elementPrice;

    /**
     * The Booking this booking element is a part of
     */
    @ApiModelProperty(value = "The Booking this booking element is a part of")
    @ManyToOne
    @JsonIgnoreProperties(value = "bookingElements", allowSetters = true)
    private Booking bookingId;

    /**
     * The housing template this booking element is on
     */
    @ApiModelProperty(value = "The housing template this booking element is on")
    @ManyToOne
    @JsonIgnoreProperties(value = "bookingElements", allowSetters = true)
    private HousingTemplate housingTemplateId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public BookingElement startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public BookingElement endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getElementPrice() {
        return elementPrice;
    }

    public BookingElement elementPrice(Integer elementPrice) {
        this.elementPrice = elementPrice;
        return this;
    }

    public void setElementPrice(Integer elementPrice) {
        this.elementPrice = elementPrice;
    }

    public Booking getBookingId() {
        return bookingId;
    }

    public BookingElement bookingId(Booking booking) {
        this.bookingId = booking;
        return this;
    }

    public void setBookingId(Booking booking) {
        this.bookingId = booking;
    }

    public HousingTemplate getHousingTemplateId() {
        return housingTemplateId;
    }

    public BookingElement housingTemplateId(HousingTemplate housingTemplate) {
        this.housingTemplateId = housingTemplate;
        return this;
    }

    public void setHousingTemplateId(HousingTemplate housingTemplate) {
        this.housingTemplateId = housingTemplate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BookingElement)) {
            return false;
        }
        return id != null && id.equals(((BookingElement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BookingElement{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", elementPrice=" + getElementPrice() +
            "}";
    }
}
