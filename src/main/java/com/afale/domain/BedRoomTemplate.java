package com.afale.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A BedRoomTemplate.
 */
@Entity
@Table(name = "bed_room_template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "bedroomtemplate")
public class BedRoomTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The number of single beds
     */
    @NotNull
    @ApiModelProperty(value = "The number of single beds", required = true)
    @Column(name = "nb_single_beds", nullable = false)
    private Integer nbSingleBeds;

    /**
     * The number of double beds
     */
    @NotNull
    @ApiModelProperty(value = "The number of double beds", required = true)
    @Column(name = "nb_double_beds", nullable = false)
    private Integer nbDoubleBeds;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNbSingleBeds() {
        return nbSingleBeds;
    }

    public BedRoomTemplate nbSingleBeds(Integer nbSingleBeds) {
        this.nbSingleBeds = nbSingleBeds;
        return this;
    }

    public void setNbSingleBeds(Integer nbSingleBeds) {
        this.nbSingleBeds = nbSingleBeds;
    }

    public Integer getNbDoubleBeds() {
        return nbDoubleBeds;
    }

    public BedRoomTemplate nbDoubleBeds(Integer nbDoubleBeds) {
        this.nbDoubleBeds = nbDoubleBeds;
        return this;
    }

    public void setNbDoubleBeds(Integer nbDoubleBeds) {
        this.nbDoubleBeds = nbDoubleBeds;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BedRoomTemplate)) {
            return false;
        }
        return id != null && id.equals(((BedRoomTemplate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BedRoomTemplate{" +
            "id=" + getId() +
            ", nbSingleBeds=" + getNbSingleBeds() +
            ", nbDoubleBeds=" + getNbDoubleBeds() +
            "}";
    }
}
