package com.afale.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * This entity is an association table between HousingTemplate and BedRoomTemplate
 */
@ApiModel(description = "This entity is an association table between HousingTemplate and BedRoomTemplate")
@Entity
@Table(name = "contains_bed_rooms")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "containsbedrooms")
public class ContainsBedRooms implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The number of unit of this bedroom template the HousingTemplate has
     */
    @NotNull
    @Min(value = 0)
    @ApiModelProperty(value = "The number of unit of this bedroom template the HousingTemplate has", required = true)
    @Column(name = "nb_of_unit", nullable = false)
    private Integer nbOfUnit;

    /**
     * the housing template where the bedroom is in
     */
    @ApiModelProperty(value = "the housing template where the bedroom is in")
    @ManyToOne
    @JsonIgnoreProperties(value = "containsBedRooms", allowSetters = true)
    private HousingTemplate housingTemplateId;

    /**
     * the bedroom template that the housing template has in
     */
    @ApiModelProperty(value = "the bedroom template that the housing template has in")
    @ManyToOne
    @JsonIgnoreProperties(value = "containsBedRooms", allowSetters = true)
    private BedRoomTemplate bedroomTemplateId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNbOfUnit() {
        return nbOfUnit;
    }

    public ContainsBedRooms nbOfUnit(Integer nbOfUnit) {
        this.nbOfUnit = nbOfUnit;
        return this;
    }

    public void setNbOfUnit(Integer nbOfUnit) {
        this.nbOfUnit = nbOfUnit;
    }

    public HousingTemplate getHousingTemplateId() {
        return housingTemplateId;
    }

    public ContainsBedRooms housingTemplateId(HousingTemplate housingTemplate) {
        this.housingTemplateId = housingTemplate;
        return this;
    }

    public void setHousingTemplateId(HousingTemplate housingTemplate) {
        this.housingTemplateId = housingTemplate;
    }

    public BedRoomTemplate getBedroomTemplateId() {
        return bedroomTemplateId;
    }

    public ContainsBedRooms bedroomTemplateId(BedRoomTemplate bedRoomTemplate) {
        this.bedroomTemplateId = bedRoomTemplate;
        return this;
    }

    public void setBedroomTemplateId(BedRoomTemplate bedRoomTemplate) {
        this.bedroomTemplateId = bedRoomTemplate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContainsBedRooms)) {
            return false;
        }
        return id != null && id.equals(((ContainsBedRooms) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContainsBedRooms{" +
            "id=" + getId() +
            ", nbOfUnit=" + getNbOfUnit() +
            "}";
    }
}
