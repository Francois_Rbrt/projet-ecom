package com.afale.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * This entity is an association table between HousingTemplate and BathRoomTemplate
 */
@ApiModel(description = "This entity is an association table between HousingTemplate and BathRoomTemplate")
@Entity
@Table(name = "contains_bath_rooms")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "containsbathrooms")
public class ContainsBathRooms implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The number of unit of this bathroom template the HousingTemplate has
     */
    @NotNull
    @Min(value = 0)
    @ApiModelProperty(value = "The number of unit of this bathroom template the HousingTemplate has", required = true)
    @Column(name = "nb_of_unit", nullable = false)
    private Integer nbOfUnit;

    /**
     * the housing template where the bathroom is in
     */
    @ApiModelProperty(value = "the housing template where the bathroom is in")
    @ManyToOne
    @JsonIgnoreProperties(value = "containsBathRooms", allowSetters = true)
    private HousingTemplate housingTemplateId;

    /**
     * the bathroom template that the housing template has in
     */
    @ApiModelProperty(value = "the bathroom template that the housing template has in")
    @ManyToOne
    @JsonIgnoreProperties(value = "containsBathRooms", allowSetters = true)
    private BathRoomTemplate bathroomTemplateId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNbOfUnit() {
        return nbOfUnit;
    }

    public ContainsBathRooms nbOfUnit(Integer nbOfUnit) {
        this.nbOfUnit = nbOfUnit;
        return this;
    }

    public void setNbOfUnit(Integer nbOfUnit) {
        this.nbOfUnit = nbOfUnit;
    }

    public HousingTemplate getHousingTemplateId() {
        return housingTemplateId;
    }

    public ContainsBathRooms housingTemplateId(HousingTemplate housingTemplate) {
        this.housingTemplateId = housingTemplate;
        return this;
    }

    public void setHousingTemplateId(HousingTemplate housingTemplate) {
        this.housingTemplateId = housingTemplate;
    }

    public BathRoomTemplate getBathroomTemplateId() {
        return bathroomTemplateId;
    }

    public ContainsBathRooms bathroomTemplateId(BathRoomTemplate bathRoomTemplate) {
        this.bathroomTemplateId = bathRoomTemplate;
        return this;
    }

    public void setBathroomTemplateId(BathRoomTemplate bathRoomTemplate) {
        this.bathroomTemplateId = bathRoomTemplate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContainsBathRooms)) {
            return false;
        }
        return id != null && id.equals(((ContainsBathRooms) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContainsBathRooms{" +
            "id=" + getId() +
            ", nbOfUnit=" + getNbOfUnit() +
            "}";
    }
}
