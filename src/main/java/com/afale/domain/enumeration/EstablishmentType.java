package com.afale.domain.enumeration;

/**
 * The EstablishmentType enumeration.
 */
public enum EstablishmentType {
    HOTEL, CAMPING
}
