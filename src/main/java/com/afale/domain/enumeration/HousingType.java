package com.afale.domain.enumeration;

/**
 * The HousingType enumeration.
 */
public enum HousingType {
    HOTELROOM, BUNGALOW
}
