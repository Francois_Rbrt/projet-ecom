package com.afale.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A BathRoomTemplate.
 */
@Entity
@Table(name = "bath_room_template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "bathroomtemplate")
public class BathRoomTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Does the bathroom has a bath
     */
    @ApiModelProperty(value = "Does the bathroom has a bath")
    @Column(name = "has_bath")
    private Boolean hasBath;

    /**
     * Does the bathroom has toilets
     */
    @ApiModelProperty(value = "Does the bathroom has toilets")
    @Column(name = "has_toilet")
    private Boolean hasToilet;

    /**
     * Does the bathroom has a shower
     */
    @ApiModelProperty(value = "Does the bathroom has a shower")
    @Column(name = "has_shower")
    private Boolean hasShower;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isHasBath() {
        return hasBath;
    }

    public BathRoomTemplate hasBath(Boolean hasBath) {
        this.hasBath = hasBath;
        return this;
    }

    public void setHasBath(Boolean hasBath) {
        this.hasBath = hasBath;
    }

    public Boolean isHasToilet() {
        return hasToilet;
    }

    public BathRoomTemplate hasToilet(Boolean hasToilet) {
        this.hasToilet = hasToilet;
        return this;
    }

    public void setHasToilet(Boolean hasToilet) {
        this.hasToilet = hasToilet;
    }

    public Boolean isHasShower() {
        return hasShower;
    }

    public BathRoomTemplate hasShower(Boolean hasShower) {
        this.hasShower = hasShower;
        return this;
    }

    public void setHasShower(Boolean hasShower) {
        this.hasShower = hasShower;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BathRoomTemplate)) {
            return false;
        }
        return id != null && id.equals(((BathRoomTemplate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BathRoomTemplate{" +
            "id=" + getId() +
            ", hasBath='" + isHasBath() + "'" +
            ", hasToilet='" + isHasToilet() + "'" +
            ", hasShower='" + isHasShower() + "'" +
            "}";
    }
}
