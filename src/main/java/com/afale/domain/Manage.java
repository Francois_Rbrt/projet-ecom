package com.afale.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A Manage.
 */
@Entity
@Table(name = "manage")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "manage")
public class Manage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The user that manage the establishment
     */
    @ApiModelProperty(value = "The user that manage the establishment")
    @ManyToOne
    @JsonIgnoreProperties(value = "manages", allowSetters = true)
    private User userId;

    /**
     * The establishment that is managed
     */
    @ApiModelProperty(value = "The establishment that is managed")
    @ManyToOne
    @JsonIgnoreProperties(value = "manages", allowSetters = true)
    private Establishment establishmentId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public Manage userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }

    public Establishment getEstablishmentId() {
        return establishmentId;
    }

    public Manage establishmentId(Establishment establishment) {
        this.establishmentId = establishment;
        return this;
    }

    public void setEstablishmentId(Establishment establishment) {
        this.establishmentId = establishment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Manage)) {
            return false;
        }
        return id != null && id.equals(((Manage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Manage{" +
            "id=" + getId() +
            "}";
    }
}
