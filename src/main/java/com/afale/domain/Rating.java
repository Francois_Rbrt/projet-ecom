package com.afale.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * The Rating entity
 */
@ApiModel(description = "The Rating entity")
@Entity
@Table(name = "rating")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "rating")
public class Rating implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The rate gave
     */
    @NotNull
    @Min(value = 1)
    @Max(value = 5)
    @ApiModelProperty(value = "The rate gave", required = true)
    @Column(name = "rate", nullable = false)
    private Integer rate;

    /**
     * comment on the rating
     */
    @Size(max = 250)
    @ApiModelProperty(value = "comment on the rating")
    @Column(name = "comment", length = 250)
    private String comment;

    /**
     * The date the rating has been done
     */
    @NotNull
    @ApiModelProperty(value = "The date the rating has been done", required = true)
    @Column(name = "rating_date", nullable = false)
    private LocalDate ratingDate;

    /**
     * The user that has got this favorite record
     */
    @ApiModelProperty(value = "The user that has got this favorite record")
    @ManyToOne
    @JsonIgnoreProperties(value = "ratings", allowSetters = true)
    private User userId;

    /**
     * The establishment the user put in his favorites
     */
    @ApiModelProperty(value = "The establishment the user put in his favorites")
    @ManyToOne
    @JsonIgnoreProperties(value = "ratings", allowSetters = true)
    private Establishment establishmentId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public Rating rate(Integer rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public Rating comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getRatingDate() {
        return ratingDate;
    }

    public Rating ratingDate(LocalDate ratingDate) {
        this.ratingDate = ratingDate;
        return this;
    }

    public void setRatingDate(LocalDate ratingDate) {
        this.ratingDate = ratingDate;
    }

    public User getUserId() {
        return userId;
    }

    public Rating userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }

    public Establishment getEstablishmentId() {
        return establishmentId;
    }

    public Rating establishmentId(Establishment establishment) {
        this.establishmentId = establishment;
        return this;
    }

    public void setEstablishmentId(Establishment establishment) {
        this.establishmentId = establishment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rating)) {
            return false;
        }
        return id != null && id.equals(((Rating) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Rating{" +
            "id=" + getId() +
            ", rate=" + getRate() +
            ", comment='" + getComment() + "'" +
            ", ratingDate='" + getRatingDate() + "'" +
            "}";
    }
}
