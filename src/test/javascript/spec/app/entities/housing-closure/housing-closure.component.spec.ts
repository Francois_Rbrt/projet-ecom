import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { HousingClosureComponent } from 'app/entities/housing-closure/housing-closure.component';
import { HousingClosureService } from 'app/entities/housing-closure/housing-closure.service';
import { HousingClosure } from 'app/shared/model/housing-closure.model';

describe('Component Tests', () => {
  describe('HousingClosure Management Component', () => {
    let comp: HousingClosureComponent;
    let fixture: ComponentFixture<HousingClosureComponent>;
    let service: HousingClosureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [HousingClosureComponent],
      })
        .overrideTemplate(HousingClosureComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HousingClosureComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HousingClosureService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new HousingClosure(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.housingClosures && comp.housingClosures[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
