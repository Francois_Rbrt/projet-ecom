import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { HousingClosureUpdateComponent } from 'app/entities/housing-closure/housing-closure-update.component';
import { HousingClosureService } from 'app/entities/housing-closure/housing-closure.service';
import { HousingClosure } from 'app/shared/model/housing-closure.model';

describe('Component Tests', () => {
  describe('HousingClosure Management Update Component', () => {
    let comp: HousingClosureUpdateComponent;
    let fixture: ComponentFixture<HousingClosureUpdateComponent>;
    let service: HousingClosureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [HousingClosureUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(HousingClosureUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HousingClosureUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HousingClosureService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new HousingClosure(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new HousingClosure();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
