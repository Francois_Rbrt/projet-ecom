import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { HousingClosureDetailComponent } from 'app/entities/housing-closure/housing-closure-detail.component';
import { HousingClosure } from 'app/shared/model/housing-closure.model';

describe('Component Tests', () => {
  describe('HousingClosure Management Detail Component', () => {
    let comp: HousingClosureDetailComponent;
    let fixture: ComponentFixture<HousingClosureDetailComponent>;
    const route = ({ data: of({ housingClosure: new HousingClosure(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [HousingClosureDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(HousingClosureDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HousingClosureDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load housingClosure on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.housingClosure).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
