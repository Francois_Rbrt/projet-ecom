import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { BathRoomTemplateUpdateComponent } from 'app/entities/bath-room-template/bath-room-template-update.component';
import { BathRoomTemplateService } from 'app/entities/bath-room-template/bath-room-template.service';
import { BathRoomTemplate } from 'app/shared/model/bath-room-template.model';

describe('Component Tests', () => {
  describe('BathRoomTemplate Management Update Component', () => {
    let comp: BathRoomTemplateUpdateComponent;
    let fixture: ComponentFixture<BathRoomTemplateUpdateComponent>;
    let service: BathRoomTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BathRoomTemplateUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BathRoomTemplateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BathRoomTemplateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BathRoomTemplateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BathRoomTemplate(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BathRoomTemplate();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
