import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { BathRoomTemplateComponent } from 'app/entities/bath-room-template/bath-room-template.component';
import { BathRoomTemplateService } from 'app/entities/bath-room-template/bath-room-template.service';
import { BathRoomTemplate } from 'app/shared/model/bath-room-template.model';

describe('Component Tests', () => {
  describe('BathRoomTemplate Management Component', () => {
    let comp: BathRoomTemplateComponent;
    let fixture: ComponentFixture<BathRoomTemplateComponent>;
    let service: BathRoomTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BathRoomTemplateComponent],
      })
        .overrideTemplate(BathRoomTemplateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BathRoomTemplateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BathRoomTemplateService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BathRoomTemplate(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bathRoomTemplates && comp.bathRoomTemplates[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
