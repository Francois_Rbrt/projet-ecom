import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { BathRoomTemplateDetailComponent } from 'app/entities/bath-room-template/bath-room-template-detail.component';
import { BathRoomTemplate } from 'app/shared/model/bath-room-template.model';

describe('Component Tests', () => {
  describe('BathRoomTemplate Management Detail Component', () => {
    let comp: BathRoomTemplateDetailComponent;
    let fixture: ComponentFixture<BathRoomTemplateDetailComponent>;
    const route = ({ data: of({ bathRoomTemplate: new BathRoomTemplate(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BathRoomTemplateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BathRoomTemplateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BathRoomTemplateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bathRoomTemplate on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bathRoomTemplate).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
