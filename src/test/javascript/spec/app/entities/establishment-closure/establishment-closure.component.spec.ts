import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { EstablishmentClosureComponent } from 'app/entities/establishment-closure/establishment-closure.component';
import { EstablishmentClosureService } from 'app/entities/establishment-closure/establishment-closure.service';
import { EstablishmentClosure } from 'app/shared/model/establishment-closure.model';

describe('Component Tests', () => {
  describe('EstablishmentClosure Management Component', () => {
    let comp: EstablishmentClosureComponent;
    let fixture: ComponentFixture<EstablishmentClosureComponent>;
    let service: EstablishmentClosureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [EstablishmentClosureComponent],
      })
        .overrideTemplate(EstablishmentClosureComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EstablishmentClosureComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EstablishmentClosureService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new EstablishmentClosure(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.establishmentClosures && comp.establishmentClosures[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
