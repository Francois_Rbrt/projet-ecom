import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { EstablishmentClosureUpdateComponent } from 'app/entities/establishment-closure/establishment-closure-update.component';
import { EstablishmentClosureService } from 'app/entities/establishment-closure/establishment-closure.service';
import { EstablishmentClosure } from 'app/shared/model/establishment-closure.model';

describe('Component Tests', () => {
  describe('EstablishmentClosure Management Update Component', () => {
    let comp: EstablishmentClosureUpdateComponent;
    let fixture: ComponentFixture<EstablishmentClosureUpdateComponent>;
    let service: EstablishmentClosureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [EstablishmentClosureUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(EstablishmentClosureUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EstablishmentClosureUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EstablishmentClosureService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EstablishmentClosure(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EstablishmentClosure();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
