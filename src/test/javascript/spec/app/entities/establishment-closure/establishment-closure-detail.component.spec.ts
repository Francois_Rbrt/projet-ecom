import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { EstablishmentClosureDetailComponent } from 'app/entities/establishment-closure/establishment-closure-detail.component';
import { EstablishmentClosure } from 'app/shared/model/establishment-closure.model';

describe('Component Tests', () => {
  describe('EstablishmentClosure Management Detail Component', () => {
    let comp: EstablishmentClosureDetailComponent;
    let fixture: ComponentFixture<EstablishmentClosureDetailComponent>;
    const route = ({ data: of({ establishmentClosure: new EstablishmentClosure(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [EstablishmentClosureDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(EstablishmentClosureDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EstablishmentClosureDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load establishmentClosure on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.establishmentClosure).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
