import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { BedRoomTemplateComponent } from 'app/entities/bed-room-template/bed-room-template.component';
import { BedRoomTemplateService } from 'app/entities/bed-room-template/bed-room-template.service';
import { BedRoomTemplate } from 'app/shared/model/bed-room-template.model';

describe('Component Tests', () => {
  describe('BedRoomTemplate Management Component', () => {
    let comp: BedRoomTemplateComponent;
    let fixture: ComponentFixture<BedRoomTemplateComponent>;
    let service: BedRoomTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BedRoomTemplateComponent],
      })
        .overrideTemplate(BedRoomTemplateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BedRoomTemplateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BedRoomTemplateService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BedRoomTemplate(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bedRoomTemplates && comp.bedRoomTemplates[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
