import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { BedRoomTemplateUpdateComponent } from 'app/entities/bed-room-template/bed-room-template-update.component';
import { BedRoomTemplateService } from 'app/entities/bed-room-template/bed-room-template.service';
import { BedRoomTemplate } from 'app/shared/model/bed-room-template.model';

describe('Component Tests', () => {
  describe('BedRoomTemplate Management Update Component', () => {
    let comp: BedRoomTemplateUpdateComponent;
    let fixture: ComponentFixture<BedRoomTemplateUpdateComponent>;
    let service: BedRoomTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BedRoomTemplateUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BedRoomTemplateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BedRoomTemplateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BedRoomTemplateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BedRoomTemplate(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BedRoomTemplate();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
