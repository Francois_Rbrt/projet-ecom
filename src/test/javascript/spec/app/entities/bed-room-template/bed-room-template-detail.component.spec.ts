import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { BedRoomTemplateDetailComponent } from 'app/entities/bed-room-template/bed-room-template-detail.component';
import { BedRoomTemplate } from 'app/shared/model/bed-room-template.model';

describe('Component Tests', () => {
  describe('BedRoomTemplate Management Detail Component', () => {
    let comp: BedRoomTemplateDetailComponent;
    let fixture: ComponentFixture<BedRoomTemplateDetailComponent>;
    const route = ({ data: of({ bedRoomTemplate: new BedRoomTemplate(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BedRoomTemplateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BedRoomTemplateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BedRoomTemplateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bedRoomTemplate on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bedRoomTemplate).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
