import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';
import { IEstablishment, Establishment } from 'app/shared/model/establishment.model';
import { EstablishmentType } from 'app/shared/model/enumerations/establishment-type.model';

describe('Service Tests', () => {
  describe('Establishment Service', () => {
    let injector: TestBed;
    let service: EstablishmentService;
    let httpMock: HttpTestingController;
    let elemDefault: IEstablishment;
    let expectedResult: IEstablishment | IEstablishment[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(EstablishmentService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Establishment(0, 'AAAAAAA', 'AAAAAAA', 0, 0, 0, EstablishmentType.HOTEL, false, false, false, false, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Establishment', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Establishment()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Establishment', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            adress: 'BBBBBB',
            latitude: 1,
            longitude: 1,
            globalRate: 1,
            establishmentType: 'BBBBBB',
            hasParking: true,
            hasRestaurant: true,
            hasFreeWifi: true,
            hasSwimmingPool: true,
            description: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Establishment', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            adress: 'BBBBBB',
            latitude: 1,
            longitude: 1,
            globalRate: 1,
            establishmentType: 'BBBBBB',
            hasParking: true,
            hasRestaurant: true,
            hasFreeWifi: true,
            hasSwimmingPool: true,
            description: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Establishment', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
