import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { EstablishmentComponent } from 'app/entities/establishment/establishment.component';
import { EstablishmentService } from 'app/entities/establishment/establishment.service';
import { Establishment } from 'app/shared/model/establishment.model';

describe('Component Tests', () => {
  describe('Establishment Management Component', () => {
    let comp: EstablishmentComponent;
    let fixture: ComponentFixture<EstablishmentComponent>;
    let service: EstablishmentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [EstablishmentComponent],
      })
        .overrideTemplate(EstablishmentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EstablishmentComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EstablishmentService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Establishment(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.establishments && comp.establishments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
