import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { EstablishmentDetailComponent } from 'app/entities/establishment/establishment-detail.component';
import { Establishment } from 'app/shared/model/establishment.model';

describe('Component Tests', () => {
  describe('Establishment Management Detail Component', () => {
    let comp: EstablishmentDetailComponent;
    let fixture: ComponentFixture<EstablishmentDetailComponent>;
    const route = ({ data: of({ establishment: new Establishment(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [EstablishmentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(EstablishmentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EstablishmentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load establishment on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.establishment).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
