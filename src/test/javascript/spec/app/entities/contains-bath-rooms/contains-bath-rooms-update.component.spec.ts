import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { ContainsBathRoomsUpdateComponent } from 'app/entities/contains-bath-rooms/contains-bath-rooms-update.component';
import { ContainsBathRoomsService } from 'app/entities/contains-bath-rooms/contains-bath-rooms.service';
import { ContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';

describe('Component Tests', () => {
  describe('ContainsBathRooms Management Update Component', () => {
    let comp: ContainsBathRoomsUpdateComponent;
    let fixture: ComponentFixture<ContainsBathRoomsUpdateComponent>;
    let service: ContainsBathRoomsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBathRoomsUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ContainsBathRoomsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContainsBathRoomsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContainsBathRoomsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContainsBathRooms(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContainsBathRooms();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
