import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { ContainsBathRoomsDetailComponent } from 'app/entities/contains-bath-rooms/contains-bath-rooms-detail.component';
import { ContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';

describe('Component Tests', () => {
  describe('ContainsBathRooms Management Detail Component', () => {
    let comp: ContainsBathRoomsDetailComponent;
    let fixture: ComponentFixture<ContainsBathRoomsDetailComponent>;
    const route = ({ data: of({ containsBathRooms: new ContainsBathRooms(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBathRoomsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ContainsBathRoomsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContainsBathRoomsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load containsBathRooms on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.containsBathRooms).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
