import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { ContainsBathRoomsComponent } from 'app/entities/contains-bath-rooms/contains-bath-rooms.component';
import { ContainsBathRoomsService } from 'app/entities/contains-bath-rooms/contains-bath-rooms.service';
import { ContainsBathRooms } from 'app/shared/model/contains-bath-rooms.model';

describe('Component Tests', () => {
  describe('ContainsBathRooms Management Component', () => {
    let comp: ContainsBathRoomsComponent;
    let fixture: ComponentFixture<ContainsBathRoomsComponent>;
    let service: ContainsBathRoomsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBathRoomsComponent],
      })
        .overrideTemplate(ContainsBathRoomsComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContainsBathRoomsComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContainsBathRoomsService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ContainsBathRooms(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.containsBathRooms && comp.containsBathRooms[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
