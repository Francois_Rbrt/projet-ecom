import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AfaleTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { ContainsBathRoomsDeleteDialogComponent } from 'app/entities/contains-bath-rooms/contains-bath-rooms-delete-dialog.component';
import { ContainsBathRoomsService } from 'app/entities/contains-bath-rooms/contains-bath-rooms.service';

describe('Component Tests', () => {
  describe('ContainsBathRooms Management Delete Component', () => {
    let comp: ContainsBathRoomsDeleteDialogComponent;
    let fixture: ComponentFixture<ContainsBathRoomsDeleteDialogComponent>;
    let service: ContainsBathRoomsService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBathRoomsDeleteDialogComponent],
      })
        .overrideTemplate(ContainsBathRoomsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContainsBathRoomsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContainsBathRoomsService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
