import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { BookingElementDetailComponent } from 'app/entities/booking-element/booking-element-detail.component';
import { BookingElement } from 'app/shared/model/booking-element.model';

describe('Component Tests', () => {
  describe('BookingElement Management Detail Component', () => {
    let comp: BookingElementDetailComponent;
    let fixture: ComponentFixture<BookingElementDetailComponent>;
    const route = ({ data: of({ bookingElement: new BookingElement(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BookingElementDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BookingElementDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BookingElementDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bookingElement on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bookingElement).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
