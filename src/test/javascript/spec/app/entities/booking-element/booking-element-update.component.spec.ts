import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { BookingElementUpdateComponent } from 'app/entities/booking-element/booking-element-update.component';
import { BookingElementService } from 'app/entities/booking-element/booking-element.service';
import { BookingElement } from 'app/shared/model/booking-element.model';

describe('Component Tests', () => {
  describe('BookingElement Management Update Component', () => {
    let comp: BookingElementUpdateComponent;
    let fixture: ComponentFixture<BookingElementUpdateComponent>;
    let service: BookingElementService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BookingElementUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BookingElementUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingElementUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingElementService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingElement(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingElement();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
