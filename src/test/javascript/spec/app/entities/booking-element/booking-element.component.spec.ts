import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { BookingElementComponent } from 'app/entities/booking-element/booking-element.component';
import { BookingElementService } from 'app/entities/booking-element/booking-element.service';
import { BookingElement } from 'app/shared/model/booking-element.model';

describe('Component Tests', () => {
  describe('BookingElement Management Component', () => {
    let comp: BookingElementComponent;
    let fixture: ComponentFixture<BookingElementComponent>;
    let service: BookingElementService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [BookingElementComponent],
      })
        .overrideTemplate(BookingElementComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingElementComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingElementService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BookingElement(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bookingElements && comp.bookingElements[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
