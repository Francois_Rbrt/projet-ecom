import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { ManageUpdateComponent } from 'app/entities/manage/manage-update.component';
import { ManageService } from 'app/entities/manage/manage.service';
import { Manage } from 'app/shared/model/manage.model';

describe('Component Tests', () => {
  describe('Manage Management Update Component', () => {
    let comp: ManageUpdateComponent;
    let fixture: ComponentFixture<ManageUpdateComponent>;
    let service: ManageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ManageUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ManageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ManageUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ManageService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Manage(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Manage();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
