import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { ManageComponent } from 'app/entities/manage/manage.component';
import { ManageService } from 'app/entities/manage/manage.service';
import { Manage } from 'app/shared/model/manage.model';

describe('Component Tests', () => {
  describe('Manage Management Component', () => {
    let comp: ManageComponent;
    let fixture: ComponentFixture<ManageComponent>;
    let service: ManageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ManageComponent],
      })
        .overrideTemplate(ManageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ManageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ManageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Manage(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.manages && comp.manages[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
