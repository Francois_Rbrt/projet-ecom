import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { ManageDetailComponent } from 'app/entities/manage/manage-detail.component';
import { Manage } from 'app/shared/model/manage.model';

describe('Component Tests', () => {
  describe('Manage Management Detail Component', () => {
    let comp: ManageDetailComponent;
    let fixture: ComponentFixture<ManageDetailComponent>;
    const route = ({ data: of({ manage: new Manage(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ManageDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ManageDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ManageDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load manage on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.manage).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
