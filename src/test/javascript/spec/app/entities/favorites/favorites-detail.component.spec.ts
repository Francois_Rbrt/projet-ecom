import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { FavoritesDetailComponent } from 'app/entities/favorites/favorites-detail.component';
import { Favorites } from 'app/shared/model/favorites.model';

describe('Component Tests', () => {
  describe('Favorites Management Detail Component', () => {
    let comp: FavoritesDetailComponent;
    let fixture: ComponentFixture<FavoritesDetailComponent>;
    const route = ({ data: of({ favorites: new Favorites(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [FavoritesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FavoritesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FavoritesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load favorites on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.favorites).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
