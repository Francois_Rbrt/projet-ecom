import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { FavoritesComponent } from 'app/entities/favorites/favorites.component';
import { FavoritesService } from 'app/entities/favorites/favorites.service';
import { Favorites } from 'app/shared/model/favorites.model';

describe('Component Tests', () => {
  describe('Favorites Management Component', () => {
    let comp: FavoritesComponent;
    let fixture: ComponentFixture<FavoritesComponent>;
    let service: FavoritesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [FavoritesComponent],
      })
        .overrideTemplate(FavoritesComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FavoritesComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FavoritesService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Favorites(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.favorites && comp.favorites[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
