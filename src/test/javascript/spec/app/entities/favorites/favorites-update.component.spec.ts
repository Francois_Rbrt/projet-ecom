import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { FavoritesUpdateComponent } from 'app/entities/favorites/favorites-update.component';
import { FavoritesService } from 'app/entities/favorites/favorites.service';
import { Favorites } from 'app/shared/model/favorites.model';

describe('Component Tests', () => {
  describe('Favorites Management Update Component', () => {
    let comp: FavoritesUpdateComponent;
    let fixture: ComponentFixture<FavoritesUpdateComponent>;
    let service: FavoritesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [FavoritesUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FavoritesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FavoritesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FavoritesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Favorites(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Favorites();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
