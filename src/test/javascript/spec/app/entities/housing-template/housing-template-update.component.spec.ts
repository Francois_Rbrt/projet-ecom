import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { HousingTemplateUpdateComponent } from 'app/entities/housing-template/housing-template-update.component';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { HousingTemplate } from 'app/shared/model/housing-template.model';

describe('Component Tests', () => {
  describe('HousingTemplate Management Update Component', () => {
    let comp: HousingTemplateUpdateComponent;
    let fixture: ComponentFixture<HousingTemplateUpdateComponent>;
    let service: HousingTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [HousingTemplateUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(HousingTemplateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HousingTemplateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HousingTemplateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new HousingTemplate(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new HousingTemplate();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
