import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { HousingTemplateComponent } from 'app/entities/housing-template/housing-template.component';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { HousingTemplate } from 'app/shared/model/housing-template.model';

describe('Component Tests', () => {
  describe('HousingTemplate Management Component', () => {
    let comp: HousingTemplateComponent;
    let fixture: ComponentFixture<HousingTemplateComponent>;
    let service: HousingTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [HousingTemplateComponent],
      })
        .overrideTemplate(HousingTemplateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HousingTemplateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HousingTemplateService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new HousingTemplate(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.housingTemplates && comp.housingTemplates[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
