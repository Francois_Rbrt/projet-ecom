import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HousingTemplateService } from 'app/entities/housing-template/housing-template.service';
import { IHousingTemplate, HousingTemplate } from 'app/shared/model/housing-template.model';
import { HousingType } from 'app/shared/model/enumerations/housing-type.model';

describe('Service Tests', () => {
  describe('HousingTemplate Service', () => {
    let injector: TestBed;
    let service: HousingTemplateService;
    let httpMock: HttpTestingController;
    let elemDefault: IHousingTemplate;
    let expectedResult: IHousingTemplate | IHousingTemplate[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(HousingTemplateService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new HousingTemplate(0, HousingType.HOTELROOM, 0, 0, 0, false, false, false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a HousingTemplate', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new HousingTemplate()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a HousingTemplate', () => {
        const returnedFromService = Object.assign(
          {
            housingType: 'BBBBBB',
            nbOfUnit: 1,
            nbMaxOfOccupants: 1,
            pricePerNight: 1,
            isNonSmoking: true,
            hasKitchen: true,
            hasToilets: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of HousingTemplate', () => {
        const returnedFromService = Object.assign(
          {
            housingType: 'BBBBBB',
            nbOfUnit: 1,
            nbMaxOfOccupants: 1,
            pricePerNight: 1,
            isNonSmoking: true,
            hasKitchen: true,
            hasToilets: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a HousingTemplate', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
