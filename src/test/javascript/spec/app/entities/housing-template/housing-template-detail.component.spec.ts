import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { HousingTemplateDetailComponent } from 'app/entities/housing-template/housing-template-detail.component';
import { HousingTemplate } from 'app/shared/model/housing-template.model';

describe('Component Tests', () => {
  describe('HousingTemplate Management Detail Component', () => {
    let comp: HousingTemplateDetailComponent;
    let fixture: ComponentFixture<HousingTemplateDetailComponent>;
    const route = ({ data: of({ housingTemplate: new HousingTemplate(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [HousingTemplateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(HousingTemplateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HousingTemplateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load housingTemplate on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.housingTemplate).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
