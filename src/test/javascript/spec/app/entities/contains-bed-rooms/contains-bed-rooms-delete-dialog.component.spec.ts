import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AfaleTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { ContainsBedRoomsDeleteDialogComponent } from 'app/entities/contains-bed-rooms/contains-bed-rooms-delete-dialog.component';
import { ContainsBedRoomsService } from 'app/entities/contains-bed-rooms/contains-bed-rooms.service';

describe('Component Tests', () => {
  describe('ContainsBedRooms Management Delete Component', () => {
    let comp: ContainsBedRoomsDeleteDialogComponent;
    let fixture: ComponentFixture<ContainsBedRoomsDeleteDialogComponent>;
    let service: ContainsBedRoomsService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBedRoomsDeleteDialogComponent],
      })
        .overrideTemplate(ContainsBedRoomsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContainsBedRoomsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContainsBedRoomsService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
