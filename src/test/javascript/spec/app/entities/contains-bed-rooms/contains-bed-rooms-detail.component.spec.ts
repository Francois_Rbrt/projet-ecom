import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { ContainsBedRoomsDetailComponent } from 'app/entities/contains-bed-rooms/contains-bed-rooms-detail.component';
import { ContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';

describe('Component Tests', () => {
  describe('ContainsBedRooms Management Detail Component', () => {
    let comp: ContainsBedRoomsDetailComponent;
    let fixture: ComponentFixture<ContainsBedRoomsDetailComponent>;
    const route = ({ data: of({ containsBedRooms: new ContainsBedRooms(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBedRoomsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ContainsBedRoomsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContainsBedRoomsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load containsBedRooms on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.containsBedRooms).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
