import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AfaleTestModule } from '../../../test.module';
import { ContainsBedRoomsUpdateComponent } from 'app/entities/contains-bed-rooms/contains-bed-rooms-update.component';
import { ContainsBedRoomsService } from 'app/entities/contains-bed-rooms/contains-bed-rooms.service';
import { ContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';

describe('Component Tests', () => {
  describe('ContainsBedRooms Management Update Component', () => {
    let comp: ContainsBedRoomsUpdateComponent;
    let fixture: ComponentFixture<ContainsBedRoomsUpdateComponent>;
    let service: ContainsBedRoomsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBedRoomsUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ContainsBedRoomsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContainsBedRoomsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContainsBedRoomsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContainsBedRooms(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContainsBedRooms();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
