import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AfaleTestModule } from '../../../test.module';
import { ContainsBedRoomsComponent } from 'app/entities/contains-bed-rooms/contains-bed-rooms.component';
import { ContainsBedRoomsService } from 'app/entities/contains-bed-rooms/contains-bed-rooms.service';
import { ContainsBedRooms } from 'app/shared/model/contains-bed-rooms.model';

describe('Component Tests', () => {
  describe('ContainsBedRooms Management Component', () => {
    let comp: ContainsBedRoomsComponent;
    let fixture: ComponentFixture<ContainsBedRoomsComponent>;
    let service: ContainsBedRoomsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AfaleTestModule],
        declarations: [ContainsBedRoomsComponent],
      })
        .overrideTemplate(ContainsBedRoomsComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContainsBedRoomsComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContainsBedRoomsService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ContainsBedRooms(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.containsBedRooms && comp.containsBedRooms[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
