package com.afale.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link BathRoomTemplateSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class BathRoomTemplateSearchRepositoryMockConfiguration {

    @MockBean
    private BathRoomTemplateSearchRepository mockBathRoomTemplateSearchRepository;

}
