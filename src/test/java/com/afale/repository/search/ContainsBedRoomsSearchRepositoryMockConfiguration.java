package com.afale.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ContainsBedRoomsSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ContainsBedRoomsSearchRepositoryMockConfiguration {

    @MockBean
    private ContainsBedRoomsSearchRepository mockContainsBedRoomsSearchRepository;

}
