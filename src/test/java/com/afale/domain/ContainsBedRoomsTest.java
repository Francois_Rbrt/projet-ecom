package com.afale.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.afale.web.rest.TestUtil;

public class ContainsBedRoomsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContainsBedRooms.class);
        ContainsBedRooms containsBedRooms1 = new ContainsBedRooms();
        containsBedRooms1.setId(1L);
        ContainsBedRooms containsBedRooms2 = new ContainsBedRooms();
        containsBedRooms2.setId(containsBedRooms1.getId());
        assertThat(containsBedRooms1).isEqualTo(containsBedRooms2);
        containsBedRooms2.setId(2L);
        assertThat(containsBedRooms1).isNotEqualTo(containsBedRooms2);
        containsBedRooms1.setId(null);
        assertThat(containsBedRooms1).isNotEqualTo(containsBedRooms2);
    }
}
