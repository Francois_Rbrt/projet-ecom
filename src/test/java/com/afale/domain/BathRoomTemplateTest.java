package com.afale.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.afale.web.rest.TestUtil;

public class BathRoomTemplateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BathRoomTemplate.class);
        BathRoomTemplate bathRoomTemplate1 = new BathRoomTemplate();
        bathRoomTemplate1.setId(1L);
        BathRoomTemplate bathRoomTemplate2 = new BathRoomTemplate();
        bathRoomTemplate2.setId(bathRoomTemplate1.getId());
        assertThat(bathRoomTemplate1).isEqualTo(bathRoomTemplate2);
        bathRoomTemplate2.setId(2L);
        assertThat(bathRoomTemplate1).isNotEqualTo(bathRoomTemplate2);
        bathRoomTemplate1.setId(null);
        assertThat(bathRoomTemplate1).isNotEqualTo(bathRoomTemplate2);
    }
}
