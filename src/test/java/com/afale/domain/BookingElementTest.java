package com.afale.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.afale.web.rest.TestUtil;

public class BookingElementTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookingElement.class);
        BookingElement bookingElement1 = new BookingElement();
        bookingElement1.setId(1L);
        BookingElement bookingElement2 = new BookingElement();
        bookingElement2.setId(bookingElement1.getId());
        assertThat(bookingElement1).isEqualTo(bookingElement2);
        bookingElement2.setId(2L);
        assertThat(bookingElement1).isNotEqualTo(bookingElement2);
        bookingElement1.setId(null);
        assertThat(bookingElement1).isNotEqualTo(bookingElement2);
    }
}
