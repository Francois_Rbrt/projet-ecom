package com.afale.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.afale.web.rest.TestUtil;

public class BedRoomTemplateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BedRoomTemplate.class);
        BedRoomTemplate bedRoomTemplate1 = new BedRoomTemplate();
        bedRoomTemplate1.setId(1L);
        BedRoomTemplate bedRoomTemplate2 = new BedRoomTemplate();
        bedRoomTemplate2.setId(bedRoomTemplate1.getId());
        assertThat(bedRoomTemplate1).isEqualTo(bedRoomTemplate2);
        bedRoomTemplate2.setId(2L);
        assertThat(bedRoomTemplate1).isNotEqualTo(bedRoomTemplate2);
        bedRoomTemplate1.setId(null);
        assertThat(bedRoomTemplate1).isNotEqualTo(bedRoomTemplate2);
    }
}
