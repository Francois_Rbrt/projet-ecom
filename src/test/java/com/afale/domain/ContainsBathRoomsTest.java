package com.afale.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.afale.web.rest.TestUtil;

public class ContainsBathRoomsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContainsBathRooms.class);
        ContainsBathRooms containsBathRooms1 = new ContainsBathRooms();
        containsBathRooms1.setId(1L);
        ContainsBathRooms containsBathRooms2 = new ContainsBathRooms();
        containsBathRooms2.setId(containsBathRooms1.getId());
        assertThat(containsBathRooms1).isEqualTo(containsBathRooms2);
        containsBathRooms2.setId(2L);
        assertThat(containsBathRooms1).isNotEqualTo(containsBathRooms2);
        containsBathRooms1.setId(null);
        assertThat(containsBathRooms1).isNotEqualTo(containsBathRooms2);
    }
}
