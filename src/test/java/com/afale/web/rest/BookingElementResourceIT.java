package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.BookingElement;
import com.afale.repository.BookingElementRepository;
import com.afale.repository.search.BookingElementSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BookingElementResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class BookingElementResourceIT {

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_ELEMENT_PRICE = 1;
    private static final Integer UPDATED_ELEMENT_PRICE = 2;

    @Autowired
    private BookingElementRepository bookingElementRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.BookingElementSearchRepositoryMockConfiguration
     */
    @Autowired
    private BookingElementSearchRepository mockBookingElementSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBookingElementMockMvc;

    private BookingElement bookingElement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookingElement createEntity(EntityManager em) {
        BookingElement bookingElement = new BookingElement()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .elementPrice(DEFAULT_ELEMENT_PRICE);
        return bookingElement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookingElement createUpdatedEntity(EntityManager em) {
        BookingElement bookingElement = new BookingElement()
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .elementPrice(UPDATED_ELEMENT_PRICE);
        return bookingElement;
    }

    @BeforeEach
    public void initTest() {
        bookingElement = createEntity(em);
    }

    @Test
    @Transactional
    public void createBookingElement() throws Exception {
        int databaseSizeBeforeCreate = bookingElementRepository.findAll().size();
        // Create the BookingElement
        restBookingElementMockMvc.perform(post("/api/booking-elements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingElement)))
            .andExpect(status().isCreated());

        // Validate the BookingElement in the database
        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeCreate + 1);
        BookingElement testBookingElement = bookingElementList.get(bookingElementList.size() - 1);
        assertThat(testBookingElement.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testBookingElement.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testBookingElement.getElementPrice()).isEqualTo(DEFAULT_ELEMENT_PRICE);

        // Validate the BookingElement in Elasticsearch
        verify(mockBookingElementSearchRepository, times(1)).save(testBookingElement);
    }

    @Test
    @Transactional
    public void createBookingElementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bookingElementRepository.findAll().size();

        // Create the BookingElement with an existing ID
        bookingElement.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookingElementMockMvc.perform(post("/api/booking-elements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingElement)))
            .andExpect(status().isBadRequest());

        // Validate the BookingElement in the database
        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeCreate);

        // Validate the BookingElement in Elasticsearch
        verify(mockBookingElementSearchRepository, times(0)).save(bookingElement);
    }


    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingElementRepository.findAll().size();
        // set the field null
        bookingElement.setStartDate(null);

        // Create the BookingElement, which fails.


        restBookingElementMockMvc.perform(post("/api/booking-elements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingElement)))
            .andExpect(status().isBadRequest());

        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingElementRepository.findAll().size();
        // set the field null
        bookingElement.setEndDate(null);

        // Create the BookingElement, which fails.


        restBookingElementMockMvc.perform(post("/api/booking-elements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingElement)))
            .andExpect(status().isBadRequest());

        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkElementPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingElementRepository.findAll().size();
        // set the field null
        bookingElement.setElementPrice(null);

        // Create the BookingElement, which fails.


        restBookingElementMockMvc.perform(post("/api/booking-elements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingElement)))
            .andExpect(status().isBadRequest());

        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBookingElements() throws Exception {
        // Initialize the database
        bookingElementRepository.saveAndFlush(bookingElement);

        // Get all the bookingElementList
        restBookingElementMockMvc.perform(get("/api/booking-elements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bookingElement.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].elementPrice").value(hasItem(DEFAULT_ELEMENT_PRICE)));
    }
    
    @Test
    @Transactional
    public void getBookingElement() throws Exception {
        // Initialize the database
        bookingElementRepository.saveAndFlush(bookingElement);

        // Get the bookingElement
        restBookingElementMockMvc.perform(get("/api/booking-elements/{id}", bookingElement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bookingElement.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.elementPrice").value(DEFAULT_ELEMENT_PRICE));
    }
    @Test
    @Transactional
    public void getNonExistingBookingElement() throws Exception {
        // Get the bookingElement
        restBookingElementMockMvc.perform(get("/api/booking-elements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBookingElement() throws Exception {
        // Initialize the database
        bookingElementRepository.saveAndFlush(bookingElement);

        int databaseSizeBeforeUpdate = bookingElementRepository.findAll().size();

        // Update the bookingElement
        BookingElement updatedBookingElement = bookingElementRepository.findById(bookingElement.getId()).get();
        // Disconnect from session so that the updates on updatedBookingElement are not directly saved in db
        em.detach(updatedBookingElement);
        updatedBookingElement
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .elementPrice(UPDATED_ELEMENT_PRICE);

        restBookingElementMockMvc.perform(put("/api/booking-elements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBookingElement)))
            .andExpect(status().isOk());

        // Validate the BookingElement in the database
        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeUpdate);
        BookingElement testBookingElement = bookingElementList.get(bookingElementList.size() - 1);
        assertThat(testBookingElement.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testBookingElement.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testBookingElement.getElementPrice()).isEqualTo(UPDATED_ELEMENT_PRICE);

        // Validate the BookingElement in Elasticsearch
        verify(mockBookingElementSearchRepository, times(1)).save(testBookingElement);
    }

    @Test
    @Transactional
    public void updateNonExistingBookingElement() throws Exception {
        int databaseSizeBeforeUpdate = bookingElementRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookingElementMockMvc.perform(put("/api/booking-elements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bookingElement)))
            .andExpect(status().isBadRequest());

        // Validate the BookingElement in the database
        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BookingElement in Elasticsearch
        verify(mockBookingElementSearchRepository, times(0)).save(bookingElement);
    }

    @Test
    @Transactional
    public void deleteBookingElement() throws Exception {
        // Initialize the database
        bookingElementRepository.saveAndFlush(bookingElement);

        int databaseSizeBeforeDelete = bookingElementRepository.findAll().size();

        // Delete the bookingElement
        restBookingElementMockMvc.perform(delete("/api/booking-elements/{id}", bookingElement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BookingElement> bookingElementList = bookingElementRepository.findAll();
        assertThat(bookingElementList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the BookingElement in Elasticsearch
        verify(mockBookingElementSearchRepository, times(1)).deleteById(bookingElement.getId());
    }

    @Test
    @Transactional
    public void searchBookingElement() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        bookingElementRepository.saveAndFlush(bookingElement);
        when(mockBookingElementSearchRepository.search(queryStringQuery("id:" + bookingElement.getId())))
            .thenReturn(Collections.singletonList(bookingElement));

        // Search the bookingElement
        restBookingElementMockMvc.perform(get("/api/_search/booking-elements?query=id:" + bookingElement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bookingElement.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].elementPrice").value(hasItem(DEFAULT_ELEMENT_PRICE)));
    }
}
