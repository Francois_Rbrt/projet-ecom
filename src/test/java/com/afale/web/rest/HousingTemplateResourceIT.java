package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.HousingTemplate;
import com.afale.repository.HousingTemplateRepository;
import com.afale.repository.search.HousingTemplateSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.afale.domain.enumeration.HousingType;
/**
 * Integration tests for the {@link HousingTemplateResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class HousingTemplateResourceIT {

    private static final HousingType DEFAULT_HOUSING_TYPE = HousingType.HOTELROOM;
    private static final HousingType UPDATED_HOUSING_TYPE = HousingType.BUNGALOW;

    private static final Integer DEFAULT_NB_OF_UNIT = 1;
    private static final Integer UPDATED_NB_OF_UNIT = 2;

    private static final Integer DEFAULT_NB_MAX_OF_OCCUPANTS = 1;
    private static final Integer UPDATED_NB_MAX_OF_OCCUPANTS = 2;

    private static final Float DEFAULT_PRICE_PER_NIGHT = 1F;
    private static final Float UPDATED_PRICE_PER_NIGHT = 2F;

    private static final Boolean DEFAULT_IS_NON_SMOKING = false;
    private static final Boolean UPDATED_IS_NON_SMOKING = true;

    private static final Boolean DEFAULT_HAS_KITCHEN = false;
    private static final Boolean UPDATED_HAS_KITCHEN = true;

    private static final Boolean DEFAULT_HAS_TOILETS = false;
    private static final Boolean UPDATED_HAS_TOILETS = true;

    @Autowired
    private HousingTemplateRepository housingTemplateRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.HousingTemplateSearchRepositoryMockConfiguration
     */
    @Autowired
    private HousingTemplateSearchRepository mockHousingTemplateSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHousingTemplateMockMvc;

    private HousingTemplate housingTemplate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HousingTemplate createEntity(EntityManager em) {
        HousingTemplate housingTemplate = new HousingTemplate()
            .housingType(DEFAULT_HOUSING_TYPE)
            .nbOfUnit(DEFAULT_NB_OF_UNIT)
            .nbMaxOfOccupants(DEFAULT_NB_MAX_OF_OCCUPANTS)
            .pricePerNight(DEFAULT_PRICE_PER_NIGHT)
            .isNonSmoking(DEFAULT_IS_NON_SMOKING)
            .hasKitchen(DEFAULT_HAS_KITCHEN)
            .hasToilets(DEFAULT_HAS_TOILETS);
        return housingTemplate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HousingTemplate createUpdatedEntity(EntityManager em) {
        HousingTemplate housingTemplate = new HousingTemplate()
            .housingType(UPDATED_HOUSING_TYPE)
            .nbOfUnit(UPDATED_NB_OF_UNIT)
            .nbMaxOfOccupants(UPDATED_NB_MAX_OF_OCCUPANTS)
            .pricePerNight(UPDATED_PRICE_PER_NIGHT)
            .isNonSmoking(UPDATED_IS_NON_SMOKING)
            .hasKitchen(UPDATED_HAS_KITCHEN)
            .hasToilets(UPDATED_HAS_TOILETS);
        return housingTemplate;
    }

    @BeforeEach
    public void initTest() {
        housingTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createHousingTemplate() throws Exception {
        int databaseSizeBeforeCreate = housingTemplateRepository.findAll().size();
        // Create the HousingTemplate
        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isCreated());

        // Validate the HousingTemplate in the database
        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        HousingTemplate testHousingTemplate = housingTemplateList.get(housingTemplateList.size() - 1);
        assertThat(testHousingTemplate.getHousingType()).isEqualTo(DEFAULT_HOUSING_TYPE);
        assertThat(testHousingTemplate.getNbOfUnit()).isEqualTo(DEFAULT_NB_OF_UNIT);
        assertThat(testHousingTemplate.getNbMaxOfOccupants()).isEqualTo(DEFAULT_NB_MAX_OF_OCCUPANTS);
        assertThat(testHousingTemplate.getPricePerNight()).isEqualTo(DEFAULT_PRICE_PER_NIGHT);
        assertThat(testHousingTemplate.isIsNonSmoking()).isEqualTo(DEFAULT_IS_NON_SMOKING);
        assertThat(testHousingTemplate.isHasKitchen()).isEqualTo(DEFAULT_HAS_KITCHEN);
        assertThat(testHousingTemplate.isHasToilets()).isEqualTo(DEFAULT_HAS_TOILETS);

        // Validate the HousingTemplate in Elasticsearch
        verify(mockHousingTemplateSearchRepository, times(1)).save(testHousingTemplate);
    }

    @Test
    @Transactional
    public void createHousingTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = housingTemplateRepository.findAll().size();

        // Create the HousingTemplate with an existing ID
        housingTemplate.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the HousingTemplate in the database
        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeCreate);

        // Validate the HousingTemplate in Elasticsearch
        verify(mockHousingTemplateSearchRepository, times(0)).save(housingTemplate);
    }


    @Test
    @Transactional
    public void checkHousingTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingTemplateRepository.findAll().size();
        // set the field null
        housingTemplate.setHousingType(null);

        // Create the HousingTemplate, which fails.


        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNbOfUnitIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingTemplateRepository.findAll().size();
        // set the field null
        housingTemplate.setNbOfUnit(null);

        // Create the HousingTemplate, which fails.


        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNbMaxOfOccupantsIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingTemplateRepository.findAll().size();
        // set the field null
        housingTemplate.setNbMaxOfOccupants(null);

        // Create the HousingTemplate, which fails.


        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPricePerNightIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingTemplateRepository.findAll().size();
        // set the field null
        housingTemplate.setPricePerNight(null);

        // Create the HousingTemplate, which fails.


        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsNonSmokingIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingTemplateRepository.findAll().size();
        // set the field null
        housingTemplate.setIsNonSmoking(null);

        // Create the HousingTemplate, which fails.


        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHasKitchenIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingTemplateRepository.findAll().size();
        // set the field null
        housingTemplate.setHasKitchen(null);

        // Create the HousingTemplate, which fails.


        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHasToiletsIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingTemplateRepository.findAll().size();
        // set the field null
        housingTemplate.setHasToilets(null);

        // Create the HousingTemplate, which fails.


        restHousingTemplateMockMvc.perform(post("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHousingTemplates() throws Exception {
        // Initialize the database
        housingTemplateRepository.saveAndFlush(housingTemplate);

        // Get all the housingTemplateList
        restHousingTemplateMockMvc.perform(get("/api/housing-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(housingTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].housingType").value(hasItem(DEFAULT_HOUSING_TYPE.toString())))
            .andExpect(jsonPath("$.[*].nbOfUnit").value(hasItem(DEFAULT_NB_OF_UNIT)))
            .andExpect(jsonPath("$.[*].nbMaxOfOccupants").value(hasItem(DEFAULT_NB_MAX_OF_OCCUPANTS)))
            .andExpect(jsonPath("$.[*].pricePerNight").value(hasItem(DEFAULT_PRICE_PER_NIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].isNonSmoking").value(hasItem(DEFAULT_IS_NON_SMOKING.booleanValue())))
            .andExpect(jsonPath("$.[*].hasKitchen").value(hasItem(DEFAULT_HAS_KITCHEN.booleanValue())))
            .andExpect(jsonPath("$.[*].hasToilets").value(hasItem(DEFAULT_HAS_TOILETS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getHousingTemplate() throws Exception {
        // Initialize the database
        housingTemplateRepository.saveAndFlush(housingTemplate);

        // Get the housingTemplate
        restHousingTemplateMockMvc.perform(get("/api/housing-templates/{id}", housingTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(housingTemplate.getId().intValue()))
            .andExpect(jsonPath("$.housingType").value(DEFAULT_HOUSING_TYPE.toString()))
            .andExpect(jsonPath("$.nbOfUnit").value(DEFAULT_NB_OF_UNIT))
            .andExpect(jsonPath("$.nbMaxOfOccupants").value(DEFAULT_NB_MAX_OF_OCCUPANTS))
            .andExpect(jsonPath("$.pricePerNight").value(DEFAULT_PRICE_PER_NIGHT.doubleValue()))
            .andExpect(jsonPath("$.isNonSmoking").value(DEFAULT_IS_NON_SMOKING.booleanValue()))
            .andExpect(jsonPath("$.hasKitchen").value(DEFAULT_HAS_KITCHEN.booleanValue()))
            .andExpect(jsonPath("$.hasToilets").value(DEFAULT_HAS_TOILETS.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingHousingTemplate() throws Exception {
        // Get the housingTemplate
        restHousingTemplateMockMvc.perform(get("/api/housing-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHousingTemplate() throws Exception {
        // Initialize the database
        housingTemplateRepository.saveAndFlush(housingTemplate);

        int databaseSizeBeforeUpdate = housingTemplateRepository.findAll().size();

        // Update the housingTemplate
        HousingTemplate updatedHousingTemplate = housingTemplateRepository.findById(housingTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedHousingTemplate are not directly saved in db
        em.detach(updatedHousingTemplate);
        updatedHousingTemplate
            .housingType(UPDATED_HOUSING_TYPE)
            .nbOfUnit(UPDATED_NB_OF_UNIT)
            .nbMaxOfOccupants(UPDATED_NB_MAX_OF_OCCUPANTS)
            .pricePerNight(UPDATED_PRICE_PER_NIGHT)
            .isNonSmoking(UPDATED_IS_NON_SMOKING)
            .hasKitchen(UPDATED_HAS_KITCHEN)
            .hasToilets(UPDATED_HAS_TOILETS);

        restHousingTemplateMockMvc.perform(put("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedHousingTemplate)))
            .andExpect(status().isOk());

        // Validate the HousingTemplate in the database
        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeUpdate);
        HousingTemplate testHousingTemplate = housingTemplateList.get(housingTemplateList.size() - 1);
        assertThat(testHousingTemplate.getHousingType()).isEqualTo(UPDATED_HOUSING_TYPE);
        assertThat(testHousingTemplate.getNbOfUnit()).isEqualTo(UPDATED_NB_OF_UNIT);
        assertThat(testHousingTemplate.getNbMaxOfOccupants()).isEqualTo(UPDATED_NB_MAX_OF_OCCUPANTS);
        assertThat(testHousingTemplate.getPricePerNight()).isEqualTo(UPDATED_PRICE_PER_NIGHT);
        assertThat(testHousingTemplate.isIsNonSmoking()).isEqualTo(UPDATED_IS_NON_SMOKING);
        assertThat(testHousingTemplate.isHasKitchen()).isEqualTo(UPDATED_HAS_KITCHEN);
        assertThat(testHousingTemplate.isHasToilets()).isEqualTo(UPDATED_HAS_TOILETS);

        // Validate the HousingTemplate in Elasticsearch
        verify(mockHousingTemplateSearchRepository, times(1)).save(testHousingTemplate);
    }

    @Test
    @Transactional
    public void updateNonExistingHousingTemplate() throws Exception {
        int databaseSizeBeforeUpdate = housingTemplateRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHousingTemplateMockMvc.perform(put("/api/housing-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the HousingTemplate in the database
        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeUpdate);

        // Validate the HousingTemplate in Elasticsearch
        verify(mockHousingTemplateSearchRepository, times(0)).save(housingTemplate);
    }

    @Test
    @Transactional
    public void deleteHousingTemplate() throws Exception {
        // Initialize the database
        housingTemplateRepository.saveAndFlush(housingTemplate);

        int databaseSizeBeforeDelete = housingTemplateRepository.findAll().size();

        // Delete the housingTemplate
        restHousingTemplateMockMvc.perform(delete("/api/housing-templates/{id}", housingTemplate.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HousingTemplate> housingTemplateList = housingTemplateRepository.findAll();
        assertThat(housingTemplateList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the HousingTemplate in Elasticsearch
        verify(mockHousingTemplateSearchRepository, times(1)).deleteById(housingTemplate.getId());
    }

    @Test
    @Transactional
    public void searchHousingTemplate() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        housingTemplateRepository.saveAndFlush(housingTemplate);
        when(mockHousingTemplateSearchRepository.search(queryStringQuery("id:" + housingTemplate.getId())))
            .thenReturn(Collections.singletonList(housingTemplate));

        // Search the housingTemplate
        restHousingTemplateMockMvc.perform(get("/api/_search/housing-templates?query=id:" + housingTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(housingTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].housingType").value(hasItem(DEFAULT_HOUSING_TYPE.toString())))
            .andExpect(jsonPath("$.[*].nbOfUnit").value(hasItem(DEFAULT_NB_OF_UNIT)))
            .andExpect(jsonPath("$.[*].nbMaxOfOccupants").value(hasItem(DEFAULT_NB_MAX_OF_OCCUPANTS)))
            .andExpect(jsonPath("$.[*].pricePerNight").value(hasItem(DEFAULT_PRICE_PER_NIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].isNonSmoking").value(hasItem(DEFAULT_IS_NON_SMOKING.booleanValue())))
            .andExpect(jsonPath("$.[*].hasKitchen").value(hasItem(DEFAULT_HAS_KITCHEN.booleanValue())))
            .andExpect(jsonPath("$.[*].hasToilets").value(hasItem(DEFAULT_HAS_TOILETS.booleanValue())));
    }
}
