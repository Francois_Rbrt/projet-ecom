package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.Favorites;
import com.afale.repository.FavoritesRepository;
import com.afale.repository.search.FavoritesSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FavoritesResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class FavoritesResourceIT {

    @Autowired
    private FavoritesRepository favoritesRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.FavoritesSearchRepositoryMockConfiguration
     */
    @Autowired
    private FavoritesSearchRepository mockFavoritesSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFavoritesMockMvc;

    private Favorites favorites;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Favorites createEntity(EntityManager em) {
        Favorites favorites = new Favorites();
        return favorites;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Favorites createUpdatedEntity(EntityManager em) {
        Favorites favorites = new Favorites();
        return favorites;
    }

    @BeforeEach
    public void initTest() {
        favorites = createEntity(em);
    }

    @Test
    @Transactional
    public void createFavorites() throws Exception {
        int databaseSizeBeforeCreate = favoritesRepository.findAll().size();
        // Create the Favorites
        restFavoritesMockMvc.perform(post("/api/favorites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(favorites)))
            .andExpect(status().isCreated());

        // Validate the Favorites in the database
        List<Favorites> favoritesList = favoritesRepository.findAll();
        assertThat(favoritesList).hasSize(databaseSizeBeforeCreate + 1);
        Favorites testFavorites = favoritesList.get(favoritesList.size() - 1);

        // Validate the Favorites in Elasticsearch
        verify(mockFavoritesSearchRepository, times(1)).save(testFavorites);
    }

    @Test
    @Transactional
    public void createFavoritesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = favoritesRepository.findAll().size();

        // Create the Favorites with an existing ID
        favorites.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFavoritesMockMvc.perform(post("/api/favorites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(favorites)))
            .andExpect(status().isBadRequest());

        // Validate the Favorites in the database
        List<Favorites> favoritesList = favoritesRepository.findAll();
        assertThat(favoritesList).hasSize(databaseSizeBeforeCreate);

        // Validate the Favorites in Elasticsearch
        verify(mockFavoritesSearchRepository, times(0)).save(favorites);
    }


    @Test
    @Transactional
    public void getAllFavorites() throws Exception {
        // Initialize the database
        favoritesRepository.saveAndFlush(favorites);

        // Get all the favoritesList
        restFavoritesMockMvc.perform(get("/api/favorites?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(favorites.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getFavorites() throws Exception {
        // Initialize the database
        favoritesRepository.saveAndFlush(favorites);

        // Get the favorites
        restFavoritesMockMvc.perform(get("/api/favorites/{id}", favorites.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(favorites.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFavorites() throws Exception {
        // Get the favorites
        restFavoritesMockMvc.perform(get("/api/favorites/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFavorites() throws Exception {
        // Initialize the database
        favoritesRepository.saveAndFlush(favorites);

        int databaseSizeBeforeUpdate = favoritesRepository.findAll().size();

        // Update the favorites
        Favorites updatedFavorites = favoritesRepository.findById(favorites.getId()).get();
        // Disconnect from session so that the updates on updatedFavorites are not directly saved in db
        em.detach(updatedFavorites);

        restFavoritesMockMvc.perform(put("/api/favorites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFavorites)))
            .andExpect(status().isOk());

        // Validate the Favorites in the database
        List<Favorites> favoritesList = favoritesRepository.findAll();
        assertThat(favoritesList).hasSize(databaseSizeBeforeUpdate);
        Favorites testFavorites = favoritesList.get(favoritesList.size() - 1);

        // Validate the Favorites in Elasticsearch
        verify(mockFavoritesSearchRepository, times(1)).save(testFavorites);
    }

    @Test
    @Transactional
    public void updateNonExistingFavorites() throws Exception {
        int databaseSizeBeforeUpdate = favoritesRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFavoritesMockMvc.perform(put("/api/favorites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(favorites)))
            .andExpect(status().isBadRequest());

        // Validate the Favorites in the database
        List<Favorites> favoritesList = favoritesRepository.findAll();
        assertThat(favoritesList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Favorites in Elasticsearch
        verify(mockFavoritesSearchRepository, times(0)).save(favorites);
    }

    @Test
    @Transactional
    public void deleteFavorites() throws Exception {
        // Initialize the database
        favoritesRepository.saveAndFlush(favorites);

        int databaseSizeBeforeDelete = favoritesRepository.findAll().size();

        // Delete the favorites
        restFavoritesMockMvc.perform(delete("/api/favorites/{id}", favorites.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Favorites> favoritesList = favoritesRepository.findAll();
        assertThat(favoritesList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Favorites in Elasticsearch
        verify(mockFavoritesSearchRepository, times(1)).deleteById(favorites.getId());
    }

    @Test
    @Transactional
    public void searchFavorites() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        favoritesRepository.saveAndFlush(favorites);
        when(mockFavoritesSearchRepository.search(queryStringQuery("id:" + favorites.getId())))
            .thenReturn(Collections.singletonList(favorites));

        // Search the favorites
        restFavoritesMockMvc.perform(get("/api/_search/favorites?query=id:" + favorites.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(favorites.getId().intValue())));
    }
}
