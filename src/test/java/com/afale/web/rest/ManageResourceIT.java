package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.Manage;
import com.afale.repository.ManageRepository;
import com.afale.repository.search.ManageSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ManageResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ManageResourceIT {

    @Autowired
    private ManageRepository manageRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.ManageSearchRepositoryMockConfiguration
     */
    @Autowired
    private ManageSearchRepository mockManageSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restManageMockMvc;

    private Manage manage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Manage createEntity(EntityManager em) {
        Manage manage = new Manage();
        return manage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Manage createUpdatedEntity(EntityManager em) {
        Manage manage = new Manage();
        return manage;
    }

    @BeforeEach
    public void initTest() {
        manage = createEntity(em);
    }

    @Test
    @Transactional
    public void createManage() throws Exception {
        int databaseSizeBeforeCreate = manageRepository.findAll().size();
        // Create the Manage
        restManageMockMvc.perform(post("/api/manages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(manage)))
            .andExpect(status().isCreated());

        // Validate the Manage in the database
        List<Manage> manageList = manageRepository.findAll();
        assertThat(manageList).hasSize(databaseSizeBeforeCreate + 1);
        Manage testManage = manageList.get(manageList.size() - 1);

        // Validate the Manage in Elasticsearch
        verify(mockManageSearchRepository, times(1)).save(testManage);
    }

    @Test
    @Transactional
    public void createManageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = manageRepository.findAll().size();

        // Create the Manage with an existing ID
        manage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restManageMockMvc.perform(post("/api/manages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(manage)))
            .andExpect(status().isBadRequest());

        // Validate the Manage in the database
        List<Manage> manageList = manageRepository.findAll();
        assertThat(manageList).hasSize(databaseSizeBeforeCreate);

        // Validate the Manage in Elasticsearch
        verify(mockManageSearchRepository, times(0)).save(manage);
    }


    @Test
    @Transactional
    public void getAllManages() throws Exception {
        // Initialize the database
        manageRepository.saveAndFlush(manage);

        // Get all the manageList
        restManageMockMvc.perform(get("/api/manages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(manage.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getManage() throws Exception {
        // Initialize the database
        manageRepository.saveAndFlush(manage);

        // Get the manage
        restManageMockMvc.perform(get("/api/manages/{id}", manage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(manage.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingManage() throws Exception {
        // Get the manage
        restManageMockMvc.perform(get("/api/manages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateManage() throws Exception {
        // Initialize the database
        manageRepository.saveAndFlush(manage);

        int databaseSizeBeforeUpdate = manageRepository.findAll().size();

        // Update the manage
        Manage updatedManage = manageRepository.findById(manage.getId()).get();
        // Disconnect from session so that the updates on updatedManage are not directly saved in db
        em.detach(updatedManage);

        restManageMockMvc.perform(put("/api/manages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedManage)))
            .andExpect(status().isOk());

        // Validate the Manage in the database
        List<Manage> manageList = manageRepository.findAll();
        assertThat(manageList).hasSize(databaseSizeBeforeUpdate);
        Manage testManage = manageList.get(manageList.size() - 1);

        // Validate the Manage in Elasticsearch
        verify(mockManageSearchRepository, times(1)).save(testManage);
    }

    @Test
    @Transactional
    public void updateNonExistingManage() throws Exception {
        int databaseSizeBeforeUpdate = manageRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restManageMockMvc.perform(put("/api/manages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(manage)))
            .andExpect(status().isBadRequest());

        // Validate the Manage in the database
        List<Manage> manageList = manageRepository.findAll();
        assertThat(manageList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Manage in Elasticsearch
        verify(mockManageSearchRepository, times(0)).save(manage);
    }

    @Test
    @Transactional
    public void deleteManage() throws Exception {
        // Initialize the database
        manageRepository.saveAndFlush(manage);

        int databaseSizeBeforeDelete = manageRepository.findAll().size();

        // Delete the manage
        restManageMockMvc.perform(delete("/api/manages/{id}", manage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Manage> manageList = manageRepository.findAll();
        assertThat(manageList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Manage in Elasticsearch
        verify(mockManageSearchRepository, times(1)).deleteById(manage.getId());
    }

    @Test
    @Transactional
    public void searchManage() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        manageRepository.saveAndFlush(manage);
        when(mockManageSearchRepository.search(queryStringQuery("id:" + manage.getId())))
            .thenReturn(Collections.singletonList(manage));

        // Search the manage
        restManageMockMvc.perform(get("/api/_search/manages?query=id:" + manage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(manage.getId().intValue())));
    }
}
