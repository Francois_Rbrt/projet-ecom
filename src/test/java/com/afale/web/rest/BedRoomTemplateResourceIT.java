package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.BedRoomTemplate;
import com.afale.repository.BedRoomTemplateRepository;
import com.afale.repository.search.BedRoomTemplateSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BedRoomTemplateResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class BedRoomTemplateResourceIT {

    private static final Integer DEFAULT_NB_SINGLE_BEDS = 1;
    private static final Integer UPDATED_NB_SINGLE_BEDS = 2;

    private static final Integer DEFAULT_NB_DOUBLE_BEDS = 1;
    private static final Integer UPDATED_NB_DOUBLE_BEDS = 2;

    @Autowired
    private BedRoomTemplateRepository bedRoomTemplateRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.BedRoomTemplateSearchRepositoryMockConfiguration
     */
    @Autowired
    private BedRoomTemplateSearchRepository mockBedRoomTemplateSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBedRoomTemplateMockMvc;

    private BedRoomTemplate bedRoomTemplate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BedRoomTemplate createEntity(EntityManager em) {
        BedRoomTemplate bedRoomTemplate = new BedRoomTemplate()
            .nbSingleBeds(DEFAULT_NB_SINGLE_BEDS)
            .nbDoubleBeds(DEFAULT_NB_DOUBLE_BEDS);
        return bedRoomTemplate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BedRoomTemplate createUpdatedEntity(EntityManager em) {
        BedRoomTemplate bedRoomTemplate = new BedRoomTemplate()
            .nbSingleBeds(UPDATED_NB_SINGLE_BEDS)
            .nbDoubleBeds(UPDATED_NB_DOUBLE_BEDS);
        return bedRoomTemplate;
    }

    @BeforeEach
    public void initTest() {
        bedRoomTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createBedRoomTemplate() throws Exception {
        int databaseSizeBeforeCreate = bedRoomTemplateRepository.findAll().size();
        // Create the BedRoomTemplate
        restBedRoomTemplateMockMvc.perform(post("/api/bed-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bedRoomTemplate)))
            .andExpect(status().isCreated());

        // Validate the BedRoomTemplate in the database
        List<BedRoomTemplate> bedRoomTemplateList = bedRoomTemplateRepository.findAll();
        assertThat(bedRoomTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        BedRoomTemplate testBedRoomTemplate = bedRoomTemplateList.get(bedRoomTemplateList.size() - 1);
        assertThat(testBedRoomTemplate.getNbSingleBeds()).isEqualTo(DEFAULT_NB_SINGLE_BEDS);
        assertThat(testBedRoomTemplate.getNbDoubleBeds()).isEqualTo(DEFAULT_NB_DOUBLE_BEDS);

        // Validate the BedRoomTemplate in Elasticsearch
        verify(mockBedRoomTemplateSearchRepository, times(1)).save(testBedRoomTemplate);
    }

    @Test
    @Transactional
    public void createBedRoomTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bedRoomTemplateRepository.findAll().size();

        // Create the BedRoomTemplate with an existing ID
        bedRoomTemplate.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBedRoomTemplateMockMvc.perform(post("/api/bed-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bedRoomTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the BedRoomTemplate in the database
        List<BedRoomTemplate> bedRoomTemplateList = bedRoomTemplateRepository.findAll();
        assertThat(bedRoomTemplateList).hasSize(databaseSizeBeforeCreate);

        // Validate the BedRoomTemplate in Elasticsearch
        verify(mockBedRoomTemplateSearchRepository, times(0)).save(bedRoomTemplate);
    }


    @Test
    @Transactional
    public void checkNbSingleBedsIsRequired() throws Exception {
        int databaseSizeBeforeTest = bedRoomTemplateRepository.findAll().size();
        // set the field null
        bedRoomTemplate.setNbSingleBeds(null);

        // Create the BedRoomTemplate, which fails.


        restBedRoomTemplateMockMvc.perform(post("/api/bed-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bedRoomTemplate)))
            .andExpect(status().isBadRequest());

        List<BedRoomTemplate> bedRoomTemplateList = bedRoomTemplateRepository.findAll();
        assertThat(bedRoomTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNbDoubleBedsIsRequired() throws Exception {
        int databaseSizeBeforeTest = bedRoomTemplateRepository.findAll().size();
        // set the field null
        bedRoomTemplate.setNbDoubleBeds(null);

        // Create the BedRoomTemplate, which fails.


        restBedRoomTemplateMockMvc.perform(post("/api/bed-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bedRoomTemplate)))
            .andExpect(status().isBadRequest());

        List<BedRoomTemplate> bedRoomTemplateList = bedRoomTemplateRepository.findAll();
        assertThat(bedRoomTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBedRoomTemplates() throws Exception {
        // Initialize the database
        bedRoomTemplateRepository.saveAndFlush(bedRoomTemplate);

        // Get all the bedRoomTemplateList
        restBedRoomTemplateMockMvc.perform(get("/api/bed-room-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bedRoomTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbSingleBeds").value(hasItem(DEFAULT_NB_SINGLE_BEDS)))
            .andExpect(jsonPath("$.[*].nbDoubleBeds").value(hasItem(DEFAULT_NB_DOUBLE_BEDS)));
    }
    
    @Test
    @Transactional
    public void getBedRoomTemplate() throws Exception {
        // Initialize the database
        bedRoomTemplateRepository.saveAndFlush(bedRoomTemplate);

        // Get the bedRoomTemplate
        restBedRoomTemplateMockMvc.perform(get("/api/bed-room-templates/{id}", bedRoomTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bedRoomTemplate.getId().intValue()))
            .andExpect(jsonPath("$.nbSingleBeds").value(DEFAULT_NB_SINGLE_BEDS))
            .andExpect(jsonPath("$.nbDoubleBeds").value(DEFAULT_NB_DOUBLE_BEDS));
    }
    @Test
    @Transactional
    public void getNonExistingBedRoomTemplate() throws Exception {
        // Get the bedRoomTemplate
        restBedRoomTemplateMockMvc.perform(get("/api/bed-room-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBedRoomTemplate() throws Exception {
        // Initialize the database
        bedRoomTemplateRepository.saveAndFlush(bedRoomTemplate);

        int databaseSizeBeforeUpdate = bedRoomTemplateRepository.findAll().size();

        // Update the bedRoomTemplate
        BedRoomTemplate updatedBedRoomTemplate = bedRoomTemplateRepository.findById(bedRoomTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedBedRoomTemplate are not directly saved in db
        em.detach(updatedBedRoomTemplate);
        updatedBedRoomTemplate
            .nbSingleBeds(UPDATED_NB_SINGLE_BEDS)
            .nbDoubleBeds(UPDATED_NB_DOUBLE_BEDS);

        restBedRoomTemplateMockMvc.perform(put("/api/bed-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBedRoomTemplate)))
            .andExpect(status().isOk());

        // Validate the BedRoomTemplate in the database
        List<BedRoomTemplate> bedRoomTemplateList = bedRoomTemplateRepository.findAll();
        assertThat(bedRoomTemplateList).hasSize(databaseSizeBeforeUpdate);
        BedRoomTemplate testBedRoomTemplate = bedRoomTemplateList.get(bedRoomTemplateList.size() - 1);
        assertThat(testBedRoomTemplate.getNbSingleBeds()).isEqualTo(UPDATED_NB_SINGLE_BEDS);
        assertThat(testBedRoomTemplate.getNbDoubleBeds()).isEqualTo(UPDATED_NB_DOUBLE_BEDS);

        // Validate the BedRoomTemplate in Elasticsearch
        verify(mockBedRoomTemplateSearchRepository, times(1)).save(testBedRoomTemplate);
    }

    @Test
    @Transactional
    public void updateNonExistingBedRoomTemplate() throws Exception {
        int databaseSizeBeforeUpdate = bedRoomTemplateRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBedRoomTemplateMockMvc.perform(put("/api/bed-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bedRoomTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the BedRoomTemplate in the database
        List<BedRoomTemplate> bedRoomTemplateList = bedRoomTemplateRepository.findAll();
        assertThat(bedRoomTemplateList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BedRoomTemplate in Elasticsearch
        verify(mockBedRoomTemplateSearchRepository, times(0)).save(bedRoomTemplate);
    }

    @Test
    @Transactional
    public void deleteBedRoomTemplate() throws Exception {
        // Initialize the database
        bedRoomTemplateRepository.saveAndFlush(bedRoomTemplate);

        int databaseSizeBeforeDelete = bedRoomTemplateRepository.findAll().size();

        // Delete the bedRoomTemplate
        restBedRoomTemplateMockMvc.perform(delete("/api/bed-room-templates/{id}", bedRoomTemplate.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BedRoomTemplate> bedRoomTemplateList = bedRoomTemplateRepository.findAll();
        assertThat(bedRoomTemplateList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the BedRoomTemplate in Elasticsearch
        verify(mockBedRoomTemplateSearchRepository, times(1)).deleteById(bedRoomTemplate.getId());
    }

    @Test
    @Transactional
    public void searchBedRoomTemplate() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        bedRoomTemplateRepository.saveAndFlush(bedRoomTemplate);
        when(mockBedRoomTemplateSearchRepository.search(queryStringQuery("id:" + bedRoomTemplate.getId())))
            .thenReturn(Collections.singletonList(bedRoomTemplate));

        // Search the bedRoomTemplate
        restBedRoomTemplateMockMvc.perform(get("/api/_search/bed-room-templates?query=id:" + bedRoomTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bedRoomTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbSingleBeds").value(hasItem(DEFAULT_NB_SINGLE_BEDS)))
            .andExpect(jsonPath("$.[*].nbDoubleBeds").value(hasItem(DEFAULT_NB_DOUBLE_BEDS)));
    }
}
