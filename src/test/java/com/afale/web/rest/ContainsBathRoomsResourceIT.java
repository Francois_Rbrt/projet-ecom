package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.ContainsBathRooms;
import com.afale.repository.ContainsBathRoomsRepository;
import com.afale.repository.search.ContainsBathRoomsSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContainsBathRoomsResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContainsBathRoomsResourceIT {

    private static final Integer DEFAULT_NB_OF_UNIT = 0;
    private static final Integer UPDATED_NB_OF_UNIT = 1;

    @Autowired
    private ContainsBathRoomsRepository containsBathRoomsRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.ContainsBathRoomsSearchRepositoryMockConfiguration
     */
    @Autowired
    private ContainsBathRoomsSearchRepository mockContainsBathRoomsSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContainsBathRoomsMockMvc;

    private ContainsBathRooms containsBathRooms;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContainsBathRooms createEntity(EntityManager em) {
        ContainsBathRooms containsBathRooms = new ContainsBathRooms()
            .nbOfUnit(DEFAULT_NB_OF_UNIT);
        return containsBathRooms;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContainsBathRooms createUpdatedEntity(EntityManager em) {
        ContainsBathRooms containsBathRooms = new ContainsBathRooms()
            .nbOfUnit(UPDATED_NB_OF_UNIT);
        return containsBathRooms;
    }

    @BeforeEach
    public void initTest() {
        containsBathRooms = createEntity(em);
    }

    @Test
    @Transactional
    public void createContainsBathRooms() throws Exception {
        int databaseSizeBeforeCreate = containsBathRoomsRepository.findAll().size();
        // Create the ContainsBathRooms
        restContainsBathRoomsMockMvc.perform(post("/api/contains-bath-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBathRooms)))
            .andExpect(status().isCreated());

        // Validate the ContainsBathRooms in the database
        List<ContainsBathRooms> containsBathRoomsList = containsBathRoomsRepository.findAll();
        assertThat(containsBathRoomsList).hasSize(databaseSizeBeforeCreate + 1);
        ContainsBathRooms testContainsBathRooms = containsBathRoomsList.get(containsBathRoomsList.size() - 1);
        assertThat(testContainsBathRooms.getNbOfUnit()).isEqualTo(DEFAULT_NB_OF_UNIT);

        // Validate the ContainsBathRooms in Elasticsearch
        verify(mockContainsBathRoomsSearchRepository, times(1)).save(testContainsBathRooms);
    }

    @Test
    @Transactional
    public void createContainsBathRoomsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = containsBathRoomsRepository.findAll().size();

        // Create the ContainsBathRooms with an existing ID
        containsBathRooms.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContainsBathRoomsMockMvc.perform(post("/api/contains-bath-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBathRooms)))
            .andExpect(status().isBadRequest());

        // Validate the ContainsBathRooms in the database
        List<ContainsBathRooms> containsBathRoomsList = containsBathRoomsRepository.findAll();
        assertThat(containsBathRoomsList).hasSize(databaseSizeBeforeCreate);

        // Validate the ContainsBathRooms in Elasticsearch
        verify(mockContainsBathRoomsSearchRepository, times(0)).save(containsBathRooms);
    }


    @Test
    @Transactional
    public void checkNbOfUnitIsRequired() throws Exception {
        int databaseSizeBeforeTest = containsBathRoomsRepository.findAll().size();
        // set the field null
        containsBathRooms.setNbOfUnit(null);

        // Create the ContainsBathRooms, which fails.


        restContainsBathRoomsMockMvc.perform(post("/api/contains-bath-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBathRooms)))
            .andExpect(status().isBadRequest());

        List<ContainsBathRooms> containsBathRoomsList = containsBathRoomsRepository.findAll();
        assertThat(containsBathRoomsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContainsBathRooms() throws Exception {
        // Initialize the database
        containsBathRoomsRepository.saveAndFlush(containsBathRooms);

        // Get all the containsBathRoomsList
        restContainsBathRoomsMockMvc.perform(get("/api/contains-bath-rooms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(containsBathRooms.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbOfUnit").value(hasItem(DEFAULT_NB_OF_UNIT)));
    }
    
    @Test
    @Transactional
    public void getContainsBathRooms() throws Exception {
        // Initialize the database
        containsBathRoomsRepository.saveAndFlush(containsBathRooms);

        // Get the containsBathRooms
        restContainsBathRoomsMockMvc.perform(get("/api/contains-bath-rooms/{id}", containsBathRooms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(containsBathRooms.getId().intValue()))
            .andExpect(jsonPath("$.nbOfUnit").value(DEFAULT_NB_OF_UNIT));
    }
    @Test
    @Transactional
    public void getNonExistingContainsBathRooms() throws Exception {
        // Get the containsBathRooms
        restContainsBathRoomsMockMvc.perform(get("/api/contains-bath-rooms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContainsBathRooms() throws Exception {
        // Initialize the database
        containsBathRoomsRepository.saveAndFlush(containsBathRooms);

        int databaseSizeBeforeUpdate = containsBathRoomsRepository.findAll().size();

        // Update the containsBathRooms
        ContainsBathRooms updatedContainsBathRooms = containsBathRoomsRepository.findById(containsBathRooms.getId()).get();
        // Disconnect from session so that the updates on updatedContainsBathRooms are not directly saved in db
        em.detach(updatedContainsBathRooms);
        updatedContainsBathRooms
            .nbOfUnit(UPDATED_NB_OF_UNIT);

        restContainsBathRoomsMockMvc.perform(put("/api/contains-bath-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedContainsBathRooms)))
            .andExpect(status().isOk());

        // Validate the ContainsBathRooms in the database
        List<ContainsBathRooms> containsBathRoomsList = containsBathRoomsRepository.findAll();
        assertThat(containsBathRoomsList).hasSize(databaseSizeBeforeUpdate);
        ContainsBathRooms testContainsBathRooms = containsBathRoomsList.get(containsBathRoomsList.size() - 1);
        assertThat(testContainsBathRooms.getNbOfUnit()).isEqualTo(UPDATED_NB_OF_UNIT);

        // Validate the ContainsBathRooms in Elasticsearch
        verify(mockContainsBathRoomsSearchRepository, times(1)).save(testContainsBathRooms);
    }

    @Test
    @Transactional
    public void updateNonExistingContainsBathRooms() throws Exception {
        int databaseSizeBeforeUpdate = containsBathRoomsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContainsBathRoomsMockMvc.perform(put("/api/contains-bath-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBathRooms)))
            .andExpect(status().isBadRequest());

        // Validate the ContainsBathRooms in the database
        List<ContainsBathRooms> containsBathRoomsList = containsBathRoomsRepository.findAll();
        assertThat(containsBathRoomsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ContainsBathRooms in Elasticsearch
        verify(mockContainsBathRoomsSearchRepository, times(0)).save(containsBathRooms);
    }

    @Test
    @Transactional
    public void deleteContainsBathRooms() throws Exception {
        // Initialize the database
        containsBathRoomsRepository.saveAndFlush(containsBathRooms);

        int databaseSizeBeforeDelete = containsBathRoomsRepository.findAll().size();

        // Delete the containsBathRooms
        restContainsBathRoomsMockMvc.perform(delete("/api/contains-bath-rooms/{id}", containsBathRooms.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContainsBathRooms> containsBathRoomsList = containsBathRoomsRepository.findAll();
        assertThat(containsBathRoomsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ContainsBathRooms in Elasticsearch
        verify(mockContainsBathRoomsSearchRepository, times(1)).deleteById(containsBathRooms.getId());
    }

    @Test
    @Transactional
    public void searchContainsBathRooms() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        containsBathRoomsRepository.saveAndFlush(containsBathRooms);
        when(mockContainsBathRoomsSearchRepository.search(queryStringQuery("id:" + containsBathRooms.getId())))
            .thenReturn(Collections.singletonList(containsBathRooms));

        // Search the containsBathRooms
        restContainsBathRoomsMockMvc.perform(get("/api/_search/contains-bath-rooms?query=id:" + containsBathRooms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(containsBathRooms.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbOfUnit").value(hasItem(DEFAULT_NB_OF_UNIT)));
    }
}
