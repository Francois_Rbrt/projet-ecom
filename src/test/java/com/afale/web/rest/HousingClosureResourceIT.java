package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.HousingClosure;
import com.afale.repository.HousingClosureRepository;
import com.afale.repository.search.HousingClosureSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HousingClosureResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class HousingClosureResourceIT {

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CAUSE = "AAAAAAAAAA";
    private static final String UPDATED_CAUSE = "BBBBBBBBBB";

    @Autowired
    private HousingClosureRepository housingClosureRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.HousingClosureSearchRepositoryMockConfiguration
     */
    @Autowired
    private HousingClosureSearchRepository mockHousingClosureSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHousingClosureMockMvc;

    private HousingClosure housingClosure;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HousingClosure createEntity(EntityManager em) {
        HousingClosure housingClosure = new HousingClosure()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .cause(DEFAULT_CAUSE);
        return housingClosure;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HousingClosure createUpdatedEntity(EntityManager em) {
        HousingClosure housingClosure = new HousingClosure()
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .cause(UPDATED_CAUSE);
        return housingClosure;
    }

    @BeforeEach
    public void initTest() {
        housingClosure = createEntity(em);
    }

    @Test
    @Transactional
    public void createHousingClosure() throws Exception {
        int databaseSizeBeforeCreate = housingClosureRepository.findAll().size();
        // Create the HousingClosure
        restHousingClosureMockMvc.perform(post("/api/housing-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingClosure)))
            .andExpect(status().isCreated());

        // Validate the HousingClosure in the database
        List<HousingClosure> housingClosureList = housingClosureRepository.findAll();
        assertThat(housingClosureList).hasSize(databaseSizeBeforeCreate + 1);
        HousingClosure testHousingClosure = housingClosureList.get(housingClosureList.size() - 1);
        assertThat(testHousingClosure.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testHousingClosure.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testHousingClosure.getCause()).isEqualTo(DEFAULT_CAUSE);

        // Validate the HousingClosure in Elasticsearch
        verify(mockHousingClosureSearchRepository, times(1)).save(testHousingClosure);
    }

    @Test
    @Transactional
    public void createHousingClosureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = housingClosureRepository.findAll().size();

        // Create the HousingClosure with an existing ID
        housingClosure.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHousingClosureMockMvc.perform(post("/api/housing-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingClosure)))
            .andExpect(status().isBadRequest());

        // Validate the HousingClosure in the database
        List<HousingClosure> housingClosureList = housingClosureRepository.findAll();
        assertThat(housingClosureList).hasSize(databaseSizeBeforeCreate);

        // Validate the HousingClosure in Elasticsearch
        verify(mockHousingClosureSearchRepository, times(0)).save(housingClosure);
    }


    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingClosureRepository.findAll().size();
        // set the field null
        housingClosure.setStartDate(null);

        // Create the HousingClosure, which fails.


        restHousingClosureMockMvc.perform(post("/api/housing-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingClosure)))
            .andExpect(status().isBadRequest());

        List<HousingClosure> housingClosureList = housingClosureRepository.findAll();
        assertThat(housingClosureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = housingClosureRepository.findAll().size();
        // set the field null
        housingClosure.setEndDate(null);

        // Create the HousingClosure, which fails.


        restHousingClosureMockMvc.perform(post("/api/housing-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingClosure)))
            .andExpect(status().isBadRequest());

        List<HousingClosure> housingClosureList = housingClosureRepository.findAll();
        assertThat(housingClosureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHousingClosures() throws Exception {
        // Initialize the database
        housingClosureRepository.saveAndFlush(housingClosure);

        // Get all the housingClosureList
        restHousingClosureMockMvc.perform(get("/api/housing-closures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(housingClosure.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].cause").value(hasItem(DEFAULT_CAUSE)));
    }
    
    @Test
    @Transactional
    public void getHousingClosure() throws Exception {
        // Initialize the database
        housingClosureRepository.saveAndFlush(housingClosure);

        // Get the housingClosure
        restHousingClosureMockMvc.perform(get("/api/housing-closures/{id}", housingClosure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(housingClosure.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.cause").value(DEFAULT_CAUSE));
    }
    @Test
    @Transactional
    public void getNonExistingHousingClosure() throws Exception {
        // Get the housingClosure
        restHousingClosureMockMvc.perform(get("/api/housing-closures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHousingClosure() throws Exception {
        // Initialize the database
        housingClosureRepository.saveAndFlush(housingClosure);

        int databaseSizeBeforeUpdate = housingClosureRepository.findAll().size();

        // Update the housingClosure
        HousingClosure updatedHousingClosure = housingClosureRepository.findById(housingClosure.getId()).get();
        // Disconnect from session so that the updates on updatedHousingClosure are not directly saved in db
        em.detach(updatedHousingClosure);
        updatedHousingClosure
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .cause(UPDATED_CAUSE);

        restHousingClosureMockMvc.perform(put("/api/housing-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedHousingClosure)))
            .andExpect(status().isOk());

        // Validate the HousingClosure in the database
        List<HousingClosure> housingClosureList = housingClosureRepository.findAll();
        assertThat(housingClosureList).hasSize(databaseSizeBeforeUpdate);
        HousingClosure testHousingClosure = housingClosureList.get(housingClosureList.size() - 1);
        assertThat(testHousingClosure.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testHousingClosure.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testHousingClosure.getCause()).isEqualTo(UPDATED_CAUSE);

        // Validate the HousingClosure in Elasticsearch
        verify(mockHousingClosureSearchRepository, times(1)).save(testHousingClosure);
    }

    @Test
    @Transactional
    public void updateNonExistingHousingClosure() throws Exception {
        int databaseSizeBeforeUpdate = housingClosureRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHousingClosureMockMvc.perform(put("/api/housing-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(housingClosure)))
            .andExpect(status().isBadRequest());

        // Validate the HousingClosure in the database
        List<HousingClosure> housingClosureList = housingClosureRepository.findAll();
        assertThat(housingClosureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the HousingClosure in Elasticsearch
        verify(mockHousingClosureSearchRepository, times(0)).save(housingClosure);
    }

    @Test
    @Transactional
    public void deleteHousingClosure() throws Exception {
        // Initialize the database
        housingClosureRepository.saveAndFlush(housingClosure);

        int databaseSizeBeforeDelete = housingClosureRepository.findAll().size();

        // Delete the housingClosure
        restHousingClosureMockMvc.perform(delete("/api/housing-closures/{id}", housingClosure.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HousingClosure> housingClosureList = housingClosureRepository.findAll();
        assertThat(housingClosureList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the HousingClosure in Elasticsearch
        verify(mockHousingClosureSearchRepository, times(1)).deleteById(housingClosure.getId());
    }

    @Test
    @Transactional
    public void searchHousingClosure() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        housingClosureRepository.saveAndFlush(housingClosure);
        when(mockHousingClosureSearchRepository.search(queryStringQuery("id:" + housingClosure.getId())))
            .thenReturn(Collections.singletonList(housingClosure));

        // Search the housingClosure
        restHousingClosureMockMvc.perform(get("/api/_search/housing-closures?query=id:" + housingClosure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(housingClosure.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].cause").value(hasItem(DEFAULT_CAUSE)));
    }
}
