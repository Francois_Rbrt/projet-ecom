package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.ContainsBedRooms;
import com.afale.repository.ContainsBedRoomsRepository;
import com.afale.repository.search.ContainsBedRoomsSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContainsBedRoomsResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContainsBedRoomsResourceIT {

    private static final Integer DEFAULT_NB_OF_UNIT = 0;
    private static final Integer UPDATED_NB_OF_UNIT = 1;

    @Autowired
    private ContainsBedRoomsRepository containsBedRoomsRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.ContainsBedRoomsSearchRepositoryMockConfiguration
     */
    @Autowired
    private ContainsBedRoomsSearchRepository mockContainsBedRoomsSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContainsBedRoomsMockMvc;

    private ContainsBedRooms containsBedRooms;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContainsBedRooms createEntity(EntityManager em) {
        ContainsBedRooms containsBedRooms = new ContainsBedRooms()
            .nbOfUnit(DEFAULT_NB_OF_UNIT);
        return containsBedRooms;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContainsBedRooms createUpdatedEntity(EntityManager em) {
        ContainsBedRooms containsBedRooms = new ContainsBedRooms()
            .nbOfUnit(UPDATED_NB_OF_UNIT);
        return containsBedRooms;
    }

    @BeforeEach
    public void initTest() {
        containsBedRooms = createEntity(em);
    }

    @Test
    @Transactional
    public void createContainsBedRooms() throws Exception {
        int databaseSizeBeforeCreate = containsBedRoomsRepository.findAll().size();
        // Create the ContainsBedRooms
        restContainsBedRoomsMockMvc.perform(post("/api/contains-bed-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBedRooms)))
            .andExpect(status().isCreated());

        // Validate the ContainsBedRooms in the database
        List<ContainsBedRooms> containsBedRoomsList = containsBedRoomsRepository.findAll();
        assertThat(containsBedRoomsList).hasSize(databaseSizeBeforeCreate + 1);
        ContainsBedRooms testContainsBedRooms = containsBedRoomsList.get(containsBedRoomsList.size() - 1);
        assertThat(testContainsBedRooms.getNbOfUnit()).isEqualTo(DEFAULT_NB_OF_UNIT);

        // Validate the ContainsBedRooms in Elasticsearch
        verify(mockContainsBedRoomsSearchRepository, times(1)).save(testContainsBedRooms);
    }

    @Test
    @Transactional
    public void createContainsBedRoomsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = containsBedRoomsRepository.findAll().size();

        // Create the ContainsBedRooms with an existing ID
        containsBedRooms.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContainsBedRoomsMockMvc.perform(post("/api/contains-bed-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBedRooms)))
            .andExpect(status().isBadRequest());

        // Validate the ContainsBedRooms in the database
        List<ContainsBedRooms> containsBedRoomsList = containsBedRoomsRepository.findAll();
        assertThat(containsBedRoomsList).hasSize(databaseSizeBeforeCreate);

        // Validate the ContainsBedRooms in Elasticsearch
        verify(mockContainsBedRoomsSearchRepository, times(0)).save(containsBedRooms);
    }


    @Test
    @Transactional
    public void checkNbOfUnitIsRequired() throws Exception {
        int databaseSizeBeforeTest = containsBedRoomsRepository.findAll().size();
        // set the field null
        containsBedRooms.setNbOfUnit(null);

        // Create the ContainsBedRooms, which fails.


        restContainsBedRoomsMockMvc.perform(post("/api/contains-bed-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBedRooms)))
            .andExpect(status().isBadRequest());

        List<ContainsBedRooms> containsBedRoomsList = containsBedRoomsRepository.findAll();
        assertThat(containsBedRoomsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContainsBedRooms() throws Exception {
        // Initialize the database
        containsBedRoomsRepository.saveAndFlush(containsBedRooms);

        // Get all the containsBedRoomsList
        restContainsBedRoomsMockMvc.perform(get("/api/contains-bed-rooms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(containsBedRooms.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbOfUnit").value(hasItem(DEFAULT_NB_OF_UNIT)));
    }
    
    @Test
    @Transactional
    public void getContainsBedRooms() throws Exception {
        // Initialize the database
        containsBedRoomsRepository.saveAndFlush(containsBedRooms);

        // Get the containsBedRooms
        restContainsBedRoomsMockMvc.perform(get("/api/contains-bed-rooms/{id}", containsBedRooms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(containsBedRooms.getId().intValue()))
            .andExpect(jsonPath("$.nbOfUnit").value(DEFAULT_NB_OF_UNIT));
    }
    @Test
    @Transactional
    public void getNonExistingContainsBedRooms() throws Exception {
        // Get the containsBedRooms
        restContainsBedRoomsMockMvc.perform(get("/api/contains-bed-rooms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContainsBedRooms() throws Exception {
        // Initialize the database
        containsBedRoomsRepository.saveAndFlush(containsBedRooms);

        int databaseSizeBeforeUpdate = containsBedRoomsRepository.findAll().size();

        // Update the containsBedRooms
        ContainsBedRooms updatedContainsBedRooms = containsBedRoomsRepository.findById(containsBedRooms.getId()).get();
        // Disconnect from session so that the updates on updatedContainsBedRooms are not directly saved in db
        em.detach(updatedContainsBedRooms);
        updatedContainsBedRooms
            .nbOfUnit(UPDATED_NB_OF_UNIT);

        restContainsBedRoomsMockMvc.perform(put("/api/contains-bed-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedContainsBedRooms)))
            .andExpect(status().isOk());

        // Validate the ContainsBedRooms in the database
        List<ContainsBedRooms> containsBedRoomsList = containsBedRoomsRepository.findAll();
        assertThat(containsBedRoomsList).hasSize(databaseSizeBeforeUpdate);
        ContainsBedRooms testContainsBedRooms = containsBedRoomsList.get(containsBedRoomsList.size() - 1);
        assertThat(testContainsBedRooms.getNbOfUnit()).isEqualTo(UPDATED_NB_OF_UNIT);

        // Validate the ContainsBedRooms in Elasticsearch
        verify(mockContainsBedRoomsSearchRepository, times(1)).save(testContainsBedRooms);
    }

    @Test
    @Transactional
    public void updateNonExistingContainsBedRooms() throws Exception {
        int databaseSizeBeforeUpdate = containsBedRoomsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContainsBedRoomsMockMvc.perform(put("/api/contains-bed-rooms")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(containsBedRooms)))
            .andExpect(status().isBadRequest());

        // Validate the ContainsBedRooms in the database
        List<ContainsBedRooms> containsBedRoomsList = containsBedRoomsRepository.findAll();
        assertThat(containsBedRoomsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ContainsBedRooms in Elasticsearch
        verify(mockContainsBedRoomsSearchRepository, times(0)).save(containsBedRooms);
    }

    @Test
    @Transactional
    public void deleteContainsBedRooms() throws Exception {
        // Initialize the database
        containsBedRoomsRepository.saveAndFlush(containsBedRooms);

        int databaseSizeBeforeDelete = containsBedRoomsRepository.findAll().size();

        // Delete the containsBedRooms
        restContainsBedRoomsMockMvc.perform(delete("/api/contains-bed-rooms/{id}", containsBedRooms.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContainsBedRooms> containsBedRoomsList = containsBedRoomsRepository.findAll();
        assertThat(containsBedRoomsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ContainsBedRooms in Elasticsearch
        verify(mockContainsBedRoomsSearchRepository, times(1)).deleteById(containsBedRooms.getId());
    }

    @Test
    @Transactional
    public void searchContainsBedRooms() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        containsBedRoomsRepository.saveAndFlush(containsBedRooms);
        when(mockContainsBedRoomsSearchRepository.search(queryStringQuery("id:" + containsBedRooms.getId())))
            .thenReturn(Collections.singletonList(containsBedRooms));

        // Search the containsBedRooms
        restContainsBedRoomsMockMvc.perform(get("/api/_search/contains-bed-rooms?query=id:" + containsBedRooms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(containsBedRooms.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbOfUnit").value(hasItem(DEFAULT_NB_OF_UNIT)));
    }
}
