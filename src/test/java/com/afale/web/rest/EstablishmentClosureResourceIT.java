package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.EstablishmentClosure;
import com.afale.repository.EstablishmentClosureRepository;
import com.afale.repository.search.EstablishmentClosureSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EstablishmentClosureResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class EstablishmentClosureResourceIT {

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CAUSE = "AAAAAAAAAA";
    private static final String UPDATED_CAUSE = "BBBBBBBBBB";

    @Autowired
    private EstablishmentClosureRepository establishmentClosureRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.EstablishmentClosureSearchRepositoryMockConfiguration
     */
    @Autowired
    private EstablishmentClosureSearchRepository mockEstablishmentClosureSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEstablishmentClosureMockMvc;

    private EstablishmentClosure establishmentClosure;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EstablishmentClosure createEntity(EntityManager em) {
        EstablishmentClosure establishmentClosure = new EstablishmentClosure()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .cause(DEFAULT_CAUSE);
        return establishmentClosure;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EstablishmentClosure createUpdatedEntity(EntityManager em) {
        EstablishmentClosure establishmentClosure = new EstablishmentClosure()
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .cause(UPDATED_CAUSE);
        return establishmentClosure;
    }

    @BeforeEach
    public void initTest() {
        establishmentClosure = createEntity(em);
    }

    @Test
    @Transactional
    public void createEstablishmentClosure() throws Exception {
        int databaseSizeBeforeCreate = establishmentClosureRepository.findAll().size();
        // Create the EstablishmentClosure
        restEstablishmentClosureMockMvc.perform(post("/api/establishment-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishmentClosure)))
            .andExpect(status().isCreated());

        // Validate the EstablishmentClosure in the database
        List<EstablishmentClosure> establishmentClosureList = establishmentClosureRepository.findAll();
        assertThat(establishmentClosureList).hasSize(databaseSizeBeforeCreate + 1);
        EstablishmentClosure testEstablishmentClosure = establishmentClosureList.get(establishmentClosureList.size() - 1);
        assertThat(testEstablishmentClosure.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEstablishmentClosure.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEstablishmentClosure.getCause()).isEqualTo(DEFAULT_CAUSE);

        // Validate the EstablishmentClosure in Elasticsearch
        verify(mockEstablishmentClosureSearchRepository, times(1)).save(testEstablishmentClosure);
    }

    @Test
    @Transactional
    public void createEstablishmentClosureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = establishmentClosureRepository.findAll().size();

        // Create the EstablishmentClosure with an existing ID
        establishmentClosure.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEstablishmentClosureMockMvc.perform(post("/api/establishment-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishmentClosure)))
            .andExpect(status().isBadRequest());

        // Validate the EstablishmentClosure in the database
        List<EstablishmentClosure> establishmentClosureList = establishmentClosureRepository.findAll();
        assertThat(establishmentClosureList).hasSize(databaseSizeBeforeCreate);

        // Validate the EstablishmentClosure in Elasticsearch
        verify(mockEstablishmentClosureSearchRepository, times(0)).save(establishmentClosure);
    }


    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentClosureRepository.findAll().size();
        // set the field null
        establishmentClosure.setStartDate(null);

        // Create the EstablishmentClosure, which fails.


        restEstablishmentClosureMockMvc.perform(post("/api/establishment-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishmentClosure)))
            .andExpect(status().isBadRequest());

        List<EstablishmentClosure> establishmentClosureList = establishmentClosureRepository.findAll();
        assertThat(establishmentClosureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentClosureRepository.findAll().size();
        // set the field null
        establishmentClosure.setEndDate(null);

        // Create the EstablishmentClosure, which fails.


        restEstablishmentClosureMockMvc.perform(post("/api/establishment-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishmentClosure)))
            .andExpect(status().isBadRequest());

        List<EstablishmentClosure> establishmentClosureList = establishmentClosureRepository.findAll();
        assertThat(establishmentClosureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEstablishmentClosures() throws Exception {
        // Initialize the database
        establishmentClosureRepository.saveAndFlush(establishmentClosure);

        // Get all the establishmentClosureList
        restEstablishmentClosureMockMvc.perform(get("/api/establishment-closures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(establishmentClosure.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].cause").value(hasItem(DEFAULT_CAUSE)));
    }
    
    @Test
    @Transactional
    public void getEstablishmentClosure() throws Exception {
        // Initialize the database
        establishmentClosureRepository.saveAndFlush(establishmentClosure);

        // Get the establishmentClosure
        restEstablishmentClosureMockMvc.perform(get("/api/establishment-closures/{id}", establishmentClosure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(establishmentClosure.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.cause").value(DEFAULT_CAUSE));
    }
    @Test
    @Transactional
    public void getNonExistingEstablishmentClosure() throws Exception {
        // Get the establishmentClosure
        restEstablishmentClosureMockMvc.perform(get("/api/establishment-closures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEstablishmentClosure() throws Exception {
        // Initialize the database
        establishmentClosureRepository.saveAndFlush(establishmentClosure);

        int databaseSizeBeforeUpdate = establishmentClosureRepository.findAll().size();

        // Update the establishmentClosure
        EstablishmentClosure updatedEstablishmentClosure = establishmentClosureRepository.findById(establishmentClosure.getId()).get();
        // Disconnect from session so that the updates on updatedEstablishmentClosure are not directly saved in db
        em.detach(updatedEstablishmentClosure);
        updatedEstablishmentClosure
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .cause(UPDATED_CAUSE);

        restEstablishmentClosureMockMvc.perform(put("/api/establishment-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEstablishmentClosure)))
            .andExpect(status().isOk());

        // Validate the EstablishmentClosure in the database
        List<EstablishmentClosure> establishmentClosureList = establishmentClosureRepository.findAll();
        assertThat(establishmentClosureList).hasSize(databaseSizeBeforeUpdate);
        EstablishmentClosure testEstablishmentClosure = establishmentClosureList.get(establishmentClosureList.size() - 1);
        assertThat(testEstablishmentClosure.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testEstablishmentClosure.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testEstablishmentClosure.getCause()).isEqualTo(UPDATED_CAUSE);

        // Validate the EstablishmentClosure in Elasticsearch
        verify(mockEstablishmentClosureSearchRepository, times(1)).save(testEstablishmentClosure);
    }

    @Test
    @Transactional
    public void updateNonExistingEstablishmentClosure() throws Exception {
        int databaseSizeBeforeUpdate = establishmentClosureRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEstablishmentClosureMockMvc.perform(put("/api/establishment-closures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishmentClosure)))
            .andExpect(status().isBadRequest());

        // Validate the EstablishmentClosure in the database
        List<EstablishmentClosure> establishmentClosureList = establishmentClosureRepository.findAll();
        assertThat(establishmentClosureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the EstablishmentClosure in Elasticsearch
        verify(mockEstablishmentClosureSearchRepository, times(0)).save(establishmentClosure);
    }

    @Test
    @Transactional
    public void deleteEstablishmentClosure() throws Exception {
        // Initialize the database
        establishmentClosureRepository.saveAndFlush(establishmentClosure);

        int databaseSizeBeforeDelete = establishmentClosureRepository.findAll().size();

        // Delete the establishmentClosure
        restEstablishmentClosureMockMvc.perform(delete("/api/establishment-closures/{id}", establishmentClosure.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EstablishmentClosure> establishmentClosureList = establishmentClosureRepository.findAll();
        assertThat(establishmentClosureList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the EstablishmentClosure in Elasticsearch
        verify(mockEstablishmentClosureSearchRepository, times(1)).deleteById(establishmentClosure.getId());
    }

    @Test
    @Transactional
    public void searchEstablishmentClosure() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        establishmentClosureRepository.saveAndFlush(establishmentClosure);
        when(mockEstablishmentClosureSearchRepository.search(queryStringQuery("id:" + establishmentClosure.getId())))
            .thenReturn(Collections.singletonList(establishmentClosure));

        // Search the establishmentClosure
        restEstablishmentClosureMockMvc.perform(get("/api/_search/establishment-closures?query=id:" + establishmentClosure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(establishmentClosure.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].cause").value(hasItem(DEFAULT_CAUSE)));
    }
}
