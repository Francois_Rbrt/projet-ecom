package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.BathRoomTemplate;
import com.afale.repository.BathRoomTemplateRepository;
import com.afale.repository.search.BathRoomTemplateSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BathRoomTemplateResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class BathRoomTemplateResourceIT {

    private static final Boolean DEFAULT_HAS_BATH = false;
    private static final Boolean UPDATED_HAS_BATH = true;

    private static final Boolean DEFAULT_HAS_TOILET = false;
    private static final Boolean UPDATED_HAS_TOILET = true;

    private static final Boolean DEFAULT_HAS_SHOWER = false;
    private static final Boolean UPDATED_HAS_SHOWER = true;

    @Autowired
    private BathRoomTemplateRepository bathRoomTemplateRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.BathRoomTemplateSearchRepositoryMockConfiguration
     */
    @Autowired
    private BathRoomTemplateSearchRepository mockBathRoomTemplateSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBathRoomTemplateMockMvc;

    private BathRoomTemplate bathRoomTemplate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BathRoomTemplate createEntity(EntityManager em) {
        BathRoomTemplate bathRoomTemplate = new BathRoomTemplate()
            .hasBath(DEFAULT_HAS_BATH)
            .hasToilet(DEFAULT_HAS_TOILET)
            .hasShower(DEFAULT_HAS_SHOWER);
        return bathRoomTemplate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BathRoomTemplate createUpdatedEntity(EntityManager em) {
        BathRoomTemplate bathRoomTemplate = new BathRoomTemplate()
            .hasBath(UPDATED_HAS_BATH)
            .hasToilet(UPDATED_HAS_TOILET)
            .hasShower(UPDATED_HAS_SHOWER);
        return bathRoomTemplate;
    }

    @BeforeEach
    public void initTest() {
        bathRoomTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createBathRoomTemplate() throws Exception {
        int databaseSizeBeforeCreate = bathRoomTemplateRepository.findAll().size();
        // Create the BathRoomTemplate
        restBathRoomTemplateMockMvc.perform(post("/api/bath-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bathRoomTemplate)))
            .andExpect(status().isCreated());

        // Validate the BathRoomTemplate in the database
        List<BathRoomTemplate> bathRoomTemplateList = bathRoomTemplateRepository.findAll();
        assertThat(bathRoomTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        BathRoomTemplate testBathRoomTemplate = bathRoomTemplateList.get(bathRoomTemplateList.size() - 1);
        assertThat(testBathRoomTemplate.isHasBath()).isEqualTo(DEFAULT_HAS_BATH);
        assertThat(testBathRoomTemplate.isHasToilet()).isEqualTo(DEFAULT_HAS_TOILET);
        assertThat(testBathRoomTemplate.isHasShower()).isEqualTo(DEFAULT_HAS_SHOWER);

        // Validate the BathRoomTemplate in Elasticsearch
        verify(mockBathRoomTemplateSearchRepository, times(1)).save(testBathRoomTemplate);
    }

    @Test
    @Transactional
    public void createBathRoomTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bathRoomTemplateRepository.findAll().size();

        // Create the BathRoomTemplate with an existing ID
        bathRoomTemplate.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBathRoomTemplateMockMvc.perform(post("/api/bath-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bathRoomTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the BathRoomTemplate in the database
        List<BathRoomTemplate> bathRoomTemplateList = bathRoomTemplateRepository.findAll();
        assertThat(bathRoomTemplateList).hasSize(databaseSizeBeforeCreate);

        // Validate the BathRoomTemplate in Elasticsearch
        verify(mockBathRoomTemplateSearchRepository, times(0)).save(bathRoomTemplate);
    }


    @Test
    @Transactional
    public void getAllBathRoomTemplates() throws Exception {
        // Initialize the database
        bathRoomTemplateRepository.saveAndFlush(bathRoomTemplate);

        // Get all the bathRoomTemplateList
        restBathRoomTemplateMockMvc.perform(get("/api/bath-room-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bathRoomTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].hasBath").value(hasItem(DEFAULT_HAS_BATH.booleanValue())))
            .andExpect(jsonPath("$.[*].hasToilet").value(hasItem(DEFAULT_HAS_TOILET.booleanValue())))
            .andExpect(jsonPath("$.[*].hasShower").value(hasItem(DEFAULT_HAS_SHOWER.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getBathRoomTemplate() throws Exception {
        // Initialize the database
        bathRoomTemplateRepository.saveAndFlush(bathRoomTemplate);

        // Get the bathRoomTemplate
        restBathRoomTemplateMockMvc.perform(get("/api/bath-room-templates/{id}", bathRoomTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bathRoomTemplate.getId().intValue()))
            .andExpect(jsonPath("$.hasBath").value(DEFAULT_HAS_BATH.booleanValue()))
            .andExpect(jsonPath("$.hasToilet").value(DEFAULT_HAS_TOILET.booleanValue()))
            .andExpect(jsonPath("$.hasShower").value(DEFAULT_HAS_SHOWER.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingBathRoomTemplate() throws Exception {
        // Get the bathRoomTemplate
        restBathRoomTemplateMockMvc.perform(get("/api/bath-room-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBathRoomTemplate() throws Exception {
        // Initialize the database
        bathRoomTemplateRepository.saveAndFlush(bathRoomTemplate);

        int databaseSizeBeforeUpdate = bathRoomTemplateRepository.findAll().size();

        // Update the bathRoomTemplate
        BathRoomTemplate updatedBathRoomTemplate = bathRoomTemplateRepository.findById(bathRoomTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedBathRoomTemplate are not directly saved in db
        em.detach(updatedBathRoomTemplate);
        updatedBathRoomTemplate
            .hasBath(UPDATED_HAS_BATH)
            .hasToilet(UPDATED_HAS_TOILET)
            .hasShower(UPDATED_HAS_SHOWER);

        restBathRoomTemplateMockMvc.perform(put("/api/bath-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBathRoomTemplate)))
            .andExpect(status().isOk());

        // Validate the BathRoomTemplate in the database
        List<BathRoomTemplate> bathRoomTemplateList = bathRoomTemplateRepository.findAll();
        assertThat(bathRoomTemplateList).hasSize(databaseSizeBeforeUpdate);
        BathRoomTemplate testBathRoomTemplate = bathRoomTemplateList.get(bathRoomTemplateList.size() - 1);
        assertThat(testBathRoomTemplate.isHasBath()).isEqualTo(UPDATED_HAS_BATH);
        assertThat(testBathRoomTemplate.isHasToilet()).isEqualTo(UPDATED_HAS_TOILET);
        assertThat(testBathRoomTemplate.isHasShower()).isEqualTo(UPDATED_HAS_SHOWER);

        // Validate the BathRoomTemplate in Elasticsearch
        verify(mockBathRoomTemplateSearchRepository, times(1)).save(testBathRoomTemplate);
    }

    @Test
    @Transactional
    public void updateNonExistingBathRoomTemplate() throws Exception {
        int databaseSizeBeforeUpdate = bathRoomTemplateRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBathRoomTemplateMockMvc.perform(put("/api/bath-room-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bathRoomTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the BathRoomTemplate in the database
        List<BathRoomTemplate> bathRoomTemplateList = bathRoomTemplateRepository.findAll();
        assertThat(bathRoomTemplateList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BathRoomTemplate in Elasticsearch
        verify(mockBathRoomTemplateSearchRepository, times(0)).save(bathRoomTemplate);
    }

    @Test
    @Transactional
    public void deleteBathRoomTemplate() throws Exception {
        // Initialize the database
        bathRoomTemplateRepository.saveAndFlush(bathRoomTemplate);

        int databaseSizeBeforeDelete = bathRoomTemplateRepository.findAll().size();

        // Delete the bathRoomTemplate
        restBathRoomTemplateMockMvc.perform(delete("/api/bath-room-templates/{id}", bathRoomTemplate.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BathRoomTemplate> bathRoomTemplateList = bathRoomTemplateRepository.findAll();
        assertThat(bathRoomTemplateList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the BathRoomTemplate in Elasticsearch
        verify(mockBathRoomTemplateSearchRepository, times(1)).deleteById(bathRoomTemplate.getId());
    }

    @Test
    @Transactional
    public void searchBathRoomTemplate() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        bathRoomTemplateRepository.saveAndFlush(bathRoomTemplate);
        when(mockBathRoomTemplateSearchRepository.search(queryStringQuery("id:" + bathRoomTemplate.getId())))
            .thenReturn(Collections.singletonList(bathRoomTemplate));

        // Search the bathRoomTemplate
        restBathRoomTemplateMockMvc.perform(get("/api/_search/bath-room-templates?query=id:" + bathRoomTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bathRoomTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].hasBath").value(hasItem(DEFAULT_HAS_BATH.booleanValue())))
            .andExpect(jsonPath("$.[*].hasToilet").value(hasItem(DEFAULT_HAS_TOILET.booleanValue())))
            .andExpect(jsonPath("$.[*].hasShower").value(hasItem(DEFAULT_HAS_SHOWER.booleanValue())));
    }
}
