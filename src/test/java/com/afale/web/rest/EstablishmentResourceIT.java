package com.afale.web.rest;

import com.afale.AfaleApp;
import com.afale.domain.Establishment;
import com.afale.repository.EstablishmentRepository;
import com.afale.repository.search.EstablishmentSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.afale.domain.enumeration.EstablishmentType;
/**
 * Integration tests for the {@link EstablishmentResource} REST controller.
 */
@SpringBootTest(classes = AfaleApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class EstablishmentResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADRESS = "BBBBBBBBBB";

    private static final Float DEFAULT_LATITUDE = 1F;
    private static final Float UPDATED_LATITUDE = 2F;

    private static final Float DEFAULT_LONGITUDE = 1F;
    private static final Float UPDATED_LONGITUDE = 2F;

    private static final Float DEFAULT_GLOBAL_RATE = 1F;
    private static final Float UPDATED_GLOBAL_RATE = 2F;

    private static final EstablishmentType DEFAULT_ESTABLISHMENT_TYPE = EstablishmentType.HOTEL;
    private static final EstablishmentType UPDATED_ESTABLISHMENT_TYPE = EstablishmentType.CAMPING;

    private static final Boolean DEFAULT_HAS_PARKING = false;
    private static final Boolean UPDATED_HAS_PARKING = true;

    private static final Boolean DEFAULT_HAS_RESTAURANT = false;
    private static final Boolean UPDATED_HAS_RESTAURANT = true;

    private static final Boolean DEFAULT_HAS_FREE_WIFI = false;
    private static final Boolean UPDATED_HAS_FREE_WIFI = true;

    private static final Boolean DEFAULT_HAS_SWIMMING_POOL = false;
    private static final Boolean UPDATED_HAS_SWIMMING_POOL = true;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private EstablishmentRepository establishmentRepository;

    /**
     * This repository is mocked in the com.afale.repository.search test package.
     *
     * @see com.afale.repository.search.EstablishmentSearchRepositoryMockConfiguration
     */
    @Autowired
    private EstablishmentSearchRepository mockEstablishmentSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEstablishmentMockMvc;

    private Establishment establishment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Establishment createEntity(EntityManager em) {
        Establishment establishment = new Establishment()
            .name(DEFAULT_NAME)
            .adress(DEFAULT_ADRESS)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .globalRate(DEFAULT_GLOBAL_RATE)
            .establishmentType(DEFAULT_ESTABLISHMENT_TYPE)
            .hasParking(DEFAULT_HAS_PARKING)
            .hasRestaurant(DEFAULT_HAS_RESTAURANT)
            .hasFreeWifi(DEFAULT_HAS_FREE_WIFI)
            .hasSwimmingPool(DEFAULT_HAS_SWIMMING_POOL)
            .description(DEFAULT_DESCRIPTION);
        return establishment;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Establishment createUpdatedEntity(EntityManager em) {
        Establishment establishment = new Establishment()
            .name(UPDATED_NAME)
            .adress(UPDATED_ADRESS)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .globalRate(UPDATED_GLOBAL_RATE)
            .establishmentType(UPDATED_ESTABLISHMENT_TYPE)
            .hasParking(UPDATED_HAS_PARKING)
            .hasRestaurant(UPDATED_HAS_RESTAURANT)
            .hasFreeWifi(UPDATED_HAS_FREE_WIFI)
            .hasSwimmingPool(UPDATED_HAS_SWIMMING_POOL)
            .description(UPDATED_DESCRIPTION);
        return establishment;
    }

    @BeforeEach
    public void initTest() {
        establishment = createEntity(em);
    }

    @Test
    @Transactional
    public void createEstablishment() throws Exception {
        int databaseSizeBeforeCreate = establishmentRepository.findAll().size();
        // Create the Establishment
        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isCreated());

        // Validate the Establishment in the database
        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeCreate + 1);
        Establishment testEstablishment = establishmentList.get(establishmentList.size() - 1);
        assertThat(testEstablishment.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEstablishment.getAdress()).isEqualTo(DEFAULT_ADRESS);
        assertThat(testEstablishment.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEstablishment.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEstablishment.getGlobalRate()).isEqualTo(DEFAULT_GLOBAL_RATE);
        assertThat(testEstablishment.getEstablishmentType()).isEqualTo(DEFAULT_ESTABLISHMENT_TYPE);
        assertThat(testEstablishment.isHasParking()).isEqualTo(DEFAULT_HAS_PARKING);
        assertThat(testEstablishment.isHasRestaurant()).isEqualTo(DEFAULT_HAS_RESTAURANT);
        assertThat(testEstablishment.isHasFreeWifi()).isEqualTo(DEFAULT_HAS_FREE_WIFI);
        assertThat(testEstablishment.isHasSwimmingPool()).isEqualTo(DEFAULT_HAS_SWIMMING_POOL);
        assertThat(testEstablishment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Establishment in Elasticsearch
        verify(mockEstablishmentSearchRepository, times(1)).save(testEstablishment);
    }

    @Test
    @Transactional
    public void createEstablishmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = establishmentRepository.findAll().size();

        // Create the Establishment with an existing ID
        establishment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        // Validate the Establishment in the database
        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeCreate);

        // Validate the Establishment in Elasticsearch
        verify(mockEstablishmentSearchRepository, times(0)).save(establishment);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentRepository.findAll().size();
        // set the field null
        establishment.setName(null);

        // Create the Establishment, which fails.


        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAdressIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentRepository.findAll().size();
        // set the field null
        establishment.setAdress(null);

        // Create the Establishment, which fails.


        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstablishmentTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentRepository.findAll().size();
        // set the field null
        establishment.setEstablishmentType(null);

        // Create the Establishment, which fails.


        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHasParkingIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentRepository.findAll().size();
        // set the field null
        establishment.setHasParking(null);

        // Create the Establishment, which fails.


        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHasRestaurantIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentRepository.findAll().size();
        // set the field null
        establishment.setHasRestaurant(null);

        // Create the Establishment, which fails.


        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHasFreeWifiIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentRepository.findAll().size();
        // set the field null
        establishment.setHasFreeWifi(null);

        // Create the Establishment, which fails.


        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHasSwimmingPoolIsRequired() throws Exception {
        int databaseSizeBeforeTest = establishmentRepository.findAll().size();
        // set the field null
        establishment.setHasSwimmingPool(null);

        // Create the Establishment, which fails.


        restEstablishmentMockMvc.perform(post("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEstablishments() throws Exception {
        // Initialize the database
        establishmentRepository.saveAndFlush(establishment);

        // Get all the establishmentList
        restEstablishmentMockMvc.perform(get("/api/establishments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(establishment.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].globalRate").value(hasItem(DEFAULT_GLOBAL_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].establishmentType").value(hasItem(DEFAULT_ESTABLISHMENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].hasParking").value(hasItem(DEFAULT_HAS_PARKING.booleanValue())))
            .andExpect(jsonPath("$.[*].hasRestaurant").value(hasItem(DEFAULT_HAS_RESTAURANT.booleanValue())))
            .andExpect(jsonPath("$.[*].hasFreeWifi").value(hasItem(DEFAULT_HAS_FREE_WIFI.booleanValue())))
            .andExpect(jsonPath("$.[*].hasSwimmingPool").value(hasItem(DEFAULT_HAS_SWIMMING_POOL.booleanValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getEstablishment() throws Exception {
        // Initialize the database
        establishmentRepository.saveAndFlush(establishment);

        // Get the establishment
        restEstablishmentMockMvc.perform(get("/api/establishments/{id}", establishment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(establishment.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.adress").value(DEFAULT_ADRESS))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.globalRate").value(DEFAULT_GLOBAL_RATE.doubleValue()))
            .andExpect(jsonPath("$.establishmentType").value(DEFAULT_ESTABLISHMENT_TYPE.toString()))
            .andExpect(jsonPath("$.hasParking").value(DEFAULT_HAS_PARKING.booleanValue()))
            .andExpect(jsonPath("$.hasRestaurant").value(DEFAULT_HAS_RESTAURANT.booleanValue()))
            .andExpect(jsonPath("$.hasFreeWifi").value(DEFAULT_HAS_FREE_WIFI.booleanValue()))
            .andExpect(jsonPath("$.hasSwimmingPool").value(DEFAULT_HAS_SWIMMING_POOL.booleanValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingEstablishment() throws Exception {
        // Get the establishment
        restEstablishmentMockMvc.perform(get("/api/establishments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEstablishment() throws Exception {
        // Initialize the database
        establishmentRepository.saveAndFlush(establishment);

        int databaseSizeBeforeUpdate = establishmentRepository.findAll().size();

        // Update the establishment
        Establishment updatedEstablishment = establishmentRepository.findById(establishment.getId()).get();
        // Disconnect from session so that the updates on updatedEstablishment are not directly saved in db
        em.detach(updatedEstablishment);
        updatedEstablishment
            .name(UPDATED_NAME)
            .adress(UPDATED_ADRESS)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .globalRate(UPDATED_GLOBAL_RATE)
            .establishmentType(UPDATED_ESTABLISHMENT_TYPE)
            .hasParking(UPDATED_HAS_PARKING)
            .hasRestaurant(UPDATED_HAS_RESTAURANT)
            .hasFreeWifi(UPDATED_HAS_FREE_WIFI)
            .hasSwimmingPool(UPDATED_HAS_SWIMMING_POOL)
            .description(UPDATED_DESCRIPTION);

        restEstablishmentMockMvc.perform(put("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEstablishment)))
            .andExpect(status().isOk());

        // Validate the Establishment in the database
        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeUpdate);
        Establishment testEstablishment = establishmentList.get(establishmentList.size() - 1);
        assertThat(testEstablishment.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEstablishment.getAdress()).isEqualTo(UPDATED_ADRESS);
        assertThat(testEstablishment.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testEstablishment.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testEstablishment.getGlobalRate()).isEqualTo(UPDATED_GLOBAL_RATE);
        assertThat(testEstablishment.getEstablishmentType()).isEqualTo(UPDATED_ESTABLISHMENT_TYPE);
        assertThat(testEstablishment.isHasParking()).isEqualTo(UPDATED_HAS_PARKING);
        assertThat(testEstablishment.isHasRestaurant()).isEqualTo(UPDATED_HAS_RESTAURANT);
        assertThat(testEstablishment.isHasFreeWifi()).isEqualTo(UPDATED_HAS_FREE_WIFI);
        assertThat(testEstablishment.isHasSwimmingPool()).isEqualTo(UPDATED_HAS_SWIMMING_POOL);
        assertThat(testEstablishment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Establishment in Elasticsearch
        verify(mockEstablishmentSearchRepository, times(1)).save(testEstablishment);
    }

    @Test
    @Transactional
    public void updateNonExistingEstablishment() throws Exception {
        int databaseSizeBeforeUpdate = establishmentRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEstablishmentMockMvc.perform(put("/api/establishments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(establishment)))
            .andExpect(status().isBadRequest());

        // Validate the Establishment in the database
        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Establishment in Elasticsearch
        verify(mockEstablishmentSearchRepository, times(0)).save(establishment);
    }

    @Test
    @Transactional
    public void deleteEstablishment() throws Exception {
        // Initialize the database
        establishmentRepository.saveAndFlush(establishment);

        int databaseSizeBeforeDelete = establishmentRepository.findAll().size();

        // Delete the establishment
        restEstablishmentMockMvc.perform(delete("/api/establishments/{id}", establishment.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Establishment> establishmentList = establishmentRepository.findAll();
        assertThat(establishmentList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Establishment in Elasticsearch
        verify(mockEstablishmentSearchRepository, times(1)).deleteById(establishment.getId());
    }

    @Test
    @Transactional
    public void searchEstablishment() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        establishmentRepository.saveAndFlush(establishment);
        when(mockEstablishmentSearchRepository.search(queryStringQuery("id:" + establishment.getId())))
            .thenReturn(Collections.singletonList(establishment));

        // Search the establishment
        restEstablishmentMockMvc.perform(get("/api/_search/establishments?query=id:" + establishment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(establishment.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].globalRate").value(hasItem(DEFAULT_GLOBAL_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].establishmentType").value(hasItem(DEFAULT_ESTABLISHMENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].hasParking").value(hasItem(DEFAULT_HAS_PARKING.booleanValue())))
            .andExpect(jsonPath("$.[*].hasRestaurant").value(hasItem(DEFAULT_HAS_RESTAURANT.booleanValue())))
            .andExpect(jsonPath("$.[*].hasFreeWifi").value(hasItem(DEFAULT_HAS_FREE_WIFI.booleanValue())))
            .andExpect(jsonPath("$.[*].hasSwimmingPool").value(hasItem(DEFAULT_HAS_SWIMMING_POOL.booleanValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
}
