################################################################################################################################################################################################

				################################################################
				#								 #
				#			ORGANISATION DU PROJET :		 #
				#								 #
				################################################################
				
################################################################################################################################################################################################
	BACK END :	./src/main/java/com/afale 
	
	# config : pleins de fichiers de config des différents services (ultra fun)  : 
		ANTOINE je crois avoir vu un truc qui parle de profil dev pour la connexion a la BD et rien pour le profil "prod", a creuser ?
	
	# domain : ensemble des fichiers JPA qui définit les tables de notre BD
	
	# repository : représentation "en java" de nos tables : c'est grace a ca qu'on va pouvoir faire des SELECT par ex : <nom du repository>.findAll(...) : renvoie une liste d'objet 
	
	# repository/search : pour elastic search (je crois)
	
	# web/rest : ensemble des rests controlers pour toutes nos entités persistantes
	
	
	
		
################################################################################################################################################################################################	
	FRONT END :	./src/main/webapp
	
	# app/entities : l'ensemble du code front end pour toutes nos entités 
	
	# app/entities : la page d'accueil du site 
	
	# i18n/xxx : contient les traduction en différentes langues des mots clés définis je ne sais ou (du style "not_found" / "created" / "updated" / ... pour chaque entités )
	

################################################################################################################################################################################################
	PARTIE TEST :. /src/test
	
	# gatling : test de performance gatling (pas besoin d'y toucher ? )
	
	# java/com/afale/domain : test unitaire sur chacunes des entités (possibilité de rajouter des tests ici j'imagine )
	
	# pas besoin de toucher au reste j'ai l'impression 
	

################################################################################################################################################################################################

génération des entités avec la commande
jhipster import-jdl <fichier .jdl> 

#########

resultat avec l'exemple de l'entité booking :

 Définition de la partie JPA (définition de la table des attributs etc )
	src/main/java/com/afale/domain/Booking.java

 Definition du Spring repository 	
	src/main/java/com/afale/repository/BookingRepository.java
	
 Definition du search repository ( pour elastic search )
	src/main/java/com/afale/repository/search/BookingSearchRepository.java
	
 Definition du REST controler pour l'entité booking
 	src/main/java/com/afale/web/rest/BookingResource.java

 Ecriture d'un changelog pour liquibase dans lequel cette entité est ajouté à la BD : 
	src/main/resources/config/liquibase/changelog/20201111172055_added_entity_Booking.xml

 Ajout de fichier .csv représentant des fake data
	src/main/resources/config/liquibase/fake-data/
	
 Création de la partie front end pour booking :
	src/main/webapp/app/entities/booking/
	
 ???
 	src/main/webapp/app/shared/model/booking.model.ts
 	
 Ecriture dans des json des traduction anglaise et fr des "message d'erreurs etc" associés 
	src/main/webapp/i18n/en/booking.json
	src/main/webapp/i18n/fr/booking.json
	
 Generation des fichiers pour les tests de performance gatling
 	src/test/gatling/user-files/

 Génération des fichiers de tests unitaire : 
 	src/test/java/com/afale/domain/

 ???
 	src/test/java/com/afale/repository/search/BookingSearchRepositoryMockConfiguration.java

 Test d'intégration pour le rest controler associé 
	src/test/java/com/afale/web/rest/BookingResourceIT.java
	
 Test sur la partie front end ?
 	src/test/javascript/spec/app/entities/
 	













